source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.6'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1.0', '< 6.2.0'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# TODO switch back when activerecord-postgis-adaptor 7.0.0 is released with Rails 6.1 support
# gem 'activerecord-postgis-adapter', '~> 6.0.0'
gem 'activerecord-postgis-adapter'

# Use Puma as the app server
gem 'puma', '~> 4.1'
# Use SCSS for stylesheets
# gem 'sass-rails', '>= 6'
gem 'sassc-rails', '~> 2.1', '>= 2.1.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

gem 'webpacker', '~> 5.x'

group :development, :test do
  gem 'sqlite3'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry'
  gem 'rspec-rails'
  gem 'rspec-collection_matchers'
  gem 'guard'
  gem 'guard-rspec'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'awesome_print', '~> 1.8.0'
  gem 'method_source'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
  gem 'simplecov'
end

group :staging, :production do
  gem 'cloudflare-rails'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'friendly_id', '~> 5.3.0'
gem 'aasm'

gem 'request_store'

gem 'sidekiq'
gem 'request_store-sidekiq'

gem 'cloudflair'

gem 'aws-sdk-s3'
# Admin 
gem 'activeadmin'
gem 'arctic_admin'
gem 'activeadmin-searchable_select'
gem 'active_admin-sortable_tree', '~> 2.0.0'
gem 'ancestry'
gem 'activeadmin_dynamic_fields'
gem 'image_processing', '~> 1.2'
gem 'devise'
gem 'pundit'
gem 'fx'
gem 'scenic'
gem 'rollbar'
gem 'faraday'
gem 'faker'
gem 'factory_bot_rails'
gem 'faraday_middleware'

gem 'netlify', path: "./gems/netlify"

gem 'imgix-rails'

gem 'humap-data_sources-capture', path: './gems/humap-data_sources-capture'

gem 'humap-data_pipelines', path: './gems/humap-data_pipelines'

gem 'humap-data_sources-excel', path: './gems/humap-data_sources-excel'

# gem 'mapstatic', path: './gems/mapstatic'
gem 'mapstatic', github: 'errorstudio/mapstatic'

gem 'ruby-oembed', require: 'oembed'

gem 'rqrcode'

gem 'mailgun-ruby'
