SHELL=/usr/bin/env bash
.DEFAULT_TARGET := info

args = `arg="$(filter-out $@,$(MAKECMDGOALS))" && echo $${arg:-${1}}`
database_state = `docker-compose ps | grep postgres | awk '{print $4}'`


info:
	@echo A few tools to ruin your day.
	@echo - make pull-production-db: download a database from heroku, DESTROYING YOUR LOCAL DATABASE. Requires the heroku binary and brew install libpq on MacOS
	@echo - make push-staging-db: you'll need to manually heroku pg:reset on the database you're targeting, first.

pull-production-db: .drop_database
	@echo Importing database from heroku production-core-10 to your local development environment.

	PGPASSWORD=postgres heroku pg:pull DATABASE_URL postgres://postgres:postgres@localhost:5432/humap_development -a production-core-10

push-staging-db: .start_database
	@echo Exporting your local database to staging.
	PGPASSWORD=postgres heroku pg:push postgres://postgres:postgres@localhost:5432/humap_development DATABASE_URL -a staging-core-6e


setup:
	@echo Setting up core in docker on your machine
	docker-compose up postgres


.install_libpq:
	brew install libpq
	brew link --force libpq

.bundle:
	docker-compose run humap-core bundle && yarn

.drop_database: .start_database
	@echo Dropping existing humap_development database. Enter postgres password.
	PGPASSWORD=postgres dropdb -h localhost -p 5432 --if-exists --username=postgres --maintenance-db=postgres humap_development

.start_database:
	docker-compose up -d postgres
#	@if [ $(database_state) != 'Up' ]; then \

#fi


	