select t.id,
       t.name,
       t.account_settings ->> 'stylesheet'::text    as stylesheet,
       (t.account_settings ->> 'embedded' = 'true') as is_embedded,
       (mai.url is not null)                        as has_map_logo,
       (sai.url is not null)                        as has_site_logo,
       coalesce(mai.url, '')                        as map_logo_url,
       coalesce(sai.url, '')                        as site_logo_url
from tenants as t
         left outer join attachments_images as mai on t.map_logo_id = mai.id
         left outer join attachments_images as sai on t.site_logo_id = sai.id