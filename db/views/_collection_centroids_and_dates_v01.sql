select humap_collections_collections.id,
       st_geometricmedian(
           st_multi(
               st_collect(
                   ARRAY(
                       SELECT records.lonlat::geometry AS lonlat
                      FROM records
                      JOIN humap_collections_record_collections hcrc ON records.id = hcrc.record_id
                      WHERE hcrc.collection_id = humap_collections_collections.id
                       )
                   )
               )
           ) as lonlat,
       (SELECT min(records.date_from)::timestamp with time zone AS min
        FROM records
                 JOIN humap_collections_record_collections h ON records.id = h.record_id
        WHERE h.collection_id = humap_collections_collections.id)                                                   AS date_from,
       (SELECT max(records.date_to)::timestamp with time zone AS max
        FROM records
                 JOIN humap_collections_record_collections h ON records.id = h.record_id
        WHERE h.collection_id = humap_collections_collections.id)                                                   AS date_to
from humap_collections_collections