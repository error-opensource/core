SELECT records.id,
       records.name,
       records.slug,
       records.sanitised_content,
       records.lonlat,
       records.date_from,
       records.date_to,
       records.tsv_document,
       'record'::text                                                                                                AS type,
       tenants.slug                                                                                                  AS tenant_slug,
       tenants.id                                                                                                    AS tenant_id,
       row_to_json((SELECT d.*::record AS d
                    FROM (SELECT records.name,
                                 records.slug,
                                 ARRAY [st_x(records.lonlat::geometry), st_y(records.lonlat::geometry)] AS lonlat,
                                 'record'::text                                                         AS type) d)) AS point_json,
       NULL::numeric                                                                                                 AS distance,
       records.excerpt,
       (
            SELECT array_agg(record_terms.taxonomy_term_id) FROM record_terms
            WHERE record_terms.record_id = records.id
       ) AS term_ids,
       records.state                                                                                                AS state
FROM records
         JOIN tenants ON records.tenant_id = tenants.id
WHERE records.lonlat IS NOT NULL
UNION ALL
SELECT humap_collections_collections.id,
       humap_collections_collections.name,
       humap_collections_collections.slug,
       humap_collections_collections.sanitised_content,
       (SELECT _collection_centroids_and_dates.lonlat
        FROM _collection_centroids_and_dates
        WHERE _collection_centroids_and_dates.id = humap_collections_collections.id
        LIMIT 1)                                                                                            AS lonlat,
       (SELECT _collection_centroids_and_dates.date_from
        FROM _collection_centroids_and_dates
        WHERE _collection_centroids_and_dates.id = humap_collections_collections.id
        LIMIT 1)                                                                                            AS date_from,
       (SELECT _collection_centroids_and_dates.date_to
        FROM _collection_centroids_and_dates
        WHERE _collection_centroids_and_dates.id = humap_collections_collections.id
        LIMIT 1)                                                                                            AS date_to,
       humap_collections_collections.tsv_document,
       'collection'::text                                                                                   AS type,
       tenants.slug                                                                                         AS tenant_slug,
       tenants.id                                                                                           AS tenant_id,
       row_to_json((SELECT d.*::record AS d
                    FROM (SELECT humap_collections_collections.name,
                                 humap_collections_collections.slug,
                                 ARRAY [st_x((SELECT _collection_centroids_and_dates.lonlat
                                              FROM _collection_centroids_and_dates
                                              WHERE _collection_centroids_and_dates.id = humap_collections_collections.id
                                              LIMIT 1)), st_y((SELECT _collection_centroids_and_dates.lonlat
                                                               FROM _collection_centroids_and_dates
                                                               WHERE _collection_centroids_and_dates.id = humap_collections_collections.id
                                                               LIMIT 1))] AS lonlat,
                                 'collection'::text                       AS type) d))                      AS point_json,
       NULL::numeric                                                                                        AS distance,
       humap_collections_collections.excerpt,
       (
            SELECT (array_agg(humap_collections_collection_taxonomy_terms.taxonomy_term_id)) FROM humap_collections_collection_taxonomy_terms
            WHERE humap_collections_collection_taxonomy_terms.collection_id = humap_collections_collections.id
       ) AS term_ids,
       humap_collections_collections.state AS state

FROM humap_collections_collections
         JOIN tenants ON humap_collections_collections.tenant_id = tenants.id
WHERE (SELECT _collection_centroids_and_dates.lonlat
       FROM _collection_centroids_and_dates
       WHERE _collection_centroids_and_dates.id = humap_collections_collections.id
       LIMIT 1) IS NOT NULL;
