SELECT records.id,
       records.name,
       records.slug,
       records.sanitised_content,
       records.lonlat,
       records.date_from,
       records.date_to,
       records.tsv_document,
       'record'::text                                                                                                AS type,
       tenants.slug                                                                                                  AS tenant_slug,
       tenants.id                                                                                                    AS tenant_id,
       row_to_json((SELECT d.*::record AS d
                    FROM (SELECT records.name,
                                 records.slug,
                                 ARRAY [st_x(records.lonlat::geometry), st_y(records.lonlat::geometry)] AS lonlat,
                                 'record'::text                                                         AS type) d)) AS point_json,
        NULL::numeric                                                                                                AS distance
FROM records
         JOIN tenants ON records.tenant_id = tenants.id
UNION ALL
SELECT humap_collections_collections.id,
       humap_collections_collections.name,
       humap_collections_collections.slug,
       humap_collections_collections.sanitised_content,
       geography(st_point(0::double precision, 0::double precision)) AS lonlat,
       now()                                                         AS date_from,
       now()                                                         AS date_to,
       humap_collections_collections.tsv_document,
       'collection'::text                                            AS type,
       tenants.slug                                                  AS tenant_slug,
       tenants.id                                                    AS tenant_id,
       row_to_json((SELECT d.*::record AS d
                    FROM (SELECT humap_collections_collections.name,
                                 humap_collections_collections.slug,
                                 'collection'::text AS type) d))     AS point_json,
       NULL::numeric                                                    AS distance
FROM humap_collections_collections
        JOIN tenants ON humap_collections_collections.tenant_id = tenants.id;
