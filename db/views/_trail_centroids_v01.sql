select humap_trails_trails.id,
       st_geometricmedian(
               st_multi(
                       st_collect(
                               ARRAY(
                                   SELECT records.lonlat::geometry AS lonlat
                      FROM records
                      JOIN humap_trails_trail_records httr ON records.id = httr.record_id
                      WHERE httr.trail_id = humap_trails_trails.id
                       )
                           )
                   )
           ) as lonlat,
       (SELECT min(records.date_from)::timestamp with time zone AS min
FROM records
    JOIN humap_trails_trail_records h ON records.id = h.record_id
WHERE h.trail_id = humap_trails_trails.id) AS date_from,
    (SELECT max(records.date_to)::timestamp with time zone AS max
FROM records
    JOIN humap_trails_trail_records h ON records.id = h.record_id
WHERE h.trail_id = humap_trails_trails.id) AS date_to
from humap_trails_trails;