select t.id,
       t.name,
       t.account_settings ->> 'stylesheet'::text    as stylesheet,
       (t.account_settings ->> 'embedded' = 'true') as is_embedded,
       (ai.url is not null)                         as has_logo,
       coalesce(ai.url, '')                         as logo_url
from tenants as t
         left outer join attachments_images as ai on t.site_logo_id = ai.id