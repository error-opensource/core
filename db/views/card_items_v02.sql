SELECT records.id,
       records.name,
       records.slug,
       records.sanitised_content,
       records.lonlat,
       records.date_from,
       records.date_to,
       records.tsv_document,
       'record'::text AS type,
       tenants.slug   AS tenant_slug,
       tenants.id     AS tenant_id,
       row_to_json(
               (SELECT d.*::record AS d
                FROM (SELECT records.name,
                             records.slug,
                             ARRAY [st_x(records.lonlat::geometry), st_y(records.lonlat::geometry)] AS lonlat,
                             'record'::text                                                         AS type
                     ) as d
               )
           )          AS point_json,
       NULL::numeric  AS distance,
       records.excerpt
FROM records
         JOIN tenants ON records.tenant_id = tenants.id
UNION ALL
SELECT humap_collections_collections.id,
       humap_collections_collections.name,
       humap_collections_collections.slug,
       humap_collections_collections.sanitised_content,
       st_geometricmedian(st_multi(st_collect(ARRAY(SELECT records.lonlat::geometry AS lonlat
                                                    FROM records
                                                             JOIN humap_collections_record_collections hcrc ON records.id = hcrc.record_id
                                                    WHERE hcrc.collection_id = humap_collections_collections.id)))) AS lonlat,
       (SELECT min(records.date_from)::timestamp with time zone AS min
        FROM records
                 JOIN humap_collections_record_collections h ON records.id = h.record_id
        WHERE h.collection_id = humap_collections_collections.id)                                                   AS date_from,
       (SELECT max(records.date_to)::timestamp with time zone AS max
        FROM records
                 JOIN humap_collections_record_collections h ON records.id = h.record_id
        WHERE h.collection_id = humap_collections_collections.id)                                                   AS date_to,
       humap_collections_collections.tsv_document,
       'collection'::text                                                                                           AS type,
       tenants.slug                                                                                                 AS tenant_slug,
       tenants.id                                                                                                   AS tenant_id,
       row_to_json((SELECT d.*::record AS d
                    FROM (SELECT humap_collections_collections.name,
                                 humap_collections_collections.slug,
                                 ARRAY [st_x(st_geometricmedian(
                                         st_multi(st_collect(ARRAY(SELECT records.lonlat::geometry AS lonlat
                                                                   FROM records
                                                                            JOIN humap_collections_record_collections hcrc ON records.id = hcrc.record_id
                                                                   WHERE hcrc.collection_id = humap_collections_collections.id))))), st_y(
                                         st_geometricmedian(
                                                 st_multi(st_collect(ARRAY(SELECT records.lonlat::geometry AS lonlat
                                                                           FROM records
                                                                                    JOIN humap_collections_record_collections hcrc ON records.id = hcrc.record_id
                                                                           WHERE hcrc.collection_id = humap_collections_collections.id)))))] AS lonlat,
                                 'collection'::text                                                                                          AS type) d)) AS point_json,
       NULL::numeric                                                                                                AS distance,
       humap_collections_collections.excerpt
FROM humap_collections_collections
         JOIN tenants ON humap_collections_collections.tenant_id = tenants.id;
