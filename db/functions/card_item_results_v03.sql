create function card_item_results(query text DEFAULT NULL::text, centroid text DEFAULT NULL::text, orderby text DEFAULT 'rank'::text, with_term_ids integer[] DEFAULT NULL::integer[]) returns SETOF card_items
    stable
    language plpgsql
as
$$
BEGIN
    IF query IS NULL AND centroid IS NULL THEN RETURN QUERY
SELECT card_items.* FROM card_items
WHERE (with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids);
ELSEIF query IS NOT NULL AND centroid IS NULL THEN RETURN QUERY
SELECT card_items.*
FROM card_items, websearch_to_tsquery(query) AS ts_query
WHERE ((with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids)) AND ts_query @@ tsv_document
ORDER BY (ts_rank_cd( tsv_document, ts_query ) );
ELSEIF query IS NULL AND centroid IS NOT NULL THEN RETURN QUERY
SELECT DISTINCT ON(results.id, results.type) * FROM
    (SELECT card_items.id,
    card_items.name,
    card_items.slug,
    card_items.sanitised_content,
    card_items.lonlat,
    card_items.date_from,
    card_items.date_to,
    card_items.tsv_document,
    card_items.type,
    card_items.tenant_slug,
    card_items.tenant_id,
    card_items.point_json,
    CAST( ST_DistanceSphere( card_items.lonlat::geometry , ST_GeomFromText('POINT(' || centroid || ')', 4326)) AS numeric) AS distance,
    card_items.excerpt,
    card_items.term_ids,
    card_items.state
    FROM card_items
    WHERE (with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids)
    ORDER BY distance
    ) AS results;
ELSE RETURN QUERY
SELECT DISTINCT ON(results.id, results.type) * FROM
    (SELECT
    card_items.id,
    card_items.name,
    card_items.slug,
    card_items.sanitised_content,
    card_items.lonlat,
    card_items.date_from,
    card_items.date_to,
    card_items.tsv_document,
    card_items.type,
    card_items.tenant_slug,
    card_items.tenant_id,
    card_items.point_json,
    CAST( ST_DistanceSphere( card_items.lonlat::geometry , ST_GeomFromText('POINT(' || centroid || ')', 4326)) AS numeric) AS distance,
    card_items.excerpt,
    card_items.term_ids,
    card_items.state
    FROM card_items, websearch_to_tsquery(query) AS ts_query
    WHERE ((with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids)) AND ts_query @@ tsv_document
    ORDER BY CASE
    WHEN(orderby = 'rank') THEN (ts_rank_cd( tsv_document, ts_query ) )
    WHEN(orderby = 'distance') THEN distance
    END
    ) AS results;
END IF;
END;
$$;
