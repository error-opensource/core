CREATE OR REPLACE FUNCTION strip_html(html text) RETURNS text AS $$
BEGIN
    return(select array_to_string(xpath('//text()', ('<root>' || html || '</root>')::xml)::text[],''));
EXCEPTION
    WHEN invalid_xml_content THEN
        return '';


END;
$$ LANGUAGE plpgsql;