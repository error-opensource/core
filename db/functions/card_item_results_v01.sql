create function card_item_results(query text DEFAULT NULL::text, centroid text DEFAULT NULL::text, orderby text DEFAULT 'rank'::text, with_term_ids integer[] DEFAULT NULL::integer[]) returns SETOF card_items
    stable
    language plpgsql
as
$$
BEGIN
    IF query IS NULL AND centroid IS NULL THEN RETURN QUERY
        SELECT card_items.* FROM card_items
            WHERE (with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids);
    ELSEIF query IS NOT NULL AND centroid IS NULL THEN RETURN QUERY
        SELECT card_items.*
        FROM card_items
            INNER JOIN (
                SELECT id,
                    (
                    ts_rank_cd(
                            tsv_document,
                            ts_query
                        )
                    ) AS rank
            FROM card_items, websearch_to_tsquery(query) AS ts_query
            WHERE ts_query @@ tsv_document
        ) AS pg_query
        ON card_items.id = pg_query.id
        WHERE (with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids)
        ORDER BY rank;
    ELSEIF query IS NULL AND centroid IS NOT NULL THEN RETURN QUERY
        SELECT DISTINCT ON(results.id, results.type) * FROM
        (SELECT card_items.id, card_items.name, card_items.slug, card_items.sanitised_content, card_items.lonlat, card_items.date_from, card_items.date_to, card_items.tsv_document, card_items.type, card_items.tenant_slug, card_items.tenant_id, card_items.point_json, pg_query.distance, card_items.excerpt, card_items.term_ids
            FROM card_items
                INNER JOIN (
                    SELECT
                    id,
                        CAST( ST_DistanceSphere( ST_Centroid( card_items.lonlat::geometry ), ST_GeomFromText('POINT(' || centroid || ')', 4326)) AS numeric) AS distance
                    FROM card_items
                ) AS pg_query
            ON card_items.id = pg_query.id
            WHERE (with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids)
            ORDER BY pg_query.distance
        ) AS results;
    ELSE RETURN QUERY
        SELECT DISTINCT ON(results.id, results.type) * FROM
        (SELECT card_items.id, card_items.name, card_items.slug, card_items.sanitised_content, card_items.lonlat, card_items.date_from, card_items.date_to, card_items.tsv_document, card_items.type, card_items.tenant_slug, card_items.tenant_id, card_items.point_json, pg_query.distance, card_items.excerpt, card_items.term_ids
            FROM card_items
                INNER JOIN (
                    SELECT
                    id,
                        (ts_rank_cd( tsv_document, ts_query ) ) AS rank,
                        CAST( ST_DistanceSphere( ST_Centroid( card_items.lonlat::geometry ), ST_GeomFromText('POINT(' || centroid || ')', 4326)) AS numeric) AS distance
                    FROM card_items, websearch_to_tsquery(query) AS ts_query
                    WHERE ts_query @@ tsv_document
                ) AS pg_query
            ON card_items.id = pg_query.id
            WHERE (with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids)
            ORDER BY CASE
                WHEN(orderby = 'rank') THEN rank
                WHEN(orderby = 'distance') THEN pg_query.distance
            END
        ) AS results;
    END IF;
END;
$$;

