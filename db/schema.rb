# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_16_085521) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "attachments_files", force: :cascade do |t|
    t.string "name", null: false
    t.string "credit"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "import_metadata", default: {}
    t.string "url"
    t.index "((import_metadata ->> 'id'::text))", name: "index_attachments_files_on_import_metadata_id"
  end

  create_table "attachments_images", force: :cascade do |t|
    t.string "name"
    t.string "credit"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "url"
    t.jsonb "metadata", default: {}
    t.jsonb "import_metadata", default: {}
    t.index "((import_metadata ->> 'id'::text))", name: "index_attachments_images_on_import_metadata_id"
  end

  create_table "attachments_video_embeds", force: :cascade do |t|
    t.string "name", null: false
    t.string "url", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "import_metadata", default: {}
    t.jsonb "metadata", default: {}
    t.string "credit"
    t.index "((import_metadata ->> 'id'::text))", name: "index_attachments_video_embeds_on_import_metadata_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "humap_collections_collection_attachments_images", force: :cascade do |t|
    t.bigint "collection_id", null: false
    t.bigint "attachments_image_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["attachments_image_id"], name: "humap_collections_attachment_id"
    t.index ["collection_id"], name: "humap_collections_attachment_collection_id"
  end

  create_table "humap_collections_collection_records", force: :cascade do |t|
    t.bigint "record_id", null: false
    t.bigint "collection_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["collection_id"], name: "index_humap_collections_collection_records_on_collection_id"
    t.index ["record_id"], name: "index_humap_collections_collection_records_on_record_id"
  end

  create_table "humap_collections_collection_taxonomy_terms", force: :cascade do |t|
    t.bigint "collection_id", null: false
    t.bigint "taxonomy_term_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["collection_id"], name: "humap_collections_collection_id"
    t.index ["taxonomy_term_id"], name: "humap_collections_term_id"
  end

  create_table "humap_collections_collections", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "name", null: false
    t.string "slug", null: false
    t.text "sanitised_content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "state"
    t.integer "image_id"
    t.tsvector "tsv_document"
    t.text "keywords"
    t.text "excerpt"
    t.index ["slug"], name: "index_humap_collections_collections_on_slug", unique: true
    t.index ["tenant_id"], name: "index_humap_collections_collections_on_tenant_id"
  end

  create_table "humap_modules", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "humap_overlays_overlay_group_overlays", force: :cascade do |t|
    t.bigint "overlay_id", null: false
    t.bigint "overlay_group_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["overlay_group_id"], name: "index_humap_overlays_overlay_group_overlays_on_overlay_group_id"
    t.index ["overlay_id"], name: "index_humap_overlays_overlay_group_overlays_on_overlay_id"
  end

  create_table "humap_overlays_overlay_groups", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "name", null: false
    t.string "slug", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "state"
    t.text "sanitised_content"
    t.integer "image_id"
    t.jsonb "import_metadata", default: {}
    t.text "excerpt"
    t.index "((import_metadata ->> 'id'::text))", name: "index_humap_overlays_overlay_groups_on_import_metadata_id"
    t.index ["slug"], name: "index_humap_overlays_overlay_groups_on_slug", unique: true
    t.index ["tenant_id"], name: "index_humap_overlays_overlay_groups_on_tenant_id"
  end

  create_table "humap_overlays_overlay_types", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "overlay_type"
  end

  create_table "humap_overlays_overlays", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.bigint "overlay_type_id", null: false
    t.string "name", null: false
    t.string "description"
    t.string "url", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "state"
    t.jsonb "metadata", default: {}
    t.jsonb "import_metadata", default: {}
    t.text "keywords"
    t.index "((import_metadata ->> 'id'::text))", name: "index_humap_overlays_overlays_on_import_metadata_id"
    t.index ["overlay_type_id"], name: "index_humap_overlays_overlays_on_overlay_type_id"
    t.index ["tenant_id"], name: "index_humap_overlays_overlays_on_tenant_id"
  end

  create_table "humap_pages_cta_blocks", force: :cascade do |t|
    t.bigint "page_id", null: false
    t.string "title", null: false
    t.text "content"
    t.string "url", null: false
    t.string "button_text", null: false
    t.string "sanitised_content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["page_id"], name: "index_cta_blocks_on_page_id"
  end

  create_table "humap_pages_page_attachments_images", force: :cascade do |t|
    t.bigint "page_id", null: false
    t.bigint "image_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["image_id"], name: "index_images_on_image_id"
    t.index ["page_id"], name: "index_images_on_page_id"
  end

  create_table "humap_pages_page_images", force: :cascade do |t|
    t.bigint "page_id", null: false
    t.bigint "attachments_image_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["attachments_image_id"], name: "index_humap_pages_page_images_on_attachments_image_id"
    t.index ["page_id"], name: "index_humap_pages_page_images_on_page_id"
  end

  create_table "humap_pages_page_quick_starts", force: :cascade do |t|
    t.bigint "page_id", null: false
    t.bigint "quick_start_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["page_id"], name: "index_humap_pages_page_quick_starts_on_page_id"
    t.index ["quick_start_id"], name: "index_humap_pages_page_quick_starts_on_quick_start_id"
  end

  create_table "humap_pages_pages", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "name", null: false
    t.string "slug", null: false
    t.text "sanitised_content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "state"
    t.integer "image_id"
    t.integer "page_type", default: 0, null: false
    t.text "highlighted_content_intro"
    t.text "description"
    t.integer "search_image_id"
    t.string "page_location"
    t.integer "position"
    t.index ["slug"], name: "index_humap_pages_pages_on_slug", unique: true
    t.index ["tenant_id"], name: "index_humap_pages_pages_on_tenant_id"
  end

  create_table "humap_streaming_media_audio_files", force: :cascade do |t|
    t.string "name"
    t.string "url"
    t.text "credit"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "humap_streaming_media_record_audio_files", force: :cascade do |t|
    t.bigint "record_id", null: false
    t.bigint "audio_file_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["audio_file_id"], name: "index_record_audio_files_on_audio_id"
    t.index ["record_id"], name: "index_humap_streaming_media_record_audio_files_on_record_id"
  end

  create_table "humap_streaming_media_record_videos", force: :cascade do |t|
    t.bigint "record_id", null: false
    t.bigint "video_attachment_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_id"], name: "index_humap_streaming_media_record_videos_on_record_id"
    t.index ["video_attachment_id"], name: "humap_streaming_media_video_id"
  end

  create_table "humap_streaming_media_video_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "credit"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "humap_teams_team_records", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_id"], name: "index_humap_teams_team_records_on_record_id"
    t.index ["team_id"], name: "humap_teams_team_id"
  end

  create_table "humap_teams_team_users", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "humap_teams_user_id"
    t.index ["user_id"], name: "index_humap_teams_team_users_on_user_id"
  end

  create_table "humap_teams_teams", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "name", null: false
    t.string "slug", null: false
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["slug"], name: "index_humap_teams_teams_on_slug", unique: true
    t.index ["tenant_id"], name: "index_humap_teams_teams_on_tenant_id"
  end

  create_table "humap_trails_trail_attachments_images", force: :cascade do |t|
    t.bigint "trail_id", null: false
    t.bigint "attachments_image_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["attachments_image_id"], name: "humap_trails_attachment_id"
    t.index ["trail_id"], name: "humap_trails_attachment_trail_id"
  end

  create_table "humap_trails_trail_records", force: :cascade do |t|
    t.bigint "trail_id", null: false
    t.bigint "record_id", null: false
    t.integer "position", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_id"], name: "index_humap_trails_trail_records_on_record_id"
    t.index ["trail_id", "position"], name: "index_humap_trails_trail_records_on_trail_id_and_position"
    t.index ["trail_id"], name: "index_humap_trails_trail_records_on_trail_id"
  end

  create_table "humap_trails_trail_taxonomy_terms", force: :cascade do |t|
    t.bigint "trail_id", null: false
    t.bigint "taxonomy_term_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["taxonomy_term_id"], name: "humap_trails_term_id"
    t.index ["trail_id"], name: "humap_trails_trail_id"
  end

  create_table "humap_trails_trails", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.bigint "image_id"
    t.string "name"
    t.string "slug", null: false
    t.text "sanitised_content"
    t.text "excerpt"
    t.text "keywords"
    t.tsvector "tsv_document"
    t.string "state"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "duration"
    t.text "distance"
    t.index ["image_id"], name: "index_images_trail_id"
    t.index ["slug"], name: "index_humap_trails_trails_on_slug", unique: true
    t.index ["tenant_id"], name: "index_humap_trails_trails_on_tenant_id"
    t.index ["tsv_document"], name: "index_humap_trails_trails_on_tsv_document"
  end

  create_table "humap_zoomable_images_record_zoomable_images", force: :cascade do |t|
    t.bigint "record_id", null: false
    t.bigint "zoomable_image_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_id"], name: "index_humap_zoomable_images_record_zoomable_images_on_record_id"
    t.index ["zoomable_image_id"], name: "humap_zoomable_images_id"
  end

  create_table "humap_zoomable_images_zoomable_images", force: :cascade do |t|
    t.string "name", null: false
    t.string "slug", null: false
    t.string "credit"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["slug"], name: "index_humap_zoomable_images_zoomable_images_on_slug", unique: true
  end

  create_table "logos", force: :cascade do |t|
    t.string "link_to"
    t.integer "image_id"
    t.integer "size", default: 0, null: false
    t.string "logo_size", default: "regular", null: false
  end

  create_table "permalinks", force: :cascade do |t|
    t.string "short_url", null: false
    t.bigint "permalinkable_id", null: false
    t.string "permalinkable_type", null: false
    t.bigint "resolution_count", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "qr_code"
    t.string "frontend_url"
    t.index ["permalinkable_id", "permalinkable_type"], name: "index_permalinks_on_permalinkable_id_and_permalinkable_type"
    t.index ["short_url"], name: "index_permalinks_on_short_url", unique: true
  end

  create_table "profiles", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.text "biography"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "quick_starts", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "title", null: false
    t.text "content"
    t.string "url"
    t.integer "image_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tenant_id"], name: "index_quick_starts_on_tenant_id"
  end

  create_table "record_attachments_files", force: :cascade do |t|
    t.bigint "record_id", null: false
    t.bigint "attachments_file_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["attachments_file_id"], name: "index_record_attachments_files_on_attachments_file_id"
    t.index ["record_id"], name: "index_record_attachments_files_on_record_id"
  end

  create_table "record_attachments_images", force: :cascade do |t|
    t.bigint "record_id", null: false
    t.bigint "attachments_image_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["attachments_image_id"], name: "index_record_attachments_images_on_attachments_image_id"
    t.index ["record_id"], name: "index_record_attachments_images_on_record_id"
  end

  create_table "record_attachments_video_embeds", force: :cascade do |t|
    t.bigint "record_id", null: false
    t.bigint "attachments_video_embed_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["attachments_video_embed_id"], name: "record_attached_video_embed_id"
    t.index ["record_id"], name: "index_record_attachments_video_embeds_on_record_id"
  end

  create_table "record_terms", force: :cascade do |t|
    t.bigint "record_id", null: false
    t.bigint "taxonomy_term_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_id"], name: "index_record_terms_on_record_id"
    t.index ["taxonomy_term_id"], name: "index_record_terms_on_taxonomy_term_id"
  end

  create_table "records", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "tenant_id", null: false
    t.string "name", null: false
    t.string "slug", null: false
    t.text "sanitised_content"
    t.geography "lonlat", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.string "state"
    t.datetime "published_at"
    t.datetime "date_from"
    t.datetime "date_to"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "image_id"
    t.boolean "include_image_in_gallery"
    t.tsvector "tsv_document"
    t.jsonb "import_metadata", default: {}
    t.text "keywords"
    t.text "excerpt"
    t.string "hero_attachment_type"
    t.integer "hero_attachment_id"
    t.index "((import_metadata ->> 'id'::text))", name: "index_records_on_import_metadata_id"
    t.index ["lonlat"], name: "index_records_on_lonlat", using: :gist
    t.index ["slug", "tenant_id"], name: "index_records_on_slug_and_tenant_id", unique: true
    t.index ["tenant_id"], name: "index_records_on_tenant_id"
    t.index ["user_id"], name: "index_records_on_user_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "name", null: false
    t.jsonb "default_value", default: "{}"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "slug", null: false
    t.index ["default_value"], name: "index_settings_on_default_value", using: :gin
  end

  create_table "site_meta_logos", force: :cascade do |t|
    t.bigint "site_meta_id", null: false
    t.bigint "logo_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["logo_id"], name: "index_site_meta_logos_on_logo_id"
    t.index ["site_meta_id"], name: "index_site_meta_logos_on_site_meta_id"
  end

  create_table "site_metas", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "website"
    t.string "contact_email"
    t.string "site_title"
    t.string "twitter"
    t.string "facebook"
    t.string "instagram"
    t.geography "centroid", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.index ["tenant_id"], name: "index_site_metas_on_tenant_id"
  end

  create_table "super_admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_super_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_super_admin_users_on_reset_password_token", unique: true
  end

  create_table "taxonomies", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "name", null: false
    t.string "slug", null: false
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "display_as", default: 0
    t.boolean "hierarchical"
    t.index ["slug"], name: "index_taxonomies_on_slug", unique: true
    t.index ["tenant_id"], name: "index_taxonomies_on_tenant_id"
  end

  create_table "taxonomy_terms", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "taxonomy_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "position"
    t.string "ancestry"
    t.integer "queryable_parent_id"
    t.integer "tenant_id"
    t.index ["ancestry"], name: "index_taxonomy_terms_on_ancestry"
    t.index ["taxonomy_id"], name: "index_taxonomy_terms_on_taxonomy_id"
  end

  create_table "tenant_humap_modules", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.bigint "humap_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["humap_module_id"], name: "index_tenant_humap_modules_on_humap_module_id"
    t.index ["tenant_id"], name: "index_tenant_humap_modules_on_tenant_id"
  end

  create_table "tenant_pipelines", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "pipeline"
    t.jsonb "settings", default: {}
    t.jsonb "log", default: []
    t.string "last_job_id"
    t.index ["last_job_id"], name: "index_tenant_pipelines_on_last_job_id"
    t.index ["log"], name: "index_tenant_pipelines_on_log", using: :gin
    t.index ["pipeline"], name: "index_tenant_pipelines_on_pipeline"
    t.index ["tenant_id"], name: "index_tenant_pipelines_on_tenant_id"
  end

  create_table "tenant_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "tenant_id", null: false
    t.string "name"
    t.index ["email"], name: "index_tenant_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_tenant_users_on_reset_password_token", unique: true
    t.index ["tenant_id"], name: "index_tenant_users_on_tenant_id"
  end

  create_table "tenants", force: :cascade do |t|
    t.string "name", null: false
    t.string "slug", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "creation_status", default: {}, null: false
    t.string "state", default: "pending", null: false
    t.integer "build_status", default: 0
    t.text "build_log"
    t.jsonb "account_settings", default: {"netlify"=>{"domain"=>nil, "site_id"=>nil, "rebuild_hook_url"=>nil}}
    t.string "last_build_url"
    t.integer "site_logo_id"
    t.integer "map_logo_id"
    t.index ["slug"], name: "index_tenants_on_slug", unique: true
  end

  create_table "tray_view_quick_starts", force: :cascade do |t|
    t.bigint "tray_view_id", null: false
    t.bigint "quick_start_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["quick_start_id"], name: "index_tray_view_quick_starts_on_quick_start_id"
    t.index ["tray_view_id"], name: "index_tray_view_quick_starts_on_tray_view_id"
  end

  create_table "tray_view_records", force: :cascade do |t|
    t.bigint "tray_view_id", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_id"], name: "index_tray_view_records_on_record_id"
    t.index ["tray_view_id"], name: "index_tray_view_records_on_tray_view_id"
  end

  create_table "tray_view_taxonomies", force: :cascade do |t|
    t.bigint "tray_view_id", null: false
    t.bigint "taxonomy_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["taxonomy_id"], name: "index_tray_view_taxonomies_on_taxonomy_id"
    t.index ["tray_view_id"], name: "index_tray_view_taxonomies_on_tray_view_id"
  end

  create_table "tray_views", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "title", null: false
    t.text "sanitised_content"
    t.string "view_type", default: "introduction", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "highlighted_records_title"
    t.text "highlighted_records_description"
    t.string "highlighted_taxonomies_title"
    t.text "highlighted_taxonomies_description"
    t.string "highlighted_quick_starts_title"
    t.text "highlighted_quick_starts_description"
    t.index ["tenant_id"], name: "index_tray_views_on_tenant_id"
    t.index ["view_type"], name: "index_tray_views_on_view_type"
  end

  create_table "users", force: :cascade do |t|
    t.bigint "tenant_id", null: false
    t.string "username"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tenant_id"], name: "index_users_on_tenant_id"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "humap_collections_collection_attachments_images", "attachments_images"
  add_foreign_key "humap_collections_collection_attachments_images", "humap_collections_collections", column: "collection_id"
  add_foreign_key "humap_collections_collection_records", "humap_collections_collections", column: "collection_id"
  add_foreign_key "humap_collections_collection_records", "records", on_delete: :cascade
  add_foreign_key "humap_collections_collection_taxonomy_terms", "humap_collections_collections", column: "collection_id"
  add_foreign_key "humap_collections_collection_taxonomy_terms", "taxonomy_terms"
  add_foreign_key "humap_collections_collections", "tenants"
  add_foreign_key "humap_overlays_overlay_group_overlays", "humap_overlays_overlay_groups", column: "overlay_group_id"
  add_foreign_key "humap_overlays_overlay_group_overlays", "humap_overlays_overlays", column: "overlay_id"
  add_foreign_key "humap_overlays_overlay_groups", "tenants"
  add_foreign_key "humap_overlays_overlays", "humap_overlays_overlay_types", column: "overlay_type_id"
  add_foreign_key "humap_overlays_overlays", "tenants"
  add_foreign_key "humap_pages_cta_blocks", "humap_pages_pages", column: "page_id"
  add_foreign_key "humap_pages_page_attachments_images", "attachments_images", column: "image_id"
  add_foreign_key "humap_pages_page_attachments_images", "humap_pages_pages", column: "page_id"
  add_foreign_key "humap_pages_page_images", "attachments_images", on_delete: :cascade
  add_foreign_key "humap_pages_page_images", "humap_pages_pages", column: "page_id", on_delete: :cascade
  add_foreign_key "humap_pages_page_quick_starts", "humap_pages_pages", column: "page_id", on_delete: :cascade
  add_foreign_key "humap_pages_page_quick_starts", "quick_starts", on_delete: :cascade
  add_foreign_key "humap_pages_pages", "tenants"
  add_foreign_key "humap_streaming_media_record_audio_files", "humap_streaming_media_record_audio_files", column: "audio_file_id"
  add_foreign_key "humap_streaming_media_record_audio_files", "records"
  add_foreign_key "humap_streaming_media_record_videos", "humap_streaming_media_video_attachments", column: "video_attachment_id"
  add_foreign_key "humap_streaming_media_record_videos", "records"
  add_foreign_key "humap_teams_team_records", "humap_teams_teams", column: "team_id"
  add_foreign_key "humap_teams_team_records", "records"
  add_foreign_key "humap_teams_team_users", "humap_teams_teams", column: "team_id"
  add_foreign_key "humap_teams_team_users", "users"
  add_foreign_key "humap_teams_teams", "tenants"
  add_foreign_key "humap_trails_trail_attachments_images", "attachments_images"
  add_foreign_key "humap_trails_trail_attachments_images", "humap_trails_trails", column: "trail_id"
  add_foreign_key "humap_trails_trail_records", "humap_trails_trails", column: "trail_id"
  add_foreign_key "humap_trails_trail_records", "records"
  add_foreign_key "humap_trails_trail_taxonomy_terms", "humap_trails_trails", column: "trail_id"
  add_foreign_key "humap_trails_trail_taxonomy_terms", "taxonomy_terms"
  add_foreign_key "humap_trails_trails", "attachments_images", column: "image_id"
  add_foreign_key "humap_trails_trails", "tenants"
  add_foreign_key "humap_zoomable_images_record_zoomable_images", "humap_zoomable_images_zoomable_images", column: "zoomable_image_id"
  add_foreign_key "humap_zoomable_images_record_zoomable_images", "records"
  add_foreign_key "profiles", "users"
  add_foreign_key "quick_starts", "tenants"
  add_foreign_key "record_attachments_files", "attachments_files"
  add_foreign_key "record_attachments_files", "records", on_delete: :cascade
  add_foreign_key "record_attachments_images", "attachments_images"
  add_foreign_key "record_attachments_images", "records", on_delete: :cascade
  add_foreign_key "record_attachments_video_embeds", "attachments_video_embeds"
  add_foreign_key "record_attachments_video_embeds", "records", on_delete: :cascade
  add_foreign_key "record_terms", "records", on_delete: :cascade
  add_foreign_key "record_terms", "taxonomy_terms", on_delete: :cascade
  add_foreign_key "records", "tenants"
  add_foreign_key "records", "users"
  add_foreign_key "site_meta_logos", "logos"
  add_foreign_key "site_meta_logos", "site_metas"
  add_foreign_key "site_metas", "tenants"
  add_foreign_key "taxonomies", "tenants"
  add_foreign_key "taxonomy_terms", "taxonomies", on_delete: :cascade
  add_foreign_key "tenant_humap_modules", "humap_modules"
  add_foreign_key "tenant_humap_modules", "tenants"
  add_foreign_key "tenant_pipelines", "tenants"
  add_foreign_key "tenant_users", "tenants"
  add_foreign_key "tray_view_quick_starts", "quick_starts"
  add_foreign_key "tray_view_quick_starts", "tray_views"
  add_foreign_key "tray_view_records", "records"
  add_foreign_key "tray_view_records", "tray_views"
  add_foreign_key "tray_view_taxonomies", "taxonomies"
  add_foreign_key "tray_view_taxonomies", "tray_views"
  add_foreign_key "tray_views", "tenants"
  add_foreign_key "users", "tenants"

  create_view "_collection_centroids_and_dates", materialized: true, sql_definition: <<-SQL
      SELECT humap_collections_collections.id,
      st_geometricmedian(st_multi(st_collect(ARRAY( SELECT (records.lonlat)::geometry AS lonlat
             FROM (records
               JOIN humap_collections_collection_records hccr ON ((records.id = hccr.record_id)))
            WHERE (hccr.collection_id = humap_collections_collections.id))))) AS lonlat,
      ( SELECT (min(records.date_from))::timestamp with time zone AS min
             FROM (records
               JOIN humap_collections_collection_records h ON ((records.id = h.record_id)))
            WHERE (h.collection_id = humap_collections_collections.id)) AS date_from,
      ( SELECT (max(records.date_to))::timestamp with time zone AS max
             FROM (records
               JOIN humap_collections_collection_records h ON ((records.id = h.record_id)))
            WHERE (h.collection_id = humap_collections_collections.id)) AS date_to
     FROM humap_collections_collections;
  SQL
  add_index "_collection_centroids_and_dates", ["id"], name: "index__collection_centroids_and_dates_on_id", unique: true

  create_view "_trail_centroids", materialized: true, sql_definition: <<-SQL
      SELECT humap_trails_trails.id,
      st_geometricmedian(st_multi(st_collect(ARRAY( SELECT (records.lonlat)::geometry AS lonlat
             FROM (records
               JOIN humap_trails_trail_records httr ON ((records.id = httr.record_id)))
            WHERE (httr.trail_id = humap_trails_trails.id))))) AS lonlat,
      ( SELECT (min(records.date_from))::timestamp with time zone AS min
             FROM (records
               JOIN humap_trails_trail_records h ON ((records.id = h.record_id)))
            WHERE (h.trail_id = humap_trails_trails.id)) AS date_from,
      ( SELECT (max(records.date_to))::timestamp with time zone AS max
             FROM (records
               JOIN humap_trails_trail_records h ON ((records.id = h.record_id)))
            WHERE (h.trail_id = humap_trails_trails.id)) AS date_to
     FROM humap_trails_trails;
  SQL
  add_index "_trail_centroids", ["id"], name: "index__trail_centroids_on_id", unique: true

  create_view "card_items", sql_definition: <<-SQL
      SELECT records.id,
      records.name,
      records.slug,
      records.sanitised_content,
      records.lonlat,
      records.date_from,
      records.date_to,
      records.tsv_document,
      'record'::text AS type,
      tenants.slug AS tenant_slug,
      tenants.id AS tenant_id,
      row_to_json(( SELECT d.*::record AS d
             FROM ( SELECT records.name,
                      records.slug,
                      ARRAY[st_x((records.lonlat)::geometry), st_y((records.lonlat)::geometry)] AS lonlat,
                      'record'::text AS type) d)) AS point_json,
      NULL::numeric AS distance,
      records.excerpt,
      ( SELECT array_agg(record_terms.taxonomy_term_id) AS array_agg
             FROM record_terms
            WHERE (record_terms.record_id = records.id)) AS term_ids,
      records.state
     FROM (records
       JOIN tenants ON ((records.tenant_id = tenants.id)))
    WHERE (records.lonlat IS NOT NULL)
  UNION ALL
   SELECT humap_collections_collections.id,
      humap_collections_collections.name,
      humap_collections_collections.slug,
      humap_collections_collections.sanitised_content,
      ( SELECT _collection_centroids_and_dates.lonlat
             FROM _collection_centroids_and_dates
            WHERE (_collection_centroids_and_dates.id = humap_collections_collections.id)
           LIMIT 1) AS lonlat,
      ( SELECT _collection_centroids_and_dates.date_from
             FROM _collection_centroids_and_dates
            WHERE (_collection_centroids_and_dates.id = humap_collections_collections.id)
           LIMIT 1) AS date_from,
      ( SELECT _collection_centroids_and_dates.date_to
             FROM _collection_centroids_and_dates
            WHERE (_collection_centroids_and_dates.id = humap_collections_collections.id)
           LIMIT 1) AS date_to,
      humap_collections_collections.tsv_document,
      'collection'::text AS type,
      tenants.slug AS tenant_slug,
      tenants.id AS tenant_id,
      row_to_json(( SELECT d.*::record AS d
             FROM ( SELECT humap_collections_collections.name,
                      humap_collections_collections.slug,
                      ARRAY[st_x(( SELECT _collection_centroids_and_dates.lonlat
                             FROM _collection_centroids_and_dates
                            WHERE (_collection_centroids_and_dates.id = humap_collections_collections.id)
                           LIMIT 1)), st_y(( SELECT _collection_centroids_and_dates.lonlat
                             FROM _collection_centroids_and_dates
                            WHERE (_collection_centroids_and_dates.id = humap_collections_collections.id)
                           LIMIT 1))] AS lonlat,
                      'collection'::text AS type) d)) AS point_json,
      NULL::numeric AS distance,
      humap_collections_collections.excerpt,
      ( SELECT array_agg(humap_collections_collection_taxonomy_terms.taxonomy_term_id) AS array_agg
             FROM humap_collections_collection_taxonomy_terms
            WHERE (humap_collections_collection_taxonomy_terms.collection_id = humap_collections_collections.id)) AS term_ids,
      humap_collections_collections.state
     FROM (humap_collections_collections
       JOIN tenants ON ((humap_collections_collections.tenant_id = tenants.id)))
    WHERE (( SELECT _collection_centroids_and_dates.lonlat
             FROM _collection_centroids_and_dates
            WHERE (_collection_centroids_and_dates.id = humap_collections_collections.id)
           LIMIT 1) IS NOT NULL)
  UNION ALL
   SELECT humap_trails_trails.id,
      humap_trails_trails.name,
      humap_trails_trails.slug,
      humap_trails_trails.sanitised_content,
      ( SELECT _trail_centroids.lonlat
             FROM _trail_centroids
            WHERE (_trail_centroids.id = humap_trails_trails.id)
           LIMIT 1) AS lonlat,
      ( SELECT _trail_centroids.date_from
             FROM _trail_centroids
            WHERE (_trail_centroids.id = humap_trails_trails.id)
           LIMIT 1) AS date_from,
      ( SELECT _trail_centroids.date_to
             FROM _trail_centroids
            WHERE (_trail_centroids.id = humap_trails_trails.id)
           LIMIT 1) AS date_to,
      humap_trails_trails.tsv_document,
      'trail'::text AS type,
      tenants.slug AS tenant_slug,
      tenants.id AS tenant_id,
      row_to_json(( SELECT d.*::record AS d
             FROM ( SELECT humap_trails_trails.name,
                      humap_trails_trails.slug,
                      ARRAY[st_x(( SELECT _trail_centroids.lonlat
                             FROM _trail_centroids
                            WHERE (_trail_centroids.id = humap_trails_trails.id)
                           LIMIT 1)), st_y(( SELECT _trail_centroids.lonlat
                             FROM _trail_centroids
                            WHERE (_trail_centroids.id = humap_trails_trails.id)
                           LIMIT 1))] AS lonlat,
                      'trail'::text AS type) d)) AS point_json,
      NULL::numeric AS distance,
      humap_trails_trails.excerpt,
      ( SELECT array_agg(humap_trails_trail_taxonomy_terms.taxonomy_term_id) AS array_agg
             FROM humap_trails_trail_taxonomy_terms
            WHERE (humap_trails_trail_taxonomy_terms.trail_id = humap_trails_trails.id)) AS term_ids,
      humap_trails_trails.state
     FROM (humap_trails_trails
       JOIN tenants ON ((humap_trails_trails.tenant_id = tenants.id)))
    WHERE (( SELECT _trail_centroids.lonlat
             FROM _trail_centroids
            WHERE (_trail_centroids.id = humap_trails_trails.id)
           LIMIT 1) IS NOT NULL);
  SQL
  create_view "tenant_settings", sql_definition: <<-SQL
      SELECT t.id,
      t.name,
      (t.account_settings ->> 'stylesheet'::text) AS stylesheet,
      ((t.account_settings ->> 'embedded'::text) = 'true'::text) AS is_embedded,
      (mai.url IS NOT NULL) AS has_map_logo,
      (sai.url IS NOT NULL) AS has_site_logo,
      COALESCE(mai.url, ''::character varying) AS map_logo_url,
      COALESCE(sai.url, ''::character varying) AS site_logo_url
     FROM ((tenants t
       LEFT JOIN attachments_images mai ON ((t.map_logo_id = mai.id)))
       LEFT JOIN attachments_images sai ON ((t.site_logo_id = sai.id)));
  SQL
  create_function :card_item_results, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.card_item_results(query text DEFAULT NULL::text, centroid text DEFAULT NULL::text, orderby text DEFAULT 'rank'::text, with_term_ids integer[] DEFAULT NULL::integer[])
       RETURNS SETOF card_items
       LANGUAGE plpgsql
       STABLE
      AS $function$
      BEGIN
          IF query IS NULL AND centroid IS NULL THEN RETURN QUERY
      SELECT card_items.* FROM card_items
      WHERE (with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids);
      ELSEIF query IS NOT NULL AND centroid IS NULL THEN RETURN QUERY
      SELECT card_items.*
      FROM card_items, websearch_to_tsquery(query) AS ts_query
      WHERE ((with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids)) AND ts_query @@ tsv_document
      ORDER BY (ts_rank_cd( tsv_document, ts_query ) );
      ELSEIF query IS NULL AND centroid IS NOT NULL THEN RETURN QUERY
      SELECT DISTINCT ON(results.id, results.type) * FROM
          (SELECT card_items.id,
          card_items.name,
          card_items.slug,
          card_items.sanitised_content,
          card_items.lonlat,
          card_items.date_from,
          card_items.date_to,
          card_items.tsv_document,
          card_items.type,
          card_items.tenant_slug,
          card_items.tenant_id,
          card_items.point_json,
          CAST( ST_DistanceSphere( card_items.lonlat::geometry , ST_GeomFromText('POINT(' || centroid || ')', 4326)) AS numeric) AS distance,
          card_items.excerpt,
          card_items.term_ids,
          card_items.state
          FROM card_items
          WHERE (with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids)
          ORDER BY distance
          ) AS results;
      ELSE RETURN QUERY
      SELECT DISTINCT ON(results.id, results.type) * FROM
          (SELECT
          card_items.id,
          card_items.name,
          card_items.slug,
          card_items.sanitised_content,
          card_items.lonlat,
          card_items.date_from,
          card_items.date_to,
          card_items.tsv_document,
          card_items.type,
          card_items.tenant_slug,
          card_items.tenant_id,
          card_items.point_json,
          CAST( ST_DistanceSphere( card_items.lonlat::geometry , ST_GeomFromText('POINT(' || centroid || ')', 4326)) AS numeric) AS distance,
          card_items.excerpt,
          card_items.term_ids,
          card_items.state
          FROM card_items, websearch_to_tsquery(query) AS ts_query
          WHERE ((with_term_ids IS NULL) OR (term_ids::INT[] && with_term_ids)) AND ts_query @@ tsv_document
          ORDER BY CASE
          WHEN(orderby = 'rank') THEN (ts_rank_cd( tsv_document, ts_query ) )
          WHEN(orderby = 'distance') THEN distance
          END
          ) AS results;
      END IF;
      END;
      $function$
  SQL
  create_function :generate_tsv_on_collections, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.generate_tsv_on_collections()
       RETURNS trigger
       LANGUAGE plpgsql
      AS $function$
      begin
          new.tsv_document :=
                      setweight(to_tsvector('english', coalesce(new.name,'')),'A') ||
                      setweight(to_tsvector('english', coalesce(strip_html(new.sanitised_content),'')),'B') ||
                      setweight(to_tsvector('english', replace(coalesce(strip_html(new.keywords),''),',',' ')),'C');
          return new;
      end
      $function$
  SQL
  create_function :generate_tsv_on_records, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.generate_tsv_on_records()
       RETURNS trigger
       LANGUAGE plpgsql
      AS $function$
      begin
          new.tsv_document :=
                      setweight(to_tsvector('english', coalesce(new.name,'')),'A') ||
                      setweight(to_tsvector('english', coalesce(strip_html(new.sanitised_content),'')),'B') ||
                      setweight(to_tsvector('english', replace(coalesce(strip_html(new.keywords),''),',',' ')),'C');
          return new;
      end
      $function$
  SQL
  create_function :generate_tsv_on_trails, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.generate_tsv_on_trails()
       RETURNS trigger
       LANGUAGE plpgsql
      AS $function$
      begin
          new.tsv_document :=
                      setweight(to_tsvector('english', coalesce(new.name,'')),'A') ||
                      setweight(to_tsvector('english', coalesce(strip_html(new.sanitised_content),'')),'B') ||
                      setweight(to_tsvector('english', replace(coalesce(strip_html(new.keywords),''),',',' ')),'C');
      return new;
      end
      $function$
  SQL
  create_function :populate_collection_taxonomy_terms, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.populate_collection_taxonomy_terms()
       RETURNS trigger
       LANGUAGE plpgsql
      AS $function$
      begin
      INSERT INTO humap_collections_collection_taxonomy_terms (collection_id, taxonomy_term_id, created_at, updated_at)
      SELECT
          hccr.collection_id,
          taxonomy_term_id,
          now(),
          now()
      FROM record_terms
               INNER JOIN humap_collections_collection_records hccr on record_terms.record_id = hccr.record_id
      WHERE record_terms.record_id = new.record_id;
      RETURN NULL;
      end
      $function$
  SQL
  create_function :populate_trail_taxonomy_terms, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.populate_trail_taxonomy_terms()
       RETURNS trigger
       LANGUAGE plpgsql
      AS $function$
      begin
      INSERT INTO humap_trails_trail_taxonomy_terms (trail_id, taxonomy_term_id, created_at, updated_at)
      SELECT
          httr.trail_id,
          taxonomy_term_id,
          now(),
          now()
      FROM record_terms
               INNER JOIN humap_trails_trail_records httr on record_terms.record_id = httr.record_id
      WHERE record_terms.record_id = new.record_id;
      RETURN NULL;
      end
      $function$
  SQL
  create_function :remove_collection_taxonomy_terms, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.remove_collection_taxonomy_terms()
       RETURNS trigger
       LANGUAGE plpgsql
      AS $function$
      begin
          DELETE FROM humap_collections_collection_taxonomy_terms
          WHERE id IN (
              SELECT humap_collections_collection_taxonomy_terms.id
              FROM humap_collections_collection_taxonomy_terms
                       INNER JOIN humap_collections_collections
                                  ON humap_collections_collections.id = humap_collections_collection_taxonomy_terms.collection_id
              WHERE humap_collections_collection_taxonomy_terms.taxonomy_term_id = old.taxonomy_term_id
          );
          RETURN old;
      end
      $function$
  SQL
  create_function :remove_trails_taxonomy_terms, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.remove_trails_taxonomy_terms()
       RETURNS trigger
       LANGUAGE plpgsql
      AS $function$
      begin
      DELETE FROM humap_trails_trail_taxonomy_terms
      WHERE id IN (
          SELECT humap_trails_trail_taxonomy_terms.id
          FROM humap_trails_trail_taxonomy_terms
                   INNER JOIN humap_trails_trails
                              ON humap_trails_trails.id = humap_trails_trail_taxonomy_terms.trail_id
          WHERE humap_trails_trail_taxonomy_terms.taxonomy_term_id = old.taxonomy_term_id
      );
      RETURN old;
      end
      $function$
  SQL
  create_function :strip_html, sql_definition: <<-SQL
      CREATE OR REPLACE FUNCTION public.strip_html(html text)
       RETURNS text
       LANGUAGE plpgsql
      AS $function$
      BEGIN
          return(select array_to_string(xpath('//text()', ('<root>' || html || '</root>')::xml)::text[],''));
      EXCEPTION
          WHEN invalid_xml_content THEN
              return '';


      END;
      $function$
  SQL

  create_trigger :generate_tsv_on_collections, sql_definition: <<-SQL
      CREATE TRIGGER generate_tsv_on_collections BEFORE INSERT OR UPDATE ON public.humap_collections_collections FOR EACH ROW EXECUTE FUNCTION generate_tsv_on_collections()
  SQL
  create_trigger :generate_tsv_on_records, sql_definition: <<-SQL
      CREATE TRIGGER generate_tsv_on_records BEFORE INSERT OR UPDATE ON public.records FOR EACH ROW EXECUTE FUNCTION generate_tsv_on_records()
  SQL
  create_trigger :generate_tsv_on_trails, sql_definition: <<-SQL
      CREATE TRIGGER generate_tsv_on_trails BEFORE INSERT OR UPDATE ON public.humap_trails_trails FOR EACH ROW EXECUTE FUNCTION generate_tsv_on_trails()
  SQL
  create_trigger :remove_trails_taxonomy_terms, sql_definition: <<-SQL
      CREATE TRIGGER remove_trails_taxonomy_terms BEFORE DELETE ON public.record_terms FOR EACH ROW EXECUTE FUNCTION remove_trails_taxonomy_terms()
  SQL
  create_trigger :populate_trail_taxonomy_terms, sql_definition: <<-SQL
      CREATE TRIGGER populate_trail_taxonomy_terms AFTER INSERT ON public.record_terms FOR EACH ROW EXECUTE FUNCTION populate_trail_taxonomy_terms()
  SQL
  create_trigger :remove_collection_taxonomy_terms, sql_definition: <<-SQL
      CREATE TRIGGER remove_collection_taxonomy_terms BEFORE DELETE ON public.record_terms FOR EACH ROW EXECUTE FUNCTION remove_collection_taxonomy_terms()
  SQL
  create_trigger :populate_collection_taxonomy_terms, sql_definition: <<-SQL
      CREATE TRIGGER populate_collection_taxonomy_terms AFTER INSERT ON public.record_terms FOR EACH ROW EXECUTE FUNCTION populate_collection_taxonomy_terms()
  SQL
  create_trigger :hdb_schema_update_event_notifier, sql_definition: <<-SQL
      CREATE TRIGGER hdb_schema_update_event_notifier AFTER INSERT OR UPDATE ON hdb_catalog.hdb_schema_update_event FOR EACH ROW EXECUTE FUNCTION hdb_catalog.hdb_schema_update_event_notifier()
  SQL
  create_trigger :event_trigger_table_name_update_trigger, sql_definition: <<-SQL
      CREATE TRIGGER event_trigger_table_name_update_trigger AFTER UPDATE ON hdb_catalog.hdb_table FOR EACH ROW EXECUTE FUNCTION hdb_catalog.event_trigger_table_name_update()
  SQL
end
