p "Creating Super Admin User"
FactoryBot.create(:super_admin_user, email: "super_admin@humap.dev", password: "changeme123")

p "Creating Humap Modules"
Humap.modules.each {|m| FactoryBot.create(:humap_module, name: "Humap::#{m}")}

p "Creating Tenant and switching to that Tenant"
tenant = FactoryBot.create(:tenant, name: "Coventry Atlas", slug: "coventryatlas")
Tenant.switch!(tenant)

p "Creating Tenant User"
tenant_user = FactoryBot.create(:tenant_user, email: "tenant_admin@humap.dev", password: "changeme123", tenant: tenant)

# p "Creating Taxonomies & Terms"
# FactoryBot.create_list(:taxonomy, 10)
#
# p "Creating Records with Attachments and Terms"
# FactoryBot.create_list(:record, 2, :with_randomised_attachments, :with_terms)

p "Creating Collections with Records and Terms"
FactoryBot.create_list(:collection, 1, :with_records, :with_terms)

p "Creating Trails with Records"
FactoryBot.create_list(:trail, 3, :with_records)

#
# p "Creating Overlay Types"
FactoryBot.create(:overlay_type, name: "Raster Overlay", description: "An image of a map or plan which has been georectified to fit over a map of the real world", overlay_type: :raster)
FactoryBot.create(:overlay_type, name: "Vector Overlay", description: "Vector or point data to render as an overlay", overlay_type: :vector)
#
# p "Creating Overlay Groups with Overlays"
# FactoryBot.create_list(:overlay_group, 1, :with_overlays)

# p "Creating Pages"
# homepage_content = File.open(File.join(Rails.root,"test","fixtures","short_content.html")).read
# FactoryBot.create(:page, name: "Home", page_type: "home_page", content: homepage_content)
# FactoryBot.create_list(:page, 5, :with_cta_blocks, page_type: "content_page", content: homepage_content)

p "Creating Tray Views"
FactoryBot.create(:tray_view, title: "Welcome", view_type: "introduction")
FactoryBot.create(:tray_view, title: "Help", view_type: "help")
