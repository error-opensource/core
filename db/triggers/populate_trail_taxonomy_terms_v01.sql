CREATE OR REPLACE FUNCTION populate_trail_taxonomy_terms()
    RETURNS trigger language plpgsql AS
$$
begin
INSERT INTO humap_trails_trail_taxonomy_terms (trail_id, taxonomy_term_id, created_at, updated_at)
SELECT
    httr.trail_id,
    taxonomy_term_id,
    now(),
    now()
FROM record_terms
         INNER JOIN humap_trails_trail_records httr on record_terms.record_id = httr.record_id
WHERE record_terms.record_id = new.record_id;
RETURN NULL;
end
$$;

CREATE TRIGGER populate_trail_taxonomy_terms
    AFTER INSERT
    ON record_terms
    FOR EACH ROW
    EXECUTE PROCEDURE populate_trail_taxonomy_terms();
