CREATE OR REPLACE FUNCTION remove_trails_taxonomy_terms()
    RETURNS trigger language plpgsql
    AS
$$
begin
DELETE FROM humap_trails_trail_taxonomy_terms
WHERE id IN (
    SELECT humap_trails_trail_taxonomy_terms.id
    FROM humap_trails_trail_taxonomy_terms
             INNER JOIN humap_trails_trails
                        ON humap_trails_trails.id = humap_trails_trail_taxonomy_terms.trail_id
    WHERE humap_trails_trail_taxonomy_terms.taxonomy_term_id = old.taxonomy_term_id
);
RETURN old;
end
$$;

CREATE TRIGGER remove_trails_taxonomy_terms
    BEFORE DELETE
    ON record_terms
    FOR EACH ROW
    EXECUTE PROCEDURE remove_trails_taxonomy_terms();
