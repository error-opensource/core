CREATE OR REPLACE FUNCTION populate_collection_taxonomy_terms()
    RETURNS trigger language plpgsql AS
$$
begin
INSERT INTO humap_collections_collection_taxonomy_terms (collection_id, taxonomy_term_id, created_at, updated_at)
SELECT
    hccr.collection_id,
    taxonomy_term_id,
    now(),
    now()
FROM record_terms
         INNER JOIN humap_collections_collection_records hccr on record_terms.record_id = hccr.record_id
WHERE record_terms.record_id = new.record_id;
RETURN NULL;
end
$$;

CREATE TRIGGER populate_collection_taxonomy_terms
    AFTER INSERT
    ON record_terms
    FOR EACH ROW
    EXECUTE PROCEDURE populate_collection_taxonomy_terms();
