CREATE OR REPLACE FUNCTION remove_collection_taxonomy_terms()
    RETURNS trigger language plpgsql
    AS
$$
begin
    DELETE FROM humap_collections_collection_taxonomy_terms
        WHERE id IN (
            SELECT humap_collections_collection_taxonomy_terms.id 
                FROM humap_collections_collection_taxonomy_terms 
                INNER JOIN humap_collections_collections 
                    ON humap_collections_collections.id = humap_collections_collection_taxonomy_terms.collection_id 
                WHERE humap_collections_collection_taxonomy_terms.taxonomy_term_id = old.taxonomy_term_id
        );
        RETURN NULL;
end
$$;

CREATE TRIGGER remove_collection_taxonomy_terms
    BEFORE DELETE
    ON record_terms
    FOR EACH ROW
EXECUTE PROCEDURE remove_collection_taxonomy_terms();
