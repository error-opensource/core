CREATE OR REPLACE FUNCTION generate_tsv_on_trails()
    RETURNS trigger AS
$$
begin
    new.tsv_document :=
                setweight(to_tsvector('english', coalesce(new.name,'')),'A') ||
                setweight(to_tsvector('english', coalesce(strip_html(new.sanitised_content),'')),'B') ||
                setweight(to_tsvector('english', replace(coalesce(strip_html(new.keywords),''),',',' ')),'C');
return new;
end
$$ language plpgsql;

CREATE TRIGGER generate_tsv_on_trails
BEFORE INSERT OR UPDATE
ON humap_trails_trails
FOR EACH ROW
EXECUTE PROCEDURE generate_tsv_on_trails();
