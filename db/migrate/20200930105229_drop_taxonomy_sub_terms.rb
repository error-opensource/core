class DropTaxonomySubTerms < ActiveRecord::Migration[6.0]
  def change
    drop_table :record_sub_terms
    drop_table :taxonomy_sub_terms
  end
end
