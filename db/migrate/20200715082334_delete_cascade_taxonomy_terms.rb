class DeleteCascadeTaxonomyTerms < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key "taxonomy_terms", "taxonomies"
    add_foreign_key "taxonomy_terms", "taxonomies", on_delete: :cascade
  end
end
