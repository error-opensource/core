class UpdateTriggerGenerateTsvOnRecordsToVersion2 < ActiveRecord::Migration[5.0]
  def change
    update_trigger :generate_tsv_on_records, on: :records, version: 2, revert_to_version: 1
  end
end
