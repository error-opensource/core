class AddImageIdToCollections < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_collections_collections, :image_id, :integer
  end
end
