class AddTenantToTenantUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :tenant_users, :tenant, null: false, foreign_key: true
  end
end
