class AddHomepageFieldsToHumapPagesPages < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_pages_pages, :highlighted_content_intro, :text
    add_column :humap_pages_pages, :description, :text
    add_column :humap_pages_pages, :search_image_id, :integer
  end
end
