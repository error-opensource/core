class CreateTaxonomyTerms < ActiveRecord::Migration[6.0]
  def change
    create_table :taxonomy_terms do |t|
      t.string :name, null: false
      t.references :taxonomy, null: false, foreign_key: true

      t.timestamps
    end
  end
end
