class AddKeywordsToRecordsCollectionsAndOverlays < ActiveRecord::Migration[6.0]
  def change
    add_column :records, :keywords, :text, null: true
    add_column :humap_collections_collections, :keywords, :text, null: true
    add_column :humap_overlays_overlays, :keywords, :text, null: true
  end
end
