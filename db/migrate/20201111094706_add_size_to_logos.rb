class AddSizeToLogos < ActiveRecord::Migration[6.0]
  def change
    add_column :logos, :size, :integer, null: false, default: 0
  end
end
