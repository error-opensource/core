class CreateSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :settings do |t|
      t.string :name, null: false
      t.jsonb  :default_value, null: false, index: {using: :gin}, default: '{}'

      t.timestamps
    end
  end
end
