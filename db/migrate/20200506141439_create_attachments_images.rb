class CreateAttachmentsImages < ActiveRecord::Migration[6.0]
  def change
    create_table :attachments_images do |t|
      t.string :name, null: false
      t.string :credit
      t.text   :description

      t.timestamps
    end
  end
end
