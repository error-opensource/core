class AddTenantIdToTaxonomyTerms < ActiveRecord::Migration[6.0]
  def change
    add_column :taxonomy_terms, :tenant_id, :integer
  end
end
