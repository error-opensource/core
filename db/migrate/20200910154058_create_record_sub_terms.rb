class CreateRecordSubTerms < ActiveRecord::Migration[6.0]
  def change
    create_table :record_sub_terms do |t|
      t.references :record, null: false, foreign_key: true, on_delete: :cascade
      t.references :taxonomy_sub_term, null: false, foreign_key: true, on_delete: :cascade

      t.timestamps
    end
  end
end
