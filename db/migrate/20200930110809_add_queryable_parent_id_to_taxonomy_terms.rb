class AddQueryableParentIdToTaxonomyTerms < ActiveRecord::Migration[6.0]
  def change
    add_column :taxonomy_terms, :queryable_parent_id, :integer
  end
end
