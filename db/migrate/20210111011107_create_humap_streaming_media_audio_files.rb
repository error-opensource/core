class CreateHumapStreamingMediaAudioFiles < ActiveRecord::Migration[6.1]
  def change
    create_table :humap_streaming_media_audio_files do |t|
      t.string :name
      t.string :url
      t.text :credit

      t.timestamps
    end
  end
end
