class CreateTenantHumapModules < ActiveRecord::Migration[6.0]
  def change
    create_table :tenant_humap_modules do |t|
      t.references :tenant, null: false, foreign_key: true
      t.references :humap_module, null: false, foreign_key: true

      t.timestamps
    end
  end
end
