class AddCustomDomainSettingToTenants < ActiveRecord::Migration[6.0]
  def up

    change_column :settings, :default_value, :jsonb, null: true

    Setting.find_or_create_by(name: :custom_domain).update_attribute(:default_value, nil)

  end

  def down
    change_column :settings, :default_value, :jsonb, null: false
    Setting.find('custom_domain').destroy
  end
end
