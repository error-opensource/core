class AddIndexesToImportMetadata < ActiveRecord::Migration[6.0]
  def change
    add_index :records, "(import_metadata->>'id')"
    add_index :attachments_images, "(import_metadata->>'id')"
    add_index :attachments_video_embeds, "(import_metadata->>'id')"
    add_index :attachments_files, "(import_metadata->>'id')"
    add_index :humap_overlays_overlays, "(import_metadata->>'id')"
    add_index :humap_overlays_overlay_groups, "(import_metadata->>'id')"

  end
end
