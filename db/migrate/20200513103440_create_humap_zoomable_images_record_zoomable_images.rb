class CreateHumapZoomableImagesRecordZoomableImages < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_zoomable_images_record_zoomable_images do |t|
      t.references :record, null: false, foreign_key: true
      t.references :zoomable_image, null: false, foreign_key: {to_table: :humap_zoomable_images_zoomable_images}, index: {name: :humap_zoomable_images_id}

      t.timestamps
    end
  end
end
