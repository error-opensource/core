class ChangeStatusToStateOnHumapPagesPage < ActiveRecord::Migration[6.0]
  def change
    remove_column :humap_pages_pages, :status, :integer
    add_column :humap_pages_pages, :state, :string
  end
end
