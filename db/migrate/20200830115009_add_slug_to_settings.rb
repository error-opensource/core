class AddSlugToSettings < ActiveRecord::Migration[6.0]
  def change
    add_column :settings, :slug, :string, null: false
  end
end
