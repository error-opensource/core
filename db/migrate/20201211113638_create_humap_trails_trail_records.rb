class CreateHumapTrailsTrailRecords < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_trails_trail_records do |t|
      t.references :trail, null: false, foreign_key: {to_table: :humap_trails_trails}
      t.references :record, null: false, foreign_key: true
      t.integer :position, null: false

      t.timestamps
    end

    add_index :humap_trails_trail_records, [:trail_id, :position], unique: true
  end
end
