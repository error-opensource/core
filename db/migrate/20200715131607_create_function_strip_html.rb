class CreateFunctionStripHtml < ActiveRecord::Migration[5.0]
  def up
    create_function :strip_html
  end

  def down
    drop_function :strip_html
  end
end
