class CreateHumapTrailsTrailAttachmentsImages < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_trails_trail_attachments_images do |t|
      t.references :trail, null: false, foreign_key: {to_table: :humap_trails_trails}, index: {name: :humap_trails_attachment_trail_id}
      t.references :attachments_image, null: false, foreign_key: true, index: {name: :humap_trails_attachment_id}

      t.timestamps
    end
  end
end
