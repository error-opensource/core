class DeleteCascadeRecordCollections < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key "humap_collections_record_collections", "records"
    add_foreign_key "humap_collections_record_collections", "records", on_delete: :cascade
  end
end
