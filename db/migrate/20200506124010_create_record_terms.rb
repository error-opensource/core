class CreateRecordTerms < ActiveRecord::Migration[6.0]
  def change
    create_table :record_terms do |t|
      t.references :record, null: false, foreign_key: true
      t.references :taxonomy_term, null: false, foreign_key: true

      t.timestamps
    end
  end
end
