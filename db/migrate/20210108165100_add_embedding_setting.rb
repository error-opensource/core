class AddEmbeddingSetting < ActiveRecord::Migration[6.1]
  def up
    Setting.create(name: 'embedded', default_value: false)
  end

  def down
    Setting.find('embedded').destroy
  end
end
