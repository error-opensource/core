class AddSettingsColumnsToTenants < ActiveRecord::Migration[6.0]
  def up
    Setting.find_or_create_by(name: :netlify).update_attribute(:default_value, {
      domain: nil,
      site_id: nil,
      build_hook_url: nil
    })

    add_column :tenants, :account_settings, :jsonb, default: {}
  end

  def down
    remove_column :tenants, :account_settings
    Setting.find('netlify').destroy
  end
end
