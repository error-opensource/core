class CreateHumapOverlaysOverlayGroupOverlays < ActiveRecord::Migration[6.0]
  def up
    create_table :humap_overlays_overlay_group_overlays do |t|
      t.references :overlay, null: false, foreign_key: {to_table: :humap_overlays_overlays}
      t.references :overlay_group, null: false, foreign_key: {to_table: :humap_overlays_overlay_groups}
      t.integer :tenant_id
      t.timestamps
    end
    #
    puts "Updating overlay groups"
    Tenant.all.each do |tenant|
      puts "tenant: #{tenant.name}"
      Tenant.switch!(tenant)
      Humap::Overlays::Overlay.unscoped.each do |overlay|
        if overlay.overlay_group_id.present?
          puts overlay.name
          Humap::Overlays::OverlayGroupOverlay.create!(overlay_id: overlay.id, overlay_group_id: overlay.overlay_group_id, tenant_id: overlay.tenant_id)
        end
      end
    end


    remove_column :humap_overlays_overlays, :overlay_group_id
  end

  def down
    create_column :humap_overlays_overlays, :overlay_group_id, :integer, null: true

    # we need to copy the data from the join table back into the overlays table. We can't do this in ruby because the assocations and class aren't there
    execute <<~SQL
      update humap_overlays_overlays
      set overlay_group_id = humap_overlays_overlay_group_overlays.overlay_group_id
      from humap_overlays_overlay_group_overlays
      where humap_overlays_overlay_group_overlays.overlay_id = humap_overlays_overlays.id;
    SQL


    drop_table :humap_overlays_overlay_group_overlays
  end
end
