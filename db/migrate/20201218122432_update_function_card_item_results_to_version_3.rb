class UpdateFunctionCardItemResultsToVersion3 < ActiveRecord::Migration[5.0]
  def up
    execute <<~SQL
DROP FUNCTION card_item_results(text, text, text, integer[]);
    SQL

    create_function :card_item_results, version: 3
  end

  def down
    execute <<~SQL
DROP FUNCTION card_item_results(text, text, text, integer[]);
    SQL

    create_function :card_item_results, version: 2
  end
end
