class AddAttributesToTrayViews < ActiveRecord::Migration[6.0]
  def change
    add_column :tray_views, :highlighted_records_title, :string
    add_column :tray_views, :highlighted_records_description, :text
    add_column :tray_views, :highlighted_taxonomies_title, :string
    add_column :tray_views, :highlighted_taxonomies_description, :text
  end
end
