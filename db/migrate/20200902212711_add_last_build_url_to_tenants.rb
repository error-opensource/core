class AddLastBuildUrlToTenants < ActiveRecord::Migration[6.0]
  def change
    add_column :tenants, :last_build_url, :string, null: true
  end
end
