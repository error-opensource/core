class RemoveTenantIdFromHumapOverlaysOverlayGroupOverlays < ActiveRecord::Migration[6.0]
  def change
    remove_column :humap_overlays_overlay_group_overlays, :tenant_id, :integer, null: true
  end
end
