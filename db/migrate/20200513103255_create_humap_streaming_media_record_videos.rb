class CreateHumapStreamingMediaRecordVideos < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_streaming_media_record_videos do |t|
      t.references :record, null: false, foreign_key: true
      t.references :video_attachment, null: false, foreign_key: {to_table: :humap_streaming_media_video_attachments}, index: {name: :humap_streaming_media_video_id}

      t.timestamps
    end
  end
end
