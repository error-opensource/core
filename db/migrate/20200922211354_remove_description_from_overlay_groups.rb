class RemoveDescriptionFromOverlayGroups < ActiveRecord::Migration[6.0]
  def change
    remove_column :humap_overlays_overlay_groups, :description, :text
  end
end
