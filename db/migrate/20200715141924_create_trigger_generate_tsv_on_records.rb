class CreateTriggerGenerateTsvOnRecords < ActiveRecord::Migration[5.0]
  def change
    create_trigger :generate_tsv_on_records, on: :records
  end
end
