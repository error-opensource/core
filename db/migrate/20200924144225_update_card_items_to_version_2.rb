class UpdateCardItemsToVersion2 < ActiveRecord::Migration[6.0]
  def change
    replace_view :card_items, version: 2, revert_to_version: 1
  end
end
