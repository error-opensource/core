class UpdateCardItemsToVersion4 < ActiveRecord::Migration[6.0]
  def change
    replace_view :card_items, version: 4, revert_to_version: 3
  end
end
