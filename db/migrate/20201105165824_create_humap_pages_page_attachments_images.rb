class CreateHumapPagesPageAttachmentsImages < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_pages_page_attachments_images do |t|
      t.references :page, null: false, foreign_key: {to_table: :humap_pages_pages}, index: { name: "index_images_on_page_id" }
      t.references :image, null: false, foreign_key: {to_table: :attachments_images}, index: { name: "index_images_on_image_id"}

      t.timestamps
    end
  end
end
