class AddIndexToCollectionCentroidsAndDatesView < ActiveRecord::Migration[6.0]
  def change
    add_index :_collection_centroids_and_dates, :id, unique: true
  end
end
