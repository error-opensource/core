class DeleteCascadeOnHumapPageImages < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key "humap_pages_page_images", "attachments_images"
    add_foreign_key "humap_pages_page_images", "attachments_images", on_delete: :cascade 

    remove_foreign_key "humap_pages_page_images", "humap_pages_pages"
    add_foreign_key "humap_pages_page_images", "humap_pages_pages", column: "page_id", on_delete: :cascade
  end
end
