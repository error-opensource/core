class CreateTenantPipelines < ActiveRecord::Migration[6.0]
  def change
    create_table :tenant_pipelines do |t|
      t.references :tenant, null: false, foreign_key: true
      t.string :pipeline, index: true
      t.jsonb :settings, default: {}
      t.jsonb :log, default: [], index: {using: :gin}
      t.string :last_job_id, index: true
    end
  end
end
