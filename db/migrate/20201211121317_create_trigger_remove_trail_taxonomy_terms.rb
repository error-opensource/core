class CreateTriggerRemoveTrailTaxonomyTerms < ActiveRecord::Migration[5.0]
  def change
    create_trigger :remove_trails_taxonomy_terms, on: :record_terms
  end
end
