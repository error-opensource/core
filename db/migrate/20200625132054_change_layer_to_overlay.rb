class ChangeLayerToOverlay < ActiveRecord::Migration[6.0]
  def change
    rename_table :humap_layers_layers, :humap_overlays_overlays
    rename_table :humap_layers_layer_groups, :humap_overlays_overlay_groups
    rename_table :humap_layers_layer_types, :humap_overlays_overlay_types

    rename_column :humap_overlays_overlays, :layer_group_id, :overlay_group_id
    rename_column :humap_overlays_overlays, :layer_type_id, :overlay_type_id
  end
end
