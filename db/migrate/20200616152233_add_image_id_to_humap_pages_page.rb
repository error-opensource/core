class AddImageIdToHumapPagesPage < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_pages_pages, :image_id, :integer
  end
end
