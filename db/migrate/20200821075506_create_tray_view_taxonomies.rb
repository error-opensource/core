class CreateTrayViewTaxonomies < ActiveRecord::Migration[6.0]
  def change
    create_table :tray_view_taxonomies do |t|
      t.references :tray_view, null: false, foreign_key: true, on_delete: :cascade
      t.references :taxonomy, null: false, foreign_key: true, on_delete: :cascade

      t.timestamps
    end
  end
end
