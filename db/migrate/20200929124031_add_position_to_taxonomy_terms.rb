class AddPositionToTaxonomyTerms < ActiveRecord::Migration[6.0]
  def change
    add_column :taxonomy_terms, :position, :integer
  end
end
