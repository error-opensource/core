class AddMapLogoToTenants < ActiveRecord::Migration[6.1]
  def change
    add_column :tenants, :map_logo_id, :integer, null: true
  end
end
