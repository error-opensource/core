class CreateRecordAttachmentsImages < ActiveRecord::Migration[6.0]
  def change
    create_table :record_attachments_images do |t|
      t.references :record, null: false, foreign_key: true
      t.references :attachments_image, null: false, foreign_key: true

      t.timestamps
    end
  end
end
