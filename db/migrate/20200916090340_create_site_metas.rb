class CreateSiteMetas < ActiveRecord::Migration[6.0]
  def change
    create_table :site_metas do |t|
      t.references :tenant, null: false, foreign_key: {to_table: :tenants}
      t.string :website
      t.string :contact_email
      t.string :site_title
      t.string :twitter
      t.string :facebook
      t.string :instagram
    end
  end
end
