class CreateRecords < ActiveRecord::Migration[6.0]
  def change
    create_table :records do |t|
      t.references :user, null: false, foreign_key: true
      t.references :tenant, null: false, foreign_key: {to_table: :tenants}

      t.string :name, null: false
      t.string :slug, null: false, index: {unique: true}
      t.text   :description

      t.st_point :lonlat, geographic: true, index: {using: :gist}

      t.string   :state
      t.datetime :published_at

      t.datetime :date_from
      t.datetime :date_to



      t.timestamps
    end
  end
end
