class AddPageTypeToHumapPagesPages < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_pages_pages, :page_type, :integer, null: false, default: 0
  end
end
