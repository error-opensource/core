class CreateHumapCollectionsCollectionAttachmentsImages < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_collections_collection_attachments_images do |t|
      t.references :collection, null: false, foreign_key: {to_table: :humap_collections_collections}, index: {name: :humap_collections_attachment_collection_id}
      t.references :attachments_image, null: false, foreign_key: true, index: {name: :humap_collections_attachment_id}

      t.timestamps
    end
  end
end
