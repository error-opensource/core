class CreateTriggerGenerateTsvOnTrails < ActiveRecord::Migration[5.0]
  def change
    create_trigger :generate_tsv_on_trails, on: :humap_trails_trails
  end
end
