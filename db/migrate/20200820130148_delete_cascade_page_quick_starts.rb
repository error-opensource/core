class DeleteCascadePageQuickStarts < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key "humap_pages_page_quick_starts", "humap_pages_pages", column: "page_id"
    add_foreign_key "humap_pages_page_quick_starts", "humap_pages_pages", column: "page_id", on_delete: :cascade
  
    remove_foreign_key "humap_pages_page_quick_starts", "quick_starts"
    add_foreign_key "humap_pages_page_quick_starts", "quick_starts", on_delete: :cascade
  end  
end
