class AddLogoSizeToLogos < ActiveRecord::Migration[6.0]
  def change
    add_column :logos, :logo_size, :string, null: false, default: "regular"
  end
end
