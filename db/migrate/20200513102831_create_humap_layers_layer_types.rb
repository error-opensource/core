class CreateHumapLayersLayerTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_layers_layer_types do |t|
      t.string :name, null: false
      t.text   :description

      t.timestamps
    end
  end
end
