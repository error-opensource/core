class UpdateFunctionCardItemResultsToVersion2 < ActiveRecord::Migration[5.0]
  # def change
  #   update_function :card_item_results, version: 2, revert_to_version: 1
  # end
  #
  def up
    create_function :card_item_results, version: 2
  end

  def down
    create_function :card_item_results, version: 1
  end
end
