class CreateHumapCollectionsCollectionTaxonomyTerms < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_collections_collection_taxonomy_terms do |t|
      t.references :collection, null: false, foreign_key: {to_table: :humap_collections_collections}, index: {name: :humap_collections_collection_id}
      t.references :taxonomy_term, null: false, foreign_key: true, index: {name: :humap_collections_term_id}

      t.timestamps
    end
  end
end
