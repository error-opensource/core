class CreateTrayViewQuickStarts < ActiveRecord::Migration[6.0]
  def change
    create_table :tray_view_quick_starts do |t|
      t.references :tray_view, null: false, foreign_key: true
      t.references :quick_start, null: false, foreign_key: true

      t.timestamps
    end
  end
end
