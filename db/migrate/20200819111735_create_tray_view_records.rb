class CreateTrayViewRecords < ActiveRecord::Migration[6.0]
  def change
    create_table :tray_view_records do |t|
      t.references :tray_view, null: false, foreign_key: true
      t.references :record, null: false, foreign_key: true

      t.timestamps
    end
  end
end
