class AddTsvDocumentColumnToRecordsAndCollections < ActiveRecord::Migration[6.0]
  def change
    add_column :records, :tsv_document, :tsvector, index: :gin
    add_column :humap_collections_collections, :tsv_document, :tsvector, index: :gin
  end
end
