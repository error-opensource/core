class AddBuildStatusToTenants < ActiveRecord::Migration[6.0]
  def change
    add_column :tenants, :build_status, :integer, default: 0
    add_column :tenants, :build_log, :text, null: true
  end
end
