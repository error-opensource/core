class AddAncestryToTaxonomyTerms < ActiveRecord::Migration[6.0]
  def change
    add_column :taxonomy_terms, :ancestry, :string
    add_index :taxonomy_terms, :ancestry
  end
end
