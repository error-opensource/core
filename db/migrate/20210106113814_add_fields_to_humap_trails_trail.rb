class AddFieldsToHumapTrailsTrail < ActiveRecord::Migration[6.1]
  def change
    add_column :humap_trails_trails, :duration, :text
    add_column :humap_trails_trails, :distance, :text
  end
end
