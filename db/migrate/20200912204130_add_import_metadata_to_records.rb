class AddImportMetadataToRecords < ActiveRecord::Migration[6.0]
  def change
    add_column :records, :import_metadata, :jsonb, default: {}
  end
end
