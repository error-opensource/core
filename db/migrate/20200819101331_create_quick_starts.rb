class CreateQuickStarts < ActiveRecord::Migration[6.0]
  def change
    create_table :quick_starts do |t|
      t.references :tenant, null: false, foreign_key: {to_table: :tenants}
      t.string :title, null: false
      t.text :content
      t.string :url
      t.integer :image_id

      t.timestamps
    end
  end
end
