class CreateSiteMetaLogos < ActiveRecord::Migration[6.0]
  def change
    create_table :site_meta_logos do |t|
      t.references :site_meta, null: false, foreign_key: true
      t.references :logo, null: false, foreign_key: true

      t.timestamps
    end
  end
end
