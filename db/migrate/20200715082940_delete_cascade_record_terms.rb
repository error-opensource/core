class DeleteCascadeRecordTerms < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key "record_terms", "taxonomy_terms"
    add_foreign_key "record_terms", "taxonomy_terms", on_delete: :cascade

    remove_foreign_key "record_terms", "records"
    add_foreign_key "record_terms", "records", on_delete: :cascade
  end
end
