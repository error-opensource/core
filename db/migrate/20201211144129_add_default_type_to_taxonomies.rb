class AddDefaultTypeToTaxonomies < ActiveRecord::Migration[6.0]
  def change
    change_column :taxonomies, :display_as, :integer, default: 0
  end
end
