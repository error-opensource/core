class AddFieldsToTaxonomies < ActiveRecord::Migration[6.0]
  def change
    add_column :taxonomies, :display_as, :integer
    add_column :taxonomies, :hierarchical, :boolean
  end
end
