class CreateRecordAttachmentsVideoEmbeds < ActiveRecord::Migration[6.0]
  def change
    create_table :record_attachments_video_embeds do |t|
      t.references :record, null: false, foreign_key: true
      t.references :attachments_video_embed, null: false, foreign_key: true, index: {name: :record_attached_video_embed_id}

      t.timestamps
    end
  end
end
