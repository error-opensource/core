class AddPageLocationToHumapPagesPage < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_pages_pages, :page_location, :string
  end
end
