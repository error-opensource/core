class UpdateTriggerRemoveCollectionTaxonomyTermsToVersion2 < ActiveRecord::Migration[5.0]
  def change
    update_trigger :remove_collection_taxonomy_terms, on: :record_terms, version: 2, revert_to_version: 1
  end
end
