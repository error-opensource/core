class AddStateToHumapCollectionsCollections < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_collections_collections, :state, :string
  end
end
