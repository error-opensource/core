class CreateTrayViews < ActiveRecord::Migration[6.0]
  def change
    create_table :tray_views do |t|
      t.references :tenant, null: false, foreign_key: {to_table: :tenants}
      t.string :title, null: false
      t.text :sanitised_content
      t.integer :view_type, default: 0, null: false
      
      t.timestamps
    end
  end
end
