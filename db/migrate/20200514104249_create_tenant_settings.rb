class CreateTenantSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :tenant_settings do |t|
      t.references :tenant, null: false, foreign_key: true
      t.references :setting, null: false, foreign_key: true
      t.jsonb      :value, null: false, index: {using: :gin}

      t.timestamps
    end
  end
end
