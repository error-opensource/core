class CreateHumapStreamingMediaRecordAudioFiles < ActiveRecord::Migration[6.1]
  def change
    create_table :humap_streaming_media_record_audio_files do |t|
      t.references :record, null: false, foreign_key: true
      t.references :audio_file, null: false, foreign_key: {to_table: :humap_streaming_media_record_audio_files}, index: { name: "index_record_audio_files_on_audio_id" }
      t.timestamps
    end
  end
end
