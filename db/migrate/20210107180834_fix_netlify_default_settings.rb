class FixNetlifyDefaultSettings < ActiveRecord::Migration[6.1]
  def up
    Setting.find("netlify").update(default_value: {domain: "", site_id: "", build_hook_url: ""})
  end

  def down
    Setting.find("netlify").update(default_value: {domain: nil, site_id: nil, rebuild_hook_url: nil})
  end
end
