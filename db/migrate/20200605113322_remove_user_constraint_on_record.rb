class RemoveUserConstraintOnRecord < ActiveRecord::Migration[6.0]
  def change
    change_column_null :records, :user_id, true
  end
end
