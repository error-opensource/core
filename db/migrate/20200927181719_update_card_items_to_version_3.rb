class UpdateCardItemsToVersion3 < ActiveRecord::Migration[6.0]
  def change
    replace_view :card_items, version: 3, revert_to_version: 2
  end
end
