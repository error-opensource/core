class CreateTenantSettingsView < ActiveRecord::Migration[6.1]
  def change
    create_view :tenant_settings
  end
end
