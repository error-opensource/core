class CreateTriggerGenerateTsvOnCollections < ActiveRecord::Migration[5.0]
  def change
    create_trigger :generate_tsv_on_collections, on: :humap_collections_collections
  end
end
