class AddStateToHumapOverlaysOverlays < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_overlays_overlays, :state, :string
  end
end
