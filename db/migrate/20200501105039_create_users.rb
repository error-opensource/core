class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.references :tenant, null: false, foreign_key: {to_table: :tenants}
      t.string :username
      t.string :email

      t.timestamps
    end
    add_index :users, :username, unique: true
  end
end
