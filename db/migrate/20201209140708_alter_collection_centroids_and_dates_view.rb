class AlterCollectionCentroidsAndDatesView < ActiveRecord::Migration[6.0]
  def up
    # drop the dependents of _collection_centroids_and_dates, then the view itself
    execute <<~SQL
DROP FUNCTION card_item_results(text, text, text, integer[]);
    SQL

    drop_view :card_items
    drop_view :_collection_centroids_and_dates, materialized: true

    create_view :_collection_centroids_and_dates, materialized: true, version: 2

    # re-create the views
    replace_view :card_items, version: 4, revert_to_version: 3
    create_function :card_item_results, version: 2
  end
end
