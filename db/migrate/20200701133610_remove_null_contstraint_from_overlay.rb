class RemoveNullContstraintFromOverlay < ActiveRecord::Migration[6.0]
  def change
    change_column_null :humap_overlays_overlays, :overlay_group_id, true
  end
end
