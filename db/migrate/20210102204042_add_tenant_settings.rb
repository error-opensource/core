class AddTenantSettings < ActiveRecord::Migration[6.0]
  def up
    Setting.find_or_create_by(name: :stylesheet).update_attribute(:default_value, nil)
  end

  def down
    Setting.find_by(name: :stylesheed).try(:destroy)
  end
end
