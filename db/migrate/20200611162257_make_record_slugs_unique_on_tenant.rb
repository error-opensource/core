class MakeRecordSlugsUniqueOnTenant < ActiveRecord::Migration[6.0]
  def change
    remove_index :records, :slug
    add_index :records, [:slug, :tenant_id], unique: :true

  end
end
