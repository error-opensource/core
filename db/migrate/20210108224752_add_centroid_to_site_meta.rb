class AddCentroidToSiteMeta < ActiveRecord::Migration[6.1]
  def change
    add_column :site_metas, :centroid, :st_point, geographic: true
  end
end
