class CreateHumapStreamingMediaVideoAttachments < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_streaming_media_video_attachments do |t|
      t.string :name, null: false
      t.string :credit
      t.text   :description

      t.timestamps
    end
  end
end
