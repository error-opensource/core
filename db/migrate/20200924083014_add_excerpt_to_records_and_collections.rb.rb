class AddExcerptToRecordsAndCollections < ActiveRecord::Migration[6.0]
  def change
    add_column :records, :excerpt, :text
    add_column :humap_collections_collections, :excerpt, :text
  end
end
