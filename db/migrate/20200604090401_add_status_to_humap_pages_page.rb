class AddStatusToHumapPagesPage < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_pages_pages, :status, :integer, default: 0
  end
end
