class AddImportMetadataToAttachmentsAndOverlays < ActiveRecord::Migration[6.0]
  def change
    add_column :attachments_images, :import_metadata, :jsonb, default: {}
    add_column :attachments_files, :import_metadata, :jsonb, default: {}
    add_column :attachments_video_embeds, :import_metadata, :jsonb, default: {}
    add_column :humap_overlays_overlay_groups, :import_metadata, :jsonb, default: {}
    add_column :humap_overlays_overlays, :import_metadata, :jsonb, default: {}
  end
end
