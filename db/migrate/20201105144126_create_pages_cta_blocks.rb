class CreatePagesCtaBlocks < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_pages_cta_blocks do |t|
      t.references :page, null: false, foreign_key: {to_table: :humap_pages_pages}, index: { name: "index_cta_blocks_on_page_id" }
      t.string :title, null: false
      t.text :content
      t.string :url, null: false
      t.string :button_text, null: false
      t.string :sanitised_content

      t.timestamps
    end
  end
end
