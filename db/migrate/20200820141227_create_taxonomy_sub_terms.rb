class CreateTaxonomySubTerms < ActiveRecord::Migration[6.0]
  def change
    create_table :taxonomy_sub_terms do |t|
      t.string :name, null: false
      t.references :taxonomy_term, null: false, foreign_key: true

      t.timestamps
    end
  end
end
