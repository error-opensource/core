class CreateHumapLayersLayers < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_layers_layers do |t|
      t.references :tenant, null: false, foreign_key: {to_table: :tenants}
      t.references :layer_group, null: false, foreign_key:  {to_table: :humap_layers_layer_groups}
      t.references :layer_type, null: false, foreign_key: {to_table: :humap_layers_layer_types}

      t.string :name, null: false
      t.string :descripton
      t.string :url, null: false

      t.timestamps
    end
  end
end
