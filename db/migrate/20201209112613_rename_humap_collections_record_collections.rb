class RenameHumapCollectionsRecordCollections < ActiveRecord::Migration[6.0]
  def change
    rename_table :humap_collections_record_collections, :humap_collections_collection_records
  end
end
