class CreatePermalinks < ActiveRecord::Migration[6.1]
  def change
    create_table :permalinks do |t|
      t.string :short_url, null: false, index: {unique: true}
      t.bigint :permalinkable_id, null: false
      t.string :permalinkable_type, null: false
      t.bigint :resolution_count, default: 0

      t.timestamps


    end
    add_index :permalinks, [:permalinkable_id, :permalinkable_type]
  end
end
