class CreateCollectionCentroidsAndDates < ActiveRecord::Migration[6.0]
  def change
    create_view :_collection_centroids_and_dates, materialized: true
    add_index :_collection_centroids_and_dates, :id, unique: true
  end
end
