class RenameSanitizedContentColumns < ActiveRecord::Migration[6.0]
  def change
    rename_column :humap_pages_pages, :content, :sanitised_content
    rename_column :records, :description, :sanitised_content 
    rename_column :humap_collections_collections, :description, :sanitised_content
  end
end
