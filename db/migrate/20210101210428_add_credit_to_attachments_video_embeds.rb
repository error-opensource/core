class AddCreditToAttachmentsVideoEmbeds < ActiveRecord::Migration[6.1]
  def change
    add_column :attachments_video_embeds, :credit, :string, null: true
  end
end
