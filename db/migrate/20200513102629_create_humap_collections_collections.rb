class CreateHumapCollectionsCollections < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_collections_collections do |t|
      t.references :tenant, null: false, foreign_key: {to_table: :tenants}
      t.string :name, null: false
      t.string :slug, null: false, index: {unique: true}
      t.text   :description

      t.timestamps
    end
  end
end
