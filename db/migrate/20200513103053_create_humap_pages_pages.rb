class CreateHumapPagesPages < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_pages_pages do |t|
      t.references :tenant, null: false, foreign_key: {to_table: :tenants}
      t.string :name, null: false
      t.string :slug, null: false, index: {unique: true}
      t.text   :content

      t.timestamps
    end
  end
end
