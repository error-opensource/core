class AddTypeEnumToOverlayTypes < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_overlays_overlay_types, :overlay_type, :integer
  end
end
