class CreateHumapStreamingMediaAudioAttachments < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_streaming_media_audio_attachments do |t|
      t.string :name, null: false
      t.string :credit
      t.text   :description

      t.timestamps
    end
  end
end
