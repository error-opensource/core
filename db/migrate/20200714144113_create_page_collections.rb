class CreatePageCollections < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_pages_page_collections do |t|
      t.references :page, null: false, foreign_key: {to_table: :humap_pages_pages}, index: { name: "index_collections_on_page_id" }
      t.references :collection, null: false, foreign_key: {to_table: :humap_collections_collections}, index: { name: "index_pages_on_collcetion_id" }

      t.timestamps
    end
  end
end
