class AddDataToLegacyTenants < ActiveRecord::Migration[6.1]

  def up
    Tenant.all.each do |tenant|
      site_meta = tenant.site_meta || tenant.build_site_meta
      unless site_meta.centroid.present?
        site_meta.update({
                            lng: Rails.configuration.tenant_defaults[:starting_centroid][:lng],
                            lat: Rails.configuration.tenant_defaults[:starting_centroid][:lat],
                           centroid: "POINT(#{Rails.configuration.tenant_defaults[:starting_centroid][:lng]} #{Rails.configuration.tenant_defaults[:starting_centroid][:lat]})",
                           site_title: tenant.name
                         })
      end

    end
  end

  def down

  end
end
