class AddMetadataToOverlays < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_overlays_overlays, :metadata, :jsonb, default: {}
  end
end
