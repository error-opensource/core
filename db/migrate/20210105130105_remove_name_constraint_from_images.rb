class RemoveNameConstraintFromImages < ActiveRecord::Migration[6.1]
  def change
    change_column :attachments_images, :name, :string, null: true
  end
end
