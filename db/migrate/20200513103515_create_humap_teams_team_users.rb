class CreateHumapTeamsTeamUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_teams_team_users do |t|
      t.references :team, null: false, foreign_key: {to_table: :humap_teams_teams}, index: {name: :humap_teams_user_id}
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
