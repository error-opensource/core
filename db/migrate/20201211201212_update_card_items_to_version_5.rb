class UpdateCardItemsToVersion5 < ActiveRecord::Migration[6.0]
  def change
    replace_view :card_items, version: 5, revert_to_version: 4
  end
end
