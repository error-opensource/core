class RemoveVariantsFromImages < ActiveRecord::Migration[6.0]
  def change
    remove_column :attachments_images, :variant_urls, :jsonb
    add_column :attachments_images, :url, :string
    add_column :attachments_images, :metadata, :jsonb, default: {}
    
  end
end
