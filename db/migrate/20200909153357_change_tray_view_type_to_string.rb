class ChangeTrayViewTypeToString < ActiveRecord::Migration[6.0]
  def up
    change_column :tray_views, :view_type, :string, default: "introduction", null: false
  end

  def down
    # string type can't be recast as an int - this is here so the db doesn't explode if we have to rollback
  end
end
