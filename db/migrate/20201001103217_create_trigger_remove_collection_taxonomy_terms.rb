class CreateTriggerRemoveCollectionTaxonomyTerms < ActiveRecord::Migration[5.0]
  def change
    create_trigger :remove_collection_taxonomy_terms, on: :record_terms
  end
end
