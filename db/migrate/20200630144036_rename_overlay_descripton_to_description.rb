class RenameOverlayDescriptonToDescription < ActiveRecord::Migration[6.0]
  def change
    rename_column :humap_overlays_overlays, :descripton, :description
  end
end
