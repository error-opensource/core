class AddPositionToHumapPagesPages < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_pages_pages, :position, :integer
  end
end
