class AddIncludeImageInGalleryToRecords < ActiveRecord::Migration[6.0]
  def change
    add_column :records, :include_image_in_gallery, :boolean
  end
end
