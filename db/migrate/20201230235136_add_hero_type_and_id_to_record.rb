class AddHeroTypeAndIdToRecord < ActiveRecord::Migration[6.0]
  def change
    add_column :records, :hero_attachment_type, :string, null: true
    add_column :records, :hero_attachment_id, :integer, null: true
  end
end
