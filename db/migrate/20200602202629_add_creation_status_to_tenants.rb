class AddCreationStatusToTenants < ActiveRecord::Migration[6.0]
  def change
    add_column :tenants, :creation_status, :jsonb, null: false, default: {}
    add_column :tenants, :state, :string, null: false, default: "pending"
  end
end
