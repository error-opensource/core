class AddImageIdToRecords < ActiveRecord::Migration[6.0]
  def change
    add_column :records, :image_id, :integer
  end
end
