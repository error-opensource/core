class AddFrontendUrlToPermalinks < ActiveRecord::Migration[6.1]
  def change
    add_column :permalinks, :frontend_url, :string
  end
end
