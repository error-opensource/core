class IndexTrayViewOnViewType < ActiveRecord::Migration[6.0]
  def change
    add_index :tray_views, :view_type
  end
end
