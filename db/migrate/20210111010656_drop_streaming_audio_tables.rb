class DropStreamingAudioTables < ActiveRecord::Migration[6.1]
  def change
    drop_table :humap_streaming_media_record_audios
    drop_table :humap_streaming_media_audio_attachments
  end
end
