class CreateHumapPagesPageImages < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_pages_page_images do |t|
      t.references :page, null: false, foreign_key: {to_table: :humap_pages_pages}
      t.references :attachments_image, null: false, foreign_key: true

      t.timestamps
    end
  end
end
