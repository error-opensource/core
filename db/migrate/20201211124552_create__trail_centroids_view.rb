class CreateTrailCentroidsView < ActiveRecord::Migration[6.0]
  def change
    create_view :_trail_centroids, materialized: true
    add_index :_trail_centroids, :id, unique: true
  end
end
