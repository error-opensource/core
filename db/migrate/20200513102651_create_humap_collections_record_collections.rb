class CreateHumapCollectionsRecordCollections < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_collections_record_collections do |t|
      t.references :record, null: false, foreign_key: true
      t.references :collection, null: false, foreign_key: {to_table: :humap_collections_collections}

      t.timestamps
    end
  end
end
