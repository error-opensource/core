class DropPageHighlightedContent < ActiveRecord::Migration[6.0]
  def up
    drop_table :humap_pages_page_collections
    drop_table :humap_pages_page_records
  end

  def down
    create_table :humap_pages_page_collections do |t|
      t.references :page, null: false, foreign_key: {to_table: :humap_pages_pages}, index: { name: "index_collections_on_page_id" }
      t.references :collection, null: false, foreign_key: {to_table: :humap_collections_collections}, index: { name: "index_pages_on_collcetion_id" }

      t.timestamps
    end

    create_table :humap_pages_page_records do |t|
      t.references :page, null: false, foreign_key: {to_table: :humap_pages_pages}
      t.references :record, null: false, foreign_key: true

      t.timestamps
    end
  end
end
