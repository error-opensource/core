class DeleteCascadeRecordAttachments < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key "record_attachments_files", "records"
    add_foreign_key "record_attachments_files", "records", column: "record_id", on_delete: :cascade

    remove_foreign_key "record_attachments_images", "records"
    add_foreign_key "record_attachments_images", "records", column: "record_id", on_delete: :cascade
  
    remove_foreign_key "record_attachments_video_embeds", "records"
    add_foreign_key "record_attachments_video_embeds", "records", column: "record_id", on_delete: :cascade
  end
end
