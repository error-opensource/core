class MakeTrailRecordPositionIndexNotUnique < ActiveRecord::Migration[6.1]
  def change
    remove_index :humap_trails_trail_records, [:trail_id, :position], unique: true
    add_index :humap_trails_trail_records, [:trail_id, :position]
  end
end
