class AddFieldsToTrayView < ActiveRecord::Migration[6.0]
  def change
    add_column :tray_views, :highlighted_quick_starts_title, :string
    add_column :tray_views, :highlighted_quick_starts_description, :text
  end
end
