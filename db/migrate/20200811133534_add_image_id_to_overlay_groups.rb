class AddImageIdToOverlayGroups < ActiveRecord::Migration[6.0]
  def change
    add_column :humap_overlays_overlay_groups, :image_id, :integer
  end
end
