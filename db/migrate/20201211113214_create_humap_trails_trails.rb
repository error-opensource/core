class CreateHumapTrailsTrails < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_trails_trails do |t|
      t.references :tenant, null: false, foreign_key: true
      t.references :image, null: true, foreign_key: {to_table: :attachments_images}, index: {name: :index_images_trail_id}

      t.string :name
      t.string :slug, null: false, index: {unique: true}
      t.text :sanitised_content
      t.text :excerpt
      t.text :keywords, null: true
      t.tsvector :tsv_document, index: :gin

      t.string   :state
      t.timestamps
    end
  end
end
