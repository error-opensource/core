class CreateHumapTrailsTrailTaxonomyTerms < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_trails_trail_taxonomy_terms do |t|
      t.references :trail, null: false, foreign_key: {to_table: :humap_trails_trails}, index: {name: :humap_trails_trail_id}
      t.references :taxonomy_term, null: false, foreign_key: true, index: {name: :humap_trails_term_id}

      t.timestamps
    end
  end
end
