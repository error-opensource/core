class AddUrlToAttachmentsFiles < ActiveRecord::Migration[6.1]
  def change
    add_column :attachments_files, :url, :string
  end
end
