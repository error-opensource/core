class AddQrcodeToPermalinks < ActiveRecord::Migration[6.1]
  def change
    add_column :permalinks, :qr_code, :text, null: true
  end
end
