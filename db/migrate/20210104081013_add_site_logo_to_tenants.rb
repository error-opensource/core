class AddSiteLogoToTenants < ActiveRecord::Migration[6.0]
  def change
    add_column :tenants, :site_logo_id, :integer, null: true
  end
end
