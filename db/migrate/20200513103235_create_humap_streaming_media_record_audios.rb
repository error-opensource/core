class CreateHumapStreamingMediaRecordAudios < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_streaming_media_record_audios do |t|
      t.references :record, null: false, foreign_key: true
      t.references :audio_attachment, null: false, foreign_key: {to_table: :humap_streaming_media_audio_attachments}, index: {name: :humap_streaming_media_audio_id}

      t.timestamps
    end
  end
end
