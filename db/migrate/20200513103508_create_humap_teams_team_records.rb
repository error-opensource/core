class CreateHumapTeamsTeamRecords < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_teams_team_records do |t|
      t.references :team, null: false, foreign_key: {to_table: :humap_teams_teams}, index: {name: :humap_teams_team_id}
      t.references :record, null: false, foreign_key: true

      t.timestamps
    end
  end
end
