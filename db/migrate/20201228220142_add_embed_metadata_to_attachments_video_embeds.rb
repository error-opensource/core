class AddEmbedMetadataToAttachmentsVideoEmbeds < ActiveRecord::Migration[6.0]
  def change
    add_column :attachments_video_embeds, :metadata, :jsonb, default: {}
  end
end
