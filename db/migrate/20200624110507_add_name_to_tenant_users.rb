class AddNameToTenantUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :tenant_users, :name, :string
  end
end
