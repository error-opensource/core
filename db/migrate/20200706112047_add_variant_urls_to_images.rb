class AddVariantUrlsToImages < ActiveRecord::Migration[6.0]
  def change
    add_column :attachments_images, :variant_urls, :jsonb, default: {}
  end
end
