class CreateHumapZoomableImagesZoomableImages < ActiveRecord::Migration[6.0]
  def change
    create_table :humap_zoomable_images_zoomable_images do |t|
      t.string :name, null: false
      t.string :slug, null: false, index: {unique: true}
      t.string :credit
      t.text   :description

      t.timestamps
    end
  end
end
