# These are needed to resolve conflicts between ActionText, ActiveAdmin & FriendlyId
ActiveSupport.on_load(:action_view) do
  include ActionText::ContentHelper
  include ActionText::TagHelper
end