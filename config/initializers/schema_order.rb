
Scenic::SchemaDumper.class_eval do
  def tables(stream)
     super
  end

  def views(stream)
    if dumpable_views_in_database.any?
      stream.puts
    end

    dumpable_views_in_database.sort_by(&:name).each do |view|
      stream.puts(view.to_schema)
      indexes(view.name, stream)
    end
  end
end

Fx::SchemaDumper::Function.class_eval do
  def tables(stream)
    super
	end
end

Fx::SchemaDumper::Trigger.class_eval do
  def tables(stream)
    super
	end
end

module SchemaOrderFix
  def dump(stream)
    header(stream)
    extensions(stream)
    tables(stream)
    views(stream)
    functions(stream)
    triggers(stream)
    trailer(stream)
    stream
  end
end

ActiveRecord::SchemaDumper.prepend(SchemaOrderFix)