class StagingEmailInterceptor
  def self.delivering_email(message)
    message.to = ['hosting@humap.me']
  end
end

unless Rails.env.development? || ENV['DELIVER_EMAIL'] === 'true'
  ActionMailer::Base.register_interceptor(StagingEmailInterceptor)
end
