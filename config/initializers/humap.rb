module Humap
  # A utility method to enumerate the modules included in the project. Call `Humap.modules` to get a list of modules.
  class << self
    def modules
      Humap.constants.select {|c| Humap.const_get(c).is_a?(Module)}
    end
  end
end