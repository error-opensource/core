Netlify.configure do |config|
  config.personal_access_token = ENV['NETLIFY_TOKEN']
  config.webhook_shared_secret = ENV['HUMAP_WEBHOOK_TOKEN']
end