require 'cloudflair'


Cloudflair.configure do |config|
  config.cloudflare.auth.user_service_key = ENV["CLOUDFLARE_DEPLOYMENT_API_TOKEN"]

  # these are optional:
  # config.cloudflare.api_base_url = 'https://your_cloudflare_mock.local'
  # config.faraday.adapter = :your_preferred_faraday_adapter
  # built-in options: :logger, :detailed_logger; default: nil
  # config.faraday.logger = :logger
end



