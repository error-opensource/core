Imgix::Rails.configure do |config|
  config.imgix = {
    source: "assets-#{Rails.env.development? ? 'development' : 'production'}.#{ENV['BASE_DOMAIN']}",
    use_https: true,
    include_library_param: false
  }
end