Humap::DataPipelines.observe(/^(error|skip|pre_process|post_process).*data_pipelines/) do |name, started, finished, unique_id, data|
  log_data = {
    event: name,
    start_at: started.in_time_zone,
    data: data
  }
  #   we have a new pipeline running; remove the old logs for this pipeline / tenant combination
  if data[:pipeline].present?
    pipeline = TenantPipeline.find_by(tenant_id: data[:tenant_id], pipeline: data[:pipeline])
    if pipeline.present?
      pipeline.update(last_job_id: unique_id)
      pipeline.clear_log!
    end
  else
    pipeline = TenantPipeline.find_by(last_job_id: unique_id, tenant_id: data[:tenant_id])
  end

  pipeline.update(log: pipeline.log << log_data) if pipeline.present?
end

if Rails.env.development?
  Humap::DataPipelines.observe(/^(error|skip|pre_process|post_process).*data_pipelines/) do |name, started, finished, unique_id, data|
    Rails.logger.debug({
      event: name,
      start_at: started.in_time_zone,
      data: data
    })
    data[:error] && Rails.logger.debug(data[:error].backtrace.each(&method(:puts)))
  end
end