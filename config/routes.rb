Rails.application.routes.draw do

  # These routes handle the permalink controller, which is at go.humap.[dev|site], but actually on this application.
  #
  constraints subdomain: "go", domain: ENV['PERMALINK_DOMAIN'] do
    get "/",  to: redirect(subdomain: "admin", path: "/")
    get "/:id", to: "permalinks#redirect", as: :permalink
  end

  # backup, allow go.humap.site or go.humap.dev to hit the right place too
  get "/:id", to: "permalinks#redirect", constraints: { subdomain: "go"}

  resources :permalinks, only: [] do
    member do
      get :qr_code, defaults: {format: :svg}
    end
  end

  devise_for :super_admin_users, ActiveAdmin::Devise.config.merge(path: :super_admin)
  devise_for :tenant_users, ActiveAdmin::Devise.config.merge(
              controllers: {
                invitations: 'tenant_users/invitations',
                sessions: 'tenant_users/sessions',
                unlocks: 'tenant_users/unlocks',
                passwords: 'tenant_users/passwords',
                registrations: 'tenant_users/registrations'
              })
              
  devise_scope :tenant_user do
    get 'sign_in', to: 'devise/sessions#new'
    get 'sign_out', to: 'devise/sessions#destroy'
  end

  ActiveAdmin.routes(self)

  # This block iterates over config/frontend_routes.yml and creates named routes for the things in there.
  # The YAML uses the same interpolation patterns as I18n YAML files.
  Rails.configuration.frontend_routes.each do |resource, route|
    route_name = resource.to_s.camelize.demodulize.underscore
    direct route_name do |res|
      if res.class.to_s.underscore != resource.to_s
        raise ActionController::RoutingError, "You've passed a #{res.class} object into a #{route_name} route"
      elsif !res.respond_to?(:tenant) || !res.tenant.present?
        raise ActionController::RoutingError, "You've defined a route in frontend_routes.yml for #{res.class}, which doesn't respond to .tenant, or tenant isn't present"
      else
        interpolated_route = route.gsub(Regexp.union(I18n.config.interpolation_patterns)) do |match|
          if match == '%%'
            '%'
          else
            key = ($1 || $2 || match.tr("%{}", "")).to_sym
            value = if res.respond_to?(key)
                      res.send(key)
                    else
                      raise ActionController::RoutingError, "You've defined #{key} in the frontend routes, which the #{res.class} class doesn't respond to"
                    end
            $3 ? sprintf("%#{$3}", value) : value
          end
        end
        "https://#{res.tenant.custom_domain}/#{interpolated_route}"
      end
    end
  end


  
  root to: "admin/pages#index"

  resources :webhooks, only: [] do
    collection do
      post 'rebuild'
      post 'netlify_build_status', defaults: {format: :json}

    end
  end

  resources :tenants, only: [] do
    member do
      get 'stylesheet', defaults: {format: :scss}
    end
  end
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
