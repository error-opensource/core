require_relative 'boot'

require 'rails/all'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module App
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    #
    #
    config.autoload_paths += [Rails.root.join('lib/hasura'), Rails.root.join("lib/tenant_settings")]
    config.eager_load_paths += [Rails.root.join('lib/hasura'), Rails.root.join("lib/tenant_settings")]
    config.active_job.queue_adapter = :sidekiq
    config.active_storage.queues.purge = :assets
    config.active_storage.queues.analysis = :assets

    # Edit image sizes in config/image_sizes.yml
    config.image_sizes = config_for(:image_sizes)

    config.cache_store = :redis_cache_store, {url: ENV['REDIS_URL']}

    config.x.asset_cache_expires_in = 2.years

    config.time_zone = 'London'

    Rails.application.routes.default_url_options[:host] = (ENV['CORE_URL'] || '').gsub(%r{http.?://},'')

  #  We don't want Rails to spend time analysing images - we push that off to imgix
    Rails.application.config.active_storage.analyzers.delete ActiveStorage::Analyzer::ImageAnalyzer

    # ActiveSupport::TestCase.file_fixture_path = Rails.root.join('test', 'fixtures', 'files')
    #

    config.tenant_defaults = config_for(:tenant_defaults)
    config.frontend_routes = config_for(:frontend_routes)

    config.action_mailer.default_options = {
      from: "user-messages@#{ENV['MAILGUN_DOMAIN']}",
      reply: "email@here"
    }
  end
end
