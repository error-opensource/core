const { environment } = require('@rails/webpacker')
const jquery = require('./plugins/jquery')

// const nodeModulesLoader = environment.loaders.get('nodeModules');
// nodeModulesLoader.exclude =  /(?:@?babel(?:\/|\\{1,2}|-).+)|regenerator-runtime|core-js|webpack|mapbox-gl/;

environment.plugins.prepend('jquery', jquery)
module.exports = environment

environment.loaders.delete('nodeModules')