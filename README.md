# Humap Development Setup

## Bootstrapping the project 

#### Bundle the humap-core gems
`docker-compose run humap-core bundle`

#### Setup your rails credentials

If you're using a shared credentials file, you can override the RAILS_MASTER_KEY environment variable in docker-compose.yml (or in docker-compose.override.yml)

If using your own credentials:
`docker-compose run -e EDITOR=vim humap-core rails credentials:edit`

```
development:
  database: 
    password: postgres-password
  
test: 
  database:
    password: postgres-password

production: 
  database:
    password: postgres-password
```

_Note:_ the password should match whatever is configured in your Postgres POSTGRES_PASSWORD environment variable

#### Create the database & migrate
`docker-compose run humap-core rails db:create db:migrate`

#### Start docker services
`docker-compose up`

## Services provided by this repository

This repository provides the Rails server on http://localhost:3000.

Other services are supplied by the frontend, graphql and vector-tiles repos.

## Networking and accessing other services

There is a shared docker network across all the repositories in the Humap project, so you can access the following:

`http://humap-core:3000` Rails API

`http://graphql-engine:8080` Hasura 

`http://gatsby:8000` Frontend

`redis://redis:6379` Redis

`http://sidekiq:9292` Sidekiq
