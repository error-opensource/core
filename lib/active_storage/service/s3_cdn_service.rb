require 'active_storage/service/s3_service'
module ActiveStorage
  class Service::S3CdnService < ActiveStorage::Service::S3Service
    def url(key, expires_in:, filename:, disposition:, content_type:)
      instrument :url, key: key do |payload|
        generated_url = object_for(key).public_url(virtual_host: true).gsub('http', 'https')
        payload[:url] = generated_url
        generated_url
      end
    end
  end
end

