class TenantSettingType < ActiveModel::Type::Value
  include ActiveModel::Type::Helpers::Mutable

  def type
    :tenant_setting
  end

  def deserialize(value)
    return value unless value.is_a?(::Hash)
    JSON.parse(value.to_json, object_class: TenantSetting) rescue nil
  end

  def serialize(value)
    if value.is_a?(TenantSetting)
      value.to_hash
    else
      value
    end
  end

  def changed_in_place?(raw_old_value, new_value)
    deserialize(raw_old_value) != new_value
  end
  
end
