class TenantSetting < OpenStruct

  def to_hash
    deep_serialize(self)
  end

  private
  def deep_serialize(object)
    object.each_pair.inject({}) do |hash, (k,v)|
      hash[k] = v.is_a?(TenantSetting) ? deep_serialize(v) : v
      hash
    end
  end

end