require 'faraday'
require 'json'
require 'fileutils'
module Hasura
  
  class << self
    attr_accessor :configuration

    def configure
      self.configuration ||= Configuration.new
      yield(self.configuration)
      self.configuration.configure_connection
    end

    def post(args)
      self.configuration.post(args).body
    end

    # Get metadata from the Hasura server
    def get_metadata
      configuration.metadata = post(type: :export_metadata, args: {})
    end

    # Update the metadata on Hasura server
    def replace_metadata!
      begin
        post(type: :replace_metadata, args: configuration.metadata)
        post(type: :reload_metadata, args: {reload_remote_schemas: true})
      rescue => e
        puts "Error setting the metadata on Hasura: #{e}"
      end
      
    end
  end

  class Configuration

    ENDPOINT = "/v1/query"

    attr_accessor :admin_secret, :url

    attr_reader :connection

    def initialize
      @metadata_path = File.join(Rails.root, "config", "hasura.json")
      FileUtils.touch(@metadata_path)
      load_metadata_from_file!
    end

    def configure_connection
      @connection ||= Faraday.new({
                                    url: "#{@url}",
                                    headers: {
                                      "X-Hasura-Admin-Secret" => @admin_secret
                                    }
                                  }) do |conn|
        conn.request :json
        conn.response :json
      end
    end

    def post(args)
      @connection.post(ENDPOINT, args)
    end

    # This is for setting the metadata on the configuration object and persisting to disk
    def metadata=(metadata)
      save_metadata_to_file!(metadata)
    end

    def metadata
      load_metadata_from_file!
    end

    private

    def load_metadata_from_file!
      JSON.parse(File.open(@metadata_path, "r").read) rescue {}
    end

    def save_metadata_to_file!(metadata)
      File.open(@metadata_path, "w+") {|f| f.write(JSON.pretty_generate(metadata))}
    end


  end

end