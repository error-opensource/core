namespace :materialized_view_updates do
  desc "Update the collection centroids and dates view"
  task collection_centroids_and_dates: :environment do
    UpdateCollectionFieldsJob.perform_later
  end
end