require 'csv'
require 'faker'
namespace :bulk_load do
  task :generate_records, [:count] => :environment do |task, args|
    Rake::Task['bulk_load:generate_csv'].invoke('.', args[:count])
    Rake::Task['bulk_load:load_csv'].invoke('.')
    Rake::Task['bulk_load:remove_csv'].invoke('.')
    UpdateCollectionFieldsJob.perform_now
  end

  task :remove_csv, [:folder] do |task, args|
    records_path = File.expand_path(File.join(args[:folder] || '.', 'records.csv'))
    File.unlink(records_path) if File.exist?(records_path)
  end

  task :load_csv, [:folder] => :environment do |task, args|
    conn = ActiveRecord::Base.connection
    rc = conn.raw_connection

    records_path = File.expand_path(File.join(args[:folder] || '.', 'records.csv'))

    # Records
    rc.exec("COPY records (name, slug, lonlat, state, tenant_id, import_metadata, created_at, updated_at, date_from, date_to) FROM STDIN WITH CSV")

    file = File.open(records_path, 'r')
    while !file.eof?
      rc.put_copy_data(file.readline)
    end
    rc.put_copy_end
    file.close

    collection_name = "Bulk Collection #{rand(1..1000000)}"
    collection_slug = collection_name.parameterize

    rc.exec <<~SQL
      insert into humap_collections_collections
          (tenant_id, name, slug, created_at, updated_at, state)
      values (
              (select id from tenants limit 1),
              '#{collection_name}',
              '#{collection_slug}',
              now(),
              now(),
              'published'
             )
    SQL

    rc.exec <<~SQL
      insert into humap_collections_collection_records (record_id, collection_id, created_at, updated_at)
      (select id as record_id,
             (select id from humap_collections_collections order by id desc limit 1) as collection_id,
             now() as created_at,
             now() as updated_at
      from records
      where import_metadata @> '{"record_type": "dummy"}')
    SQL

  end

  task :generate_csv, [:folder, :count] do |task, args|
    records_path = File.expand_path(File.join(args[:folder] || '.', 'records.csv'))
    CSV.open(records_path, "w+") do |c|
      args[:count].to_i.times do |i|
        print "\r #{i + 1} / #{args[:count]}"
        name = Faker::Lorem.sentence
        slug = "#{name.parameterize}-#{rand(1..100000)}"
        date_from = Faker::Date.backward(days: rand(365*1..365*100))
        date_to = date_from.advance(days: (rand(1..365*100)))
        date_to = date_to.future? ? DateTime.now : date_to
        c << [name, slug, "POINT(#{rand(-1.605946..-1.321544)} #{rand(52.324029..52.470196)})", 'published', 1, {record_type: :dummy}.to_json, DateTime.now.strftime("%Y-%m-%d %H:%M:%S"),DateTime.now.strftime("%Y-%m-%d %H:%M:%S"), date_from.strftime("%Y-%m-%d %H:%M:%S"), date_to.strftime("%Y-%m-%d %H:%M:%S")]
      end
    end
    puts
    puts "Generated #{args[:count]} rows"
  end
end

