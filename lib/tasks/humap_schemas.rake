namespace :humap do 
  desc "Create default schemas"
  task create_schema: :environment do 
    puts "Enabling extensions"
    
    # drop PostGIS extension from public schema;
    # ActiveRecord::Base.connection.execute 'DROP EXTENSION IF EXISTS postgis;' if Rails.env.in?(%w(development test))
    ActiveRecord::Base.connection.execute 'CREATE EXTENSION IF NOT EXISTS postgis SCHEMA public;'
    ActiveRecord::Base.connection.execute 'CREATE SCHEMA IF NOT EXISTS hdb_catalog;'
    ActiveRecord::Base.connection.execute 'CREATE EXTENSION IF NOT EXISTS pgcrypto SCHEMA public;'

  end
end

Rake::Task["db:migrate"].enhance ["humap:create_schema"]

Rake::Task["db:test:purge"].enhance ["humap:create_schema"]
