namespace :hasura do
  task setup_hasura: :environment do
    Hasura.configure do |config|
      config.url = ENV['GRAPHQL-SERVICE_URL']
      config.admin_secret = ENV['HASURA_GRAPHQL_ADMIN_SECRET']
    end
  end

  desc "Download and store Hasura metadata configuration in config/hasura.json"
  task get_metadata: [:environment, :setup_hasura] do
    puts "Getting metadata to config/hasura.json"
    Hasura.get_metadata
  end

  desc "Replace the running Hasura metadata config with the contents of config/hasura.json"
  task replace_metadata: [:environment, :setup_hasura] do
    puts "Replacing Hasura config from config/hasura.json"
    Hasura.replace_metadata!
  end
end

Rake::Task["db:migrate"].enhance do
  if Rails.env.development? || Rails.env.test?
    puts "Don't forget to run rails hasura:replace_metadata to keep GraphQL in sync with the database"
  else
    Rake::Task["hasura:replace_metadata"].invoke
  end
end

