class OverlayGroupImageJob < ApplicationJob
  queue_as :default

  def perform(overlay_group)
    overlay = overlay_group.overlays.raster.first
    return unless overlay.present?
    has_data = %w(minzoom maxzoom bounds).all? {|k| overlay.metadata.has_key?(k)}
    if has_data && overlay.metadata['bounds'].is_a?(Array)
      blx, bly, trx, try = overlay.metadata['bounds']
      cx = blx + (trx-blx)/2
      cy = bly + (try-bly)/2
      zoom = overlay.metadata['maxzoom'].to_i >= 17 ? 17 : overlay.metadata['maxzoom']
      tile_size = overlay.import_metadata.has_key?('tile_size') ? overlay.import_metadata['tile_size'] : 1024
      map = Mapstatic::Map.new({zoom: zoom, lat: cy, lng: cx, height: tile_size, width: tile_size, provider: overlay.url, tile_size: tile_size})
      file = Tempfile.new.binmode
      map.render_map(file.path)
      image = Attachments::Image.new(name: overlay.name)
      image.file.attach(io: File.open(file.path,'r'), filename: "#{overlay.name.parameterize}.png")
      image.save
      overlay_group.image = image
      overlay_group.save
      overlay_group.image.save
    end
  end
end
