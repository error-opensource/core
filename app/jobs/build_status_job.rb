class BuildStatusJob < ApplicationJob
  queue_as :default

  def perform(tenant, build_url, status, log=nil)
    tenant.update({
                    build_status: status.to_sym,
                    build_log: (log if log.present?),
                    last_build_url: build_url
                             })
  end
end
