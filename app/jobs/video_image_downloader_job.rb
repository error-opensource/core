require 'open-uri'
class VideoImageDownloaderJob < ApplicationJob
  queue_as :default

  def perform(record_id)
    record = Record.unscoped.find(record_id)
    if record.hero_attachment.is_a?(Attachments::VideoEmbed) && !record.image.present?
      image_url = record.hero_attachment.metadata["thumbnail_url"]
      image_title = record.hero_attachment.metadata["title"]
      image = Attachments::Image.new(name: "Video: #{image_title}")
      image.file.attach(io: open(image_url), filename: File.basename(image_url))
      image.save
      record.image = image
      record.save
    end
  end
end
