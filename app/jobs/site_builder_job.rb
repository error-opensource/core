require 'sidekiq/api'
class SiteBuilderJob < ApplicationJob
  # This job is queued on the normal runners. The *builder* job is queued specifically on the frontend container which runs the sidekiq runners for shelling out to Gatsby.
  queue_as :default

  # Takes a Tenant object, expects it to have a slug
  def perform(tenant)
    if Rails.env.production? || ENV['RUN_SITEBUILDER'] === 'true'
      if tenant.created?
        url = tenant.netlify.build_hook_url
        res = Faraday.post(url)
        if res.success?
          tenant.update_attribute(:build_status, :building)
        end
      end
    end
    
  end
  
end
