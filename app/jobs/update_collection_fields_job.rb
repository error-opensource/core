class UpdateCollectionFieldsJob < ApplicationJob
  queue_as :default

  def perform
    Scenic.database.refresh_materialized_view(:_collection_centroids_and_dates, concurrently: true, cascade: false)
  end
end
