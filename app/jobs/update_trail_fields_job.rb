class UpdateTrailFieldsJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Scenic.database.refresh_materialized_view(:_trail_centroids, concurrently: true, cascade: false)
  end
end
