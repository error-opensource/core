module TenantCreationStatus
  extend ActiveSupport::Concern

  included do
    retry_on JobNotReadyError, attempts: 3

    before_perform do |job, block|
      tenant = job.arguments.first
      raise JobNotReadyError, "Could not start #{self.class} because predecessors have not succeeded." unless tenant.may_run_job?(self.class)
    end

    around_perform do |job, block|
      tenant = job.arguments.first
      begin
        tenant.set_creation_status(self.class, :pending)
        block.call
        tenant.set_creation_status(self.class, :complete)
      rescue
        tenant.set_creation_status(self.class, :failed)
        raise
      end

    end
  end
end