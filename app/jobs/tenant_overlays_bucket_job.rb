require 'aws-sdk-s3'

class TenantOverlaysBucketJob < ApplicationJob
  queue_as :urgent

  around_perform do |job, block|
    tenant = job.arguments.first
    begin
      tenant.set_creation_status(self.class, :pending)
      block.call
      tenant.set_creation_status(self.class, :complete)
    rescue
      tenant.set_creation_status(self.class, :failed)
      raise
    end

  end

  def perform(tenant)
    if Rails.env.production? || ENV['RUN_SITEBUILDER'] === 'true'
      bucket = ['overlays',ENV['BASE_DOMAIN']].join('.')

      s3 = Aws::S3::Client.new(region: 'eu-west-2')
      begin
        s3.get_bucket_website(bucket: bucket)
      rescue Aws::S3::Errors::NoSuchWebsiteConfiguration
        #  nothing
      end

      s3.put_object(
        bucket: bucket,
        key: "#{tenant.slug}/index.html",
        body: "Map overlays for #{tenant.name}",
        acl: "public-read",
        content_type: 'text/html'
      )
    end
  end
end
