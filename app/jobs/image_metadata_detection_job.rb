class ImageMetadataDetectionJob < ApplicationJob
  queue_as :default

  def perform(image_id)
    image = Attachments::Image.find(image_id)
    if image.file.attached?
      face_data = JSON.parse(open("#{image.url}?faces=1&fm=json").read) rescue {}

      image.update({
                                metadata: {
                                  faces_detected: face_data.has_key?('Faces'),
                                  face_data: face_data['Faces'],
                                  image_data: face_data.except('Faces')
                                }
                              })
    end
  end
end
