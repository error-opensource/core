class DataPipelineJob < ApplicationJob
  queue_as :pipelines

  def perform(tenant_pipeline)
    s = tenant_pipeline.settings.inject({}) do |h,(k,v)|
      h[k.to_sym] = v
      h
    end
    Kiba.run(tenant_pipeline.pipeline.constantize.send(:setup,**s))
  end
end
