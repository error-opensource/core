class NetlifySiteCreationJob < ApplicationJob
  # include TenantCreationStatus
  queue_as :urgent
  
  retry_on JobNotReadyError, attempts: 3
  

  around_perform do |job, block|
    tenant = job.arguments.first
    begin
      tenant.set_creation_status(self.class, :pending)
      block.call
      tenant.set_creation_status(self.class, :complete)
    rescue
      tenant.set_creation_status(self.class, :failed)
      raise
    end
  end


  def perform(tenant)
    if Rails.env.production? || ENV['RUN_SITEBUILDER'] === 'true'
      raise JobNotReadyError, "Could not start #{self.class} because predecessors have not succeeded, or this job has already run." unless tenant.may_run_creation_job?(self.class)
      domain = tenant.custom_domain
      name = [tenant.slug,'humap',ENV['HOSTING_ENVIRONMENT'],SecureRandom.hex(3)].join('-')
      site = Netlify::Site.create!({
                                 custom_domain: domain,
                                 name: name,
                                 tenant: tenant
                               })
      if site.id.present?
        build_hook = site.create_build_hook!(title: "Core build hook")
        [:deploy_building, :deploy_created, :deploy_failed].each do |event|
          site.create_hook!({type: :url, event: event, data: {url: Rails.application.routes.url_helpers.netlify_build_status_webhooks_url, signature_secret: ENV['HUMAP_WEBHOOK_TOKEN']}})
        end
        tenant.update(netlify: {
          domain: site.name,
          site_id: site.id,
          build_hook_url: build_hook.url
        })
      end
    end
  end
end
