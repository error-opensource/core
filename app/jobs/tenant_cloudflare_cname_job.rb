require 'tenant'
class TenantCloudflareCnameJob < ApplicationJob
  # include TenantCreationStatus
  queue_as :urgent

  retry_on JobNotReadyError, attempts: 3

  around_perform do |job, block|
    tenant = job.arguments.first
    begin
      tenant.set_creation_status(self.class, :pending)
      block.call
      tenant.set_creation_status(self.class, :complete)
    rescue
      tenant.set_creation_status(self.class, :failed)
      raise
    end

  end

  def perform(tenant)
    raise JobNotReadyError, "Could not start #{self.class} because predecessors have not succeeded, or this job has already run." unless tenant.may_run_creation_job?(self.class)
    if Rails.env.production? || ENV['RUN_SITEBUILDER'] === 'true'
      zone = Cloudflair.zone(ENV['CLOUDFLARE_ZONE_ID'])
      #  check the record doesn't already exist
      unless zone.dns_records.select {|r| r.name == "#{tenant.slug}.#{zone.name}"}.any?
        data = {
          type: 'CNAME',
          name: tenant.slug,
          content: [tenant.account_settings['netlify']['domain'], "netlify.app"].join("."),
          proxied: true
        }
        zone.new_dns_record(data)
      end
    end


  end

end
