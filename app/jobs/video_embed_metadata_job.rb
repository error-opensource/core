class VideoEmbedMetadataJob < ApplicationJob
  queue_as :default

  def perform(video_embed_id)
    video = Attachments::VideoEmbed.find(video_embed_id)
    oembed = OEmbed::Providers.get(video.url)
    if oembed.video?
      video.update_attribute(:metadata, oembed.fields)
    end
  end
end
