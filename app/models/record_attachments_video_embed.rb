class RecordAttachmentsVideoEmbed < ApplicationRecord
  belongs_to :record, touch: true
  belongs_to :attachments_video_embed, class_name: 'Attachments::VideoEmbed'
end
