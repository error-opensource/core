class SiteMeta < ApplicationRecord
  include TenantScope
  include TenantDataTracking

  has_many :site_meta_logos
  has_many :logos, through: :site_meta_logos

  attribute :lng, :float, default: Rails.configuration.tenant_defaults[:starting_centroid][:lng]
  attribute :lat, :float, default: Rails.configuration.tenant_defaults[:starting_centroid][:lat]

  validates :lng, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }
  validates :lat, numericality: { greater_than_or_equal_to:  -90, less_than_or_equal_to:  90 }

  accepts_nested_attributes_for :logos, allow_destroy: true

  after_initialize do
    self.lat = centroid&.lat
    self.lng = centroid&.lon
  end

  after_validation do
    self.centroid = "POINT(#{lng} #{lat})"
  end
  
end