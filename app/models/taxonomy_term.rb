class TaxonomyTerm < ApplicationRecord
  include TenantScope
  
  has_ancestry orphan_strategy: :rootify

  before_save :set_queryable_parent_id

  belongs_to :taxonomy, touch: true
  
  scope :in_taxonomy, -> (taxonomy_id) { roots.where(taxonomy_id: taxonomy_id) }

  def self.filter_by_taxonomy(taxonomy_id)
    taxonomy_id.present? ? in_taxonomy(taxonomy_id) : roots
  end

  def has_children?
    children.any?
  end

  def set_queryable_parent_id
    self.queryable_parent_id = parent_id
  end
end
