class Attachments::File < ApplicationRecord
  include DenormaliseAttachmentUrl
  has_one_attached :file, service: :s3_files
end
