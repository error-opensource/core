class Attachments::VideoEmbed < ApplicationRecord
  validates :name, presence: true
  validates :url, presence: true

  after_commit on: [:create, :update] do
    VideoEmbedMetadataJob.perform_later(self.id) if metadata.empty?
  end
end
