class Attachments::Image < ApplicationRecord
  include DenormaliseAttachmentUrl
  has_one_attached :file, service: :s3_images

  attr_accessor :metadata_update_required

  before_save do
    @metadata_update_required = true if url_changed?
  end

  after_commit on: [:create, :update], if: -> {@metadata_update_required} do
    ImageMetadataDetectionJob.perform_later(self.id)
  end
end
