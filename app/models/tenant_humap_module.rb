class TenantHumapModule < ApplicationRecord
  belongs_to :tenant, touch: true
  belongs_to :humap_module
end
