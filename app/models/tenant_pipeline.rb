class TenantPipeline < ApplicationRecord
  belongs_to :tenant
  validates :pipeline, uniqueness: {scope: :tenant}

  before_validation do
    unless self.settings.is_a?(Hash)
      self.settings = JSON.parse(self.settings) rescue {}
    end
  end

  def run!(now: false)
    if now
      DataPipelineJob.perform_now(self)
    else
      DataPipelineJob.perform_later(self)
    end
  end

  def run_now!
    run!(now: true)
  end

  def state
    error_logs.any? ? :error : :ok
  end

  def error_logs
    log.select {|entry| entry['event'].match?(/^error/)}
  end

  def clear_log!
    update_attribute(:log, [])
  end

  def log_event(event)
    event = event.is_a?(Regexp) ? event : Regexp.new(event)
    log.select {|line| line["event"] =~ event}
  end

  def log_lines(event: nil)
    if event.present?
      log_event(event).collect do |entry|
        "#{entry['start_at']} #{entry['event']} #{entry['data']}"
      end.join("\n")
    else
      log.collect do |entry|
        "#{entry['start_at']} #{entry['event']} #{entry['data']}"
      end.join("\n")
    end
  end

  def last_run
    result = ActiveRecord::Base.connection.execute <<~SQL
      select i.log->>'start_at' as date from tenant_pipelines p
      cross join lateral jsonb_array_elements(p.log) as i(log)                   
      where p.id = #{id}
      order by i.log->>'start_at'
      limit 1
    SQL

    DateTime.parse(result.first['date']) rescue nil
  end
end
