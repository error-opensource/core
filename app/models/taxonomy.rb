class Taxonomy < ApplicationRecord
  extend FriendlyId
  include TenantScope
  friendly_id :name, use: :slugged
  
  has_many :taxonomy_terms, dependent: :destroy

  accepts_nested_attributes_for :taxonomy_terms, allow_destroy: true

  # enum display_as: ["dropdown", "radio_buttons", "nested"]

  enum display_as: {
    dropdown: 0,
    radio_buttons: 1,
    nested: 2
  }
end
