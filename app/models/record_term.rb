class RecordTerm < ApplicationRecord
  belongs_to :record, touch: true
  belongs_to :taxonomy_term, touch: true
end
