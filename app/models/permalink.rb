class Permalink < ApplicationRecord
  CHARS = ('a'..'z').to_a
  NUMS = (1..9).to_a
  LENGTH = 6
  TRIES = 10

  belongs_to :permalinkable, polymorphic: true

  validates_presence_of :short_url

  before_validation if: -> {short_url.nil?} do
    generate_short_url
  end

  before_save if: -> { qr_code.nil? } do
    generate_qr_code
  end

  before_save do
    self.frontend_url = permalinkable_type.classify.constantize.unscoped do
      permalinkable.try(:frontend_url)
    end
  end

  def url
    Rails.application.routes.url_helpers.permalink_url(self)
  end

  def to_param
    short_url
  end

  private

  def new_url_candidate
    LENGTH.times.collect {rand(1..3).even? ? NUMS.sample : CHARS.sample}.join
  end

  def generate_short_url
    candidate = new_url_candidate
    try = 1
    loop do
      if self.class.unscoped.exists?(short_url: candidate)
        if try > TRIES
          raise ArgumentError, "Gave up generating permalink after 10 attempts"
        end
        candidate = new_url_candidate
        try += 1
      else
        self.short_url = candidate
        break
      end
    end
  end

  def generate_qr_code
    self.qr_code = RQRCode::QRCode.new(url).as_svg(
      offset: 0,
      color: '000',
      shape_rendering: 'crispEdges',
      module_size: 6,
      standalone: true
    )
  end
end
