class User < ApplicationRecord
    include TenantScope
    has_one :profile
    accepts_nested_attributes_for :profile, reject_if: ->(attributes){ attributes['email'].blank? }, allow_destroy: true

    validates :username, presence: true
    validates :username, uniqueness: true
end
