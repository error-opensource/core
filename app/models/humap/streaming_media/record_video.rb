class Humap::StreamingMedia::RecordVideo < ApplicationRecord
  belongs_to :record, touch: true
  belongs_to :video_attachment
end
