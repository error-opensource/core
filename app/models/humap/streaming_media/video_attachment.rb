class Humap::StreamingMedia::VideoAttachment < ApplicationRecord
  has_one :record_video
  has_one :record, through: :record_video
end
