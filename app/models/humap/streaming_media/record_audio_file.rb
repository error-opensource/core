class Humap::StreamingMedia::RecordAudioFile < ApplicationRecord
  belongs_to :record
  belongs_to :audio_file, :class_name => 'Humap::StreamingMedia::AudioFile'
end
