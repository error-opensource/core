class Humap::StreamingMedia::AudioFile < ApplicationRecord
  include DenormaliseAttachmentUrl
  has_one_attached :file, service: :s3_audio

  validates :name, presence: true

end
