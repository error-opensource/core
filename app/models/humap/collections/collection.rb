class Humap::Collections::Collection < ApplicationRecord
  extend FriendlyId
  include TenantScope
  include SetSanitisedContent
  include SetExcerpt
  include AASM
  include Permalinkable

  friendly_id :name, use: :slugged
  
  has_many :collection_records, dependent: :destroy
  has_many :records, through: :collection_records

  has_many :collection_taxonomy_terms, dependent: :destroy
  has_many :taxonomy_terms, through: :collection_taxonomy_terms

  has_many :collection_attachments_images, dependent: :destroy
  has_many :attached_images, through: :collection_attachments_images, source: :attachments_image

  belongs_to :image, class_name: "Attachments::Image", dependent: :destroy, optional: true
  accepts_nested_attributes_for :image

  has_rich_text :content

  validates :name, presence: true
  validate :has_content
  validates :excerpt, length: { maximum: 200 }

  after_commit do
    UpdateCollectionFieldsJob.perform_later
  end

  def has_content
    unless content&.body&.present?
      errors.add(:content, "can't be blank")
    end
  end

  attr_accessor :active_admin_state_event

  aasm no_direct_assignment: true, column: :state do
    state :draft, initial: true
    state :published

    event :publish do
      transitions from: :draft,
                  to: :published
    end

    event :unpublish do
      transitions from: :published,
                  to: :draft
    end
  end
end
