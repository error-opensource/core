class Humap::Collections::CollectionAttachmentsImage < ApplicationRecord
  belongs_to :collection, touch: true
  belongs_to :attachments_image, class_name: 'Attachments::Image'
end
