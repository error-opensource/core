class Humap::Collections::CollectionRecord < ApplicationRecord
  belongs_to :record, touch: true
  belongs_to :collection
end
