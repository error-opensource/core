class Humap::Collections::CollectionTaxonomyTerm < ApplicationRecord
  belongs_to :collection, touch: true
  belongs_to :taxonomy_term, touch: true
end
