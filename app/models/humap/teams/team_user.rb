class Humap::Teams::TeamUser < ApplicationRecord
  belongs_to :team, touch: true
  belongs_to :user, touch: true
end
