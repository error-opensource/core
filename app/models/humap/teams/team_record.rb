class Humap::Teams::TeamRecord < ApplicationRecord
  belongs_to :team, touch: true
  belongs_to :record, touch: true
end
