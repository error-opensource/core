class Humap::Teams::Team < ApplicationRecord
  extend FriendlyId
  include TenantScope
  friendly_id :name, use: :slugged
  
  has_many :team_users
  has_many :users, through: :team_users

  has_many :team_records
  has_many :records, through: :team_records
end
