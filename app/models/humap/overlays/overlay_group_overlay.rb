class Humap::Overlays::OverlayGroupOverlay < ApplicationRecord
  belongs_to :overlay, class_name: "Humap::Overlays::Overlay"
  belongs_to :overlay_group, class_name: "Humap::Overlays::OverlayGroup"
end
