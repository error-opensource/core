class Humap::Overlays::Overlay < ApplicationRecord
  include TenantScope
  include TenantDataTracking
  include AASM


  belongs_to :overlay_type
  # belongs_to :overlay_group, :class_name => 'Humap::Overlays::OverlayGroup'

  has_many :overlay_group_overlays, :class_name => 'Humap::Overlays::OverlayGroupOverlay', dependent: :destroy
  has_many :overlay_groups, through: :overlay_group_overlays, class_name: 'Humap::Overlays::OverlayGroup'

  scope :raster, -> {
    includes(:overlay_type).references(:overlay_type).where(overlay_type: Humap::Overlays::OverlayType.raster.first)
  }

  scope :vector, -> {
    includes(:overlay_type).references(:overlay_type).where(overlay_type: Humap::Overlays::OverlayType.vector.first)
  }

  validates :name, :url, presence: true

  aasm no_direct_assignment: true, column: :state do
    state :draft, initial: true
    state :published

    event :publish do
      transitions from: [:draft],
                  to: :published
    end

    event :unpublish do
      transitions from: [:published],
                  to: :draft
    end
  end
end
