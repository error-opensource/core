class Humap::Overlays::OverlayType < ApplicationRecord
  has_many :overlays

  enum overlay_type: [:raster, :vector]
  validates :overlay_type, presence: true, uniqueness: true

  after_save do
    overlays.each(&:touch)
  end
end
