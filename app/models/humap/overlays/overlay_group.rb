class Humap::Overlays::OverlayGroup < ApplicationRecord
  extend FriendlyId
  include TenantScope
  include TenantDataTracking
  include AASM
  include SetSanitisedContent
  include SetExcerpt

  friendly_id :name, use: :slugged
  
  has_many :overlays
  has_many :overlay_group_overlays, :class_name => 'Humap::Overlays::OverlayGroupOverlay', dependent: :destroy
  has_many :overlays, through: :overlay_group_overlays, class_name: 'Humap::Overlays::Overlay'
  belongs_to :image, class_name: "Attachments::Image", dependent: :destroy, optional: true


  
  has_rich_text :content

  validates :name, presence: true
  validate :has_content
  validate :has_published_overlays, if: -> { published?}

  accepts_nested_attributes_for :image, allow_destroy: true
  
  def has_content
    unless content&.body&.present?
      errors.add(:content, "can't be blank")
    end
  end

  def has_published_overlays
    if overlays.published.none?
      errors.add(:overlays, "need to be published before you can publish this overlay")
    end
  end

  aasm no_direct_assignment: true, column: :state do
    state :draft, initial: true
    state :published

    event :publish do
      transitions from: :draft,
                  to: :published do
        guard do
          overlays.published.any?
        end
      end
    end

    event :unpublish do
      transitions from: :published,
                  to: :draft
    end
  end

  after_commit do
    OverlayGroupImageJob.perform_later(self) unless image.present?
  end
end
