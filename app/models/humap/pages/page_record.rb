class Humap::Pages::PageRecord < ApplicationRecord
  belongs_to :page, touch: true
  belongs_to :record, touch: true
end