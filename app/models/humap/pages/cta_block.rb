class Humap::Pages::CtaBlock < ApplicationRecord
  include ActionView::Helpers::SanitizeHelper
  include ActionView::Helpers::TextHelper
  belongs_to :page
  validates_presence_of :title, :url, :button_text

  before_validation do
    if self.content.present?
      self.content = strip_tags(self.content)
      self.sanitised_content = simple_format(self.content)
    end
  end
end
