class Humap::Pages::PageAttachmentsImage < ApplicationRecord
  belongs_to :page, class_name: "Humap::Pages::Page"
  belongs_to :image, class_name: "Attachments::Image"
end
