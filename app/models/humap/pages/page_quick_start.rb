class Humap::Pages::PageQuickStart < ApplicationRecord
  belongs_to :page, touch: true
  belongs_to :quick_start, touch: true
end