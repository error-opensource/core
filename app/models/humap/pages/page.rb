class Humap::Pages::Page < ApplicationRecord
  extend FriendlyId
  include TenantScope
  include TenantDataTracking
  include SetSanitisedContent
  include AASM

  friendly_id :name, use: :slugged

  has_many :page_quick_starts
  has_many :quick_starts, through: :page_quick_starts

  belongs_to :image, class_name: "Attachments::Image", dependent: :destroy, optional: true
  accepts_nested_attributes_for :image

  belongs_to :search_image, class_name: "Attachments::Image", dependent: :destroy, optional: true
  accepts_nested_attributes_for :search_image

  has_many :cta_blocks, :class_name => 'Pages::CtaBlock'
  accepts_nested_attributes_for :cta_blocks, allow_destroy: true

  has_many :humap_pages_page_attachments_images, :class_name => 'Humap::Pages::PageAttachmentsImage'
  has_many :images, through: :humap_pages_page_attachments_images, class_name: "Attachments::Image"
  accepts_nested_attributes_for :images, allow_destroy: true
  
  validates_presence_of :name
  validate :homepage_already_exists

  before_destroy :homepage_must_exist

  has_rich_text :content

  enum page_type: {
    content_page: 0,
    home_page: 1
  }

  LOCATIONS = {"Main Navigation": "main", "Footer Navigation": "footer"}

  attr_accessor :active_admin_state_event

  aasm no_direct_assignment: true, column: :state do
    state :draft, initial: true
    state :published

    event :publish do
      transitions from: [:draft, :published],
                  to: :published
    end

    event :unpublish do
      transitions from: [:draft, :published],
                  to: :draft
    end
  end

  def homepage_already_exists
    page = self.class.where(page_type: "home_page").where.not(id: self.id).first
    
    errors.add(:page_type, "#{page.name} is already set as the homepage") if page.present? && self.homepage?
  end

  def homepage_must_exist
    return true unless self.homepage?
    errors.add(:base, "You cannot delete the homepage - assign another page as the homepage first.")
    throw(:abort)
  end

  def homepage?
    page_type == "home_page"
  end

  def page_label 
    label = "#{name} <span class='status_tag'>#{state}</span>"
    label += "<span class='status_tag'>#{page_type.humanize}</span>" if homepage?
    label.html_safe
  end
end
