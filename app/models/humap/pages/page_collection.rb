class Humap::Pages::PageCollection < ApplicationRecord
  belongs_to :page, touch: true
  belongs_to :collection, touch: true, class_name: 'Humap::Collections::Collection'
end