class Humap::Trails::TrailTaxonomyTerm < ApplicationRecord
  belongs_to :trail, touch: true
  belongs_to :taxonomy_term, touch: true
end
