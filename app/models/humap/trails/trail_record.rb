class Humap::Trails::TrailRecord < ApplicationRecord
  belongs_to :trail
  belongs_to :record, touch: true
end
