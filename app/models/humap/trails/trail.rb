class Humap::Trails::Trail < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :scoped, scope: :tenant
  
  include TenantScope
  include SetSanitisedContent
  include SetExcerpt
  include AASM
  include Permalinkable
  
  belongs_to :user, optional: true
  belongs_to :image, class_name: "Attachments::Image", dependent: :destroy, optional: true

  has_many :trail_records, dependent: :destroy
  has_many :records, through: :trail_records

  accepts_nested_attributes_for :trail_records, allow_destroy: true

  has_many :trail_taxonomy_terms, dependent: :destroy
  has_many :taxonomy_terms, through: :trail_taxonomy_terms

  has_many :trail_attachments_images, dependent: :destroy
  has_many :attached_images, through: :trail_attachments_images, source: :attachments_image

  accepts_nested_attributes_for :image

  has_rich_text :content

  validates :name, presence: true
  validate :has_content

  before_save do
    if trail_records.map(&:position).include?(0)
      # the ordinal positions should be rendered from 1 and up, not from the 0 index
      trail_records.map {|tr| tr.position = tr.position+1}
    end
  end

  after_commit do
    UpdateTrailFieldsJob.perform_later
  end

  def has_content
    unless content&.body&.present?
      errors.add(:content, "can't be blank")
    end
  end

  attr_accessor :active_admin_state_event

  aasm no_direct_assignment: true, column: :state do
    state :draft, initial: true
    state :published

    event :publish do
      transitions from: [:draft, :published],
                  to: :published
    end

    event :unpublish do
      transitions from: [:draft, :published],
                  to: :draft
    end
  end
end
