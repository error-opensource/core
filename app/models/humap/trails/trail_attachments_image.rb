class Humap::Trails::TrailAttachmentsImage < ApplicationRecord
  belongs_to :trail
  belongs_to :attachments_image, class_name: 'Attachments::Image'
end
