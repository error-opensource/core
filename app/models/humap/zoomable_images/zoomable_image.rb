class Humap::ZoomableImages::ZoomableImage < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  
  has_one :record_zoomable_image
  has_one :record, through: :record_zoomable_image

  has_one_attached :file #, service: :s3
end
