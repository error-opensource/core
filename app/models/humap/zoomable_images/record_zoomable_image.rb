class Humap::ZoomableImages::RecordZoomableImage < ApplicationRecord
  belongs_to :record, touch: true
  belongs_to :zoomable_image
end
