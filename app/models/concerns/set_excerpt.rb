module SetExcerpt
  extend ActiveSupport::Concern

  included do
    after_validation :set_excerpt
  end

  def set_excerpt
    content_for_excerpt = excerpt.present? ? excerpt : content.body.to_s

    doc = Nokogiri::HTML(content_for_excerpt)
    self.excerpt = doc.xpath("//text()").to_s.truncate(200)
  end
end