module TenantSettings
  extend ActiveSupport::Concern

  included do
    begin
      Setting.all.each do |setting|
        attribute setting.name.to_sym, :tenant_setting, default: setting.default_value
      end
    rescue ActiveRecord::StatementInvalid, ActiveAdmin::DatabaseHitDuringLoad => e
      Rollbar.error("Error including TenantSettings into Tenant",e)
    end

    after_initialize do
      Setting.all.each do |setting|
        send(:"#{setting.name}=",account_settings[setting.name])
      end
    end
    
    before_validation do
      self.account_settings = Setting.all.inject({}) do |hash, setting|
        value = send(setting.name.to_sym)
        hash[setting.name] = value.is_a?(TenantSetting) ? value.to_hash : value
        hash
      end
    end
  end
end