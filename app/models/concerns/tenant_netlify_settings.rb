module TenantNetlifySettings
  extend ActiveSupport::Concern

  class_methods do
    def find_by_netlify_id(id)
      find_by("account_settings @> ?", {netlify: {site_id: id}}.to_json)
    end

  end

  def netlify_site
    Netlify::Site.find(netlify.site_id)
  end

end