module SetSanitisedContent
  extend ActiveSupport::Concern

  included do
    after_validation :set_sanitised_content
  end

  def set_sanitised_content
    self.sanitised_content = content.body.to_s
  end
end