module DenormaliseAttachmentUrl
  extend ActiveSupport::Concern

#   This copies the url of the current attached file (assumes the attachment is at :file) onto the model itself.
  included do
    after_commit if: -> { file.attached? && url != file.url} do
      update!(url: file.url)
    end
  end
end