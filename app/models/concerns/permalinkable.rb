module Permalinkable
  extend ActiveSupport::Concern

  included do
    has_one :permalink, as: :permalinkable, dependent: :nullify
  end

  def frontend_url
    method = self.class.to_s.demodulize.underscore + "_url"
    Rails.application.routes.url_helpers.send(method.to_sym, self)
  end

  def recreate_permalink!
    permalink.destroy if permalink
    create_permalink
  end
end