module TenantDataTracking
  extend ActiveSupport::Concern

#  this mixes into all tenant models, which will check for changes and trigger a rebuild of the contents on the frontend.
#
  included do
    after_commit do
      SiteBuilderJob.perform_later(tenant) if respond_to?(:tenant) && Tenant.site_builds_enabled?
    end
  end
end