module TenantStylesheet
  extend ActiveSupport::Concern

  included do
    validate :valid_scss
  end

  def has_stylesheet?
    stylesheet.present?
  end

  private

  def valid_scss
    if stylesheet.present?
      begin
        SassC::Engine.new(stylesheet, syntax: :scss).render
      rescue SassC::SyntaxError => e
        errors[:base] << "The custom stylesheet isn't valid: #{e}"
      end
    end
  end


end