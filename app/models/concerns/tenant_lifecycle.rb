module TenantLifecycle
  extend ActiveSupport::Concern

  included do
    include AASM

    CREATION_JOB_ORDER = [
      NetlifySiteCreationJob,
      TenantCloudflareCnameJob,
      TenantOverlaysBucketJob
    ]

    after_create do
      # NB this doesn't ensure the jobs are going to finish sequentially. For that, we need to inspect the creation_status field
      CREATION_JOB_ORDER.each do |job|
        job.perform_later(self)
      end
    end

    aasm no_direct_assignment: true, column: :state do
      state :pending, initial: true
      state :created
      state :archived

      event :create do
        transitions from: :pending,
                    to: :created,
                    if: :creation_complete?
      end

      event :archive do
        transitions from: :created, to: :archived
      end
    end
  end

  def creation_complete?
    creation_status.values.all? {|v| v.to_s == "complete"}
  end

  def may_run_creation_job?(job)
    # Is the previous job in the list marked as complete?
    index = CREATION_JOB_ORDER.index(job) - 1

    #A job may be run if its predecessor has been marked as complete, or this is the first in the list and it's NOT complete.
    (index < 0 && creation_status[CREATION_JOB_ORDER[0].to_s.underscore] != 'complete') || creation_status[CREATION_JOB_ORDER[index].to_s.underscore] == 'complete'
  end

  # Update just the k/v pair in the creation status field.
  def set_creation_status(klass,v)
    self.class.where(id: id).
      update_all(
        "creation_status = jsonb_set(creation_status, '{#{klass.to_s.underscore}}', to_json('#{v}'::text)::jsonb)"
      )
    create! if may_create?
  end

end