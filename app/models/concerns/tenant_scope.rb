module TenantScope
  extend ActiveSupport::Concern

  included do
    belongs_to :tenant, class_name: "Tenant"
    default_scope {
      Rails.logger.debug("No tenant specified. Do you need to call Tenant.switch!() ?") unless Tenant.current_id.present?
      where(tenant_id: Tenant.current_id)
    }
  end
end