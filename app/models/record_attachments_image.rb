class RecordAttachmentsImage < ApplicationRecord
  belongs_to :record, touch: true
  belongs_to :attachments_image, class_name: 'Attachments::Image'
end
