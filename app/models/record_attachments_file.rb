class RecordAttachmentsFile < ApplicationRecord
  belongs_to :record, touch: true
  belongs_to :attachments_file, class_name: 'Attachments::File'
end
