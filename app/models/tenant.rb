class Tenant < ApplicationRecord
  extend FriendlyId
  include TenantSettings
  include TenantLifecycle
  include TenantNetlifySettings
  include TenantStylesheet

  S3_REGION = "eu-west-2"

  DISALLOWED_NAMES = %w(
    www
    assets-development
    assets-production
    admin
    graphql-service
    vector-tiles
    core
    frontend
    go
  )
  
  friendly_id :name, use: :slugged
  
  has_many :tenant_humap_modules
  has_many :enabled_modules, through: :tenant_humap_modules, source: :humap_module
  has_one :site_meta, autosave: true

  before_create do
    build_site_meta({
                      centroid: "POINT(#{Rails.configuration.tenant_defaults[:starting_centroid][:lng]} #{Rails.configuration.tenant_defaults[:starting_centroid][:lat]})",
                      site_title: name
                    })
  end
  
  belongs_to :site_logo, class_name: "Attachments::Image", dependent: :destroy, optional: true
  belongs_to :map_logo, class_name: "Attachments::Image", dependent: :destroy, optional: true
  accepts_nested_attributes_for :site_logo
  accepts_nested_attributes_for :map_logo

  has_many :tenant_pipelines

  with_options dependent: :restrict_with_error do
    has_many :users
    has_many :records
    has_many :taxonomies
    has_many :collections, class_name: "Humap::Collections::Collection"
    has_many :overlay_groups, class_name: "Humap::Overlays::OverlayGroup"
    has_many :overlays, class_name: "Humap::Overlays::Overlay"
    has_many :teams, class_name: "Humap::Teams::Team"
    has_many :pages, class_name: "Humap::Pages::Page"
    has_many :tenant_users
  end

  validates :name, exclusion: { in: DISALLOWED_NAMES }
  
  enum build_status: {
    idle: 0,
    building: 1,
    fail: 2
  }

  
  def hasModule?(module_name: nil)
    enabled_modules.map(&:name).include?(module_name)
  end

  def self.disable_site_builds!
    RequestStore.store[:disable_site_builds] = true
  end

  def self.enable_site_builds!
    RequestStore.store[:disable_site_builds] = false
  end

  def self.site_builds_enabled?
    !RequestStore.store[:disable_site_builds].is_a?(TrueClass)
  end

  def self.current
    RequestStore.store[:tenant]
  end

  def self.current_id
    RequestStore.store[:tenant_id]
  end

  def self.current=(tenant)
    RequestStore.store[:tenant] = tenant
    RequestStore.store[:tenant_id] = tenant.id
  end

  def self.switch!(tenant)
    self.current = tenant
  end

  def switch!
    self.class.switch!(self)
  end

  def rebuild!
    SiteBuilderJob.perform_later(self)
  end
end
