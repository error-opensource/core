class TrayView < ApplicationRecord
  include TenantScope
  include SetSanitisedContent

  VIEW_TYPES = {"Introduction": "introduction", "Help": "help"}

  has_rich_text :content

  has_many :tray_view_taxonomies
  has_many :highlighted_taxonomies, through: :tray_view_taxonomies, source: :taxonomy

  has_many :tray_view_records
  has_many :highlighted_records, through: :tray_view_records, source: :record

  has_many :tray_view_quick_starts
  has_many :highlighted_quick_starts, through: :tray_view_quick_starts, source: :quick_start

  validate :view_already_exists

  def view_already_exists
    view = self.class.where(view_type: self.view_type).where.not(id: self.id).first
    
    errors.add(:view_type, "#{view.title} is already set as the #{view.view_type} view") if view.present? && view.view_type == self.view_type
  end
end