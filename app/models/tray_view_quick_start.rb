class TrayViewQuickStart < ApplicationRecord
  belongs_to :tray_view, touch: true
  belongs_to :quick_start, touch: true
end