class SiteMetaLogo < ApplicationRecord
  belongs_to :site_meta, touch: true
  belongs_to :logo, touch: true
end