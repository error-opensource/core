class QuickStart < ApplicationRecord
  include TenantScope
  include TenantDataTracking

  belongs_to :image, class_name: "Attachments::Image", dependent: :destroy, optional: true
  accepts_nested_attributes_for :image
end