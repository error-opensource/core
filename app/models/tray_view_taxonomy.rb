class TrayViewTaxonomy < ApplicationRecord
  belongs_to :tray_view, touch: true
  belongs_to :taxonomy, touch: true
end