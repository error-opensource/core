class TrayViewRecord < ApplicationRecord
  belongs_to :tray_view, touch: true
  belongs_to :record, touch: true
end