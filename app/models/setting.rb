class Setting < ApplicationRecord
  include FriendlyId
  validates :name, uniqueness: true

  friendly_id :name, use: :slugged

end
