class Logo < ApplicationRecord
  belongs_to :image, class_name: "Attachments::Image", dependent: :destroy
  accepts_nested_attributes_for :image

  enum size: {
    regular: 0,
    xxxsmall: 1,
    xxsmall: 2,
    xsmall: 3,
    small: 4,
    large: 5,
    xlarge: 6,
    xxlarge: 7,
    xxxlarge: 8
  }

  before_validation do
    self.logo_size = self.size
  end

end