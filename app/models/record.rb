class Record < ApplicationRecord
  include TenantScope
  include SetSanitisedContent
  include SetExcerpt
  include AASM
  include Permalinkable

  extend FriendlyId
  friendly_id :name, use: :scoped, scope: :tenant
  
  belongs_to :user, optional: true
  belongs_to :image, class_name: "Attachments::Image", dependent: :destroy, optional: true
  
  has_many :record_terms, dependent: :destroy
  has_many :taxonomy_terms, through: :record_terms

  has_many :record_attachments_files
  has_many :record_attachments_images
  has_many :record_attachments_video_embeds
  has_many :attached_files, through: :record_attachments_files, source: :attachments_file, dependent: :destroy
  has_many :attached_images, through: :record_attachments_images, source: :attachments_image, dependent: :destroy
  has_many :attached_video_embeds, through: :record_attachments_video_embeds, source: :attachments_video_embed, dependent: :destroy

  # non-core associations
  has_many :collection_records, class_name: "Humap::Collections::CollectionRecord"
  has_many :collections, through: :collection_records, class_name: "Humap::Collections::Collection"

  has_many :trail_records, class_name: "Humap::Trails::TrailRecord"
  has_many :trails, through: :trail_records, class_name: "Humap::Trails::Trail"

  has_many :record_audio_files, class_name: 'Humap::StreamingMedia::RecordAudioFile'
  has_many :audio_files, through: :record_audio_files, class_name: "Humap::StreamingMedia::AudioFile"

  has_rich_text :content

  attr_accessor :active_admin_state_event, :longitude, :latitude

  validates :longitude, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }
  validates :latitude, numericality: { greater_than_or_equal_to:  -90, less_than_or_equal_to:  90 }
  validates :name, presence: true
  validate :date_to_is_after_date_from
  validates :excerpt, length: { maximum: 200 }

  accepts_nested_attributes_for :image, allow_destroy: true
  accepts_nested_attributes_for :attached_images, allow_destroy: true
  accepts_nested_attributes_for :attached_files, allow_destroy: true
  accepts_nested_attributes_for :attached_video_embeds, allow_destroy: true

  before_validation do
    if image.present?
      attached_images << image unless attached_images.include?(image)
    end

    if hero_attachment.present?
      self.hero_attachment_type = hero_attachment.class.to_s.demodulize.underscore
      self.hero_attachment_id = hero_attachment&.id
    end

    if hero_attachment.is_a?(Attachments::VideoEmbed) && !image.present?
      VideoImageDownloaderJob.perform_later(self.id)
    end
    
  end

  after_commit do
    UpdateCollectionFieldsJob.perform_later
  end

  def date_to_is_after_date_from
    return if date_from.blank? || date_to.blank?
  
    if date_to < date_from
      errors.add(:date_to, "can't be before 'date from'") 
    end 
  end

  aasm no_direct_assignment: true, column: :state do
    state :draft, initial: true
    state :published

    event :publish do
      transitions from: :draft,
                  to: :published,
                  after: -> {
                    update_attribute(:published_at, published_at ||= DateTime.now)
                  }
    end

    event :unpublish do
      transitions from: :published,
                  to: :draft
    end
  end

  def self.to_geojson
    raise ArgumentError, "You need to set a current tenant" unless Tenant.current.present?
    result = ActiveRecord::Base.connection.execute <<~SQL
      select
          row_to_json(json) as geojson
          FROM ( 
            SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
              FROM (
                SELECT 'Feature' As type
                , ST_AsGeoJSON(records.lonlat)::json As geometry
                , row_to_json(
                    (
                      SELECT fields FROM (
                        SELECT id, name, state, created_at
                      ) AS fields ) 
                ) AS properties
                FROM records
                WHERE records.tenant_id = #{Tenant.current.id} 
              ) 
            AS f 
          )
          AS json
    SQL
    result.to_json
  end

  def set_lonlat(lon, lat)
    self.lonlat = "POINT(#{lon} #{lat})"
  end

  def longitude
    lonlat&.x
  end

  def latitude
    lonlat&.y
  end

  # This is where we determine what kind of attachment is the 'hero' one, displayed at the top of the record tray on the frontend.
  def hero_attachment
    if attached_video_embeds.any?
      attached_video_embeds.first
    elsif image.present?
      image
    end
  end
end
