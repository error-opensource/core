$(function() {
  if ($('input[name=permalink]') && window.location.hash === "#sharing") {
    $('input[name=permalink]').attr('disabled',false).select().attr('disabled', true);
  }
})