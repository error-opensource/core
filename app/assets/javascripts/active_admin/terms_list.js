$(function() {
  $('input.has-subterms').each(function() {
    if(!this.checked) {
      $(`.subterms#${this.id}`).hide()
    }
  });

  $('input.has-subterms').change(function() {
    if(this.checked) {
      $(`.subterms#${this.id}`).show()
    } else {
      $(`.subterms#${this.id}`).hide()
      $(`.subterms#${this.id} input`).each(function() {
        $(this).prop('checked', false)
      }) 
    }
  })
})