ActiveAdmin.register Humap::Overlays::Overlay, as: "Overlay" do
  extend Publishable
  is_publishable

  permit_params :tenant_id, :name, :slug, :content, :url, :overlay_type_id, :active_admin_state_event, overlay_group_ids: []

  form partial: 'form'

  menu priority: 40

  filter :name_contains, label: "Name"
  filter :state, as: :select



  controller do
    def show
      redirect_to edit_admin_overlay_path(resource)
    end
  end

  index do
    selectable_column
    column :name
    column :state
    column :overlay_groups
    column :created_at
    
    actions defaults: false do |overlay|
      item 'Edit', edit_admin_overlay_path(overlay), class: 'member_link'
      item 'Delete', admin_overlay_path(overlay), class: 'member_link', method: :delete
    end
  end
  
end
