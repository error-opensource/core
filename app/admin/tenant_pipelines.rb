ActiveAdmin.register TenantPipeline, namespace: :super_admin do

  belongs_to :tenant, parent_class: Tenant, class_name: "Tenant"
  # navigation_menu :pipelines

  permit_params :pipeline, :settings

  form partial: 'form'

  config.filters = false
  #
  # controller do
  #   def scoped_collection
  #     end_of_association_chain.where(tenant_id: params[:tenant_id])
  #   end
  #
  #   def find_resource
  #     scoped_collection.where(tenant_id: params[:tenant_id])
  #   end
  # end

  member_action :run, method: :post do
    resource.run!
    redirect_to resource_path, notice: "Pipeline has started"
  end

  action_item :run, only: :show do
    link_to 'Run', run_super_admin_tenant_tenant_pipeline_path(resource.tenant, resource), method: :post
  end

  index do
    
    column :pipeline
    column :last_run
    column "Status", :state do |pipeline|
      status_tag(pipeline.state)
    end

    actions defaults: false do |pipeline|
      item 'View', super_admin_tenant_tenant_pipeline_path(pipeline.tenant, pipeline), class: 'member_link'
      item 'Edit', edit_super_admin_tenant_tenant_pipeline_path(pipeline.tenant,pipeline), class: 'member_link'
      item 'Run', run_super_admin_tenant_tenant_pipeline_path(pipeline.tenant, pipeline), class: 'member_link', method: :post
    end
  end

  show do
    attributes_table do
      row :pipeline
      row :settings
      row :state do |pipeline|
        status_tag(pipeline.state)
      end
    end

    panel "Pipeline Log" do
      pre class: "build-log" do
        resource.try(:log_lines)
      end
    end
  end
end
