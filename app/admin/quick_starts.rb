ActiveAdmin.register QuickStart do
  form partial: 'form'

  menu priority: 10

  config.filters = false

  permit_params :title, 
                :content,
                :url,
                image_attributes: [
                  :id,
                  :file,
                  :name
                ]

  controller do
    def new
      @quick_start = QuickStart.new(image: Attachments::Image.new)
    end

    def edit
      super do
        @quick_start.image = Attachments::Image.new unless @quick_start.image.present? 
      end
    end

    def show
      redirect_to edit_admin_quick_start_path(resource)
    end
  end

  index do
    column :title
    column :created_at
    actions defaults: false do |quick_start|
      item 'Edit', edit_admin_quick_start_path(quick_start), class: 'member_link'
      item 'Delete', admin_quick_start_path(quick_start), class: 'member_link', method: :delete
    end
  end
end
