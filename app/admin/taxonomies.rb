ActiveAdmin.register Taxonomy do

  permit_params :name, 
                :description,
                :display_as, 
                taxonomy_terms_attributes: [:id, :name, :description, :_destroy]

  form partial: 'form'

  menu priority: 60

  actions :all, except: :show

  config.filters = false  

  controller do
    def show
      redirect_to edit_admin_taxonomy_path(resource)
    end
  end

  index do
    column :name
    column :taxonomy_terms
    column :created_at
    
    actions defaults: false do |taxonomy|
      item 'Edit', edit_admin_taxonomy_path(taxonomy), class: 'member_link'
      item 'Delete', admin_taxonomy_path(taxonomy), class: 'member_link', method: :delete
    end
  end
end
