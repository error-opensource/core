ActiveAdmin.register Record do
  extend Publishable
  is_publishable
  extend HasPermalink
  has_permalink

  permit_params :user_id, 
                :name, 
                :content, 
                :active_admin_state_event, 
                :longitude, 
                :latitude, 
                :date_from, 
                :date_to,
                :excerpt,
                image_attributes: [
                  :id,
                  :file,
                  :name,
                  :credit,
                  :_destroy
                ],
                attached_images_attributes: [
                  :id,
                  :file,
                  :name,
                  :credit,
                  :_destroy
                ],
                attached_files_attributes: [
                  :id,
                  :file,
                  :name,
                  :credit,
                  :_destroy
                ],
                attached_video_embeds_attributes: [
                  :id,
                  :name,
                  :url,
                  :_destroy
                ],
                taxonomy_term_ids: []
                
  form partial: 'form'

  menu priority: 20

  filter :name_contains, label: "Name"
  filter :state, as: :select

  index do
    selectable_column
    column :name
    column "Status", :state
    column :created_at
    
    actions defaults: false do |record|
      item 'Edit', edit_admin_record_path(record), class: 'member_link'
      item 'Delete', admin_record_path(record), class: 'member_link', method: :delete
    end
  end

  before_save do |record|
    record.set_lonlat(permitted_params[:record][:longitude], permitted_params[:record][:latitude])
  end

  controller do
    def new
      @record = Record.new(image: Attachments::Image.new)
    end

    def edit
      super do
        @record.image = Attachments::Image.new unless @record.image.present? 
      end
    end

    def show
      redirect_to edit_admin_record_path(resource)
    end
  end

  after_save do |record|
    event = params[:record][:active_admin_state_event]
    unless event.blank?
      safe_event = (record.aasm.events.map(&:name) & [event.to_sym]).first
      raise "Forbidden event #{event} requested on instance #{record.id}" unless safe_event
      record.send("#{event}!")
    end
  end
end
