ActiveAdmin.register_page "Site Settings" do

  menu priority: 100

  content do
    @site_meta = Tenant.current.site_meta ? Tenant.current.site_meta : SiteMeta.new
    render partial: 'form', locals: {site_meta: @site_meta} 
  end
end