ActiveAdmin.register Humap::Collections::Collection, as: "Collection" do
  extend Publishable
  is_publishable
  extend HasPermalink
  has_permalink

  form partial: 'form'

  menu priority: 30

  permit_params :tenant_id, 
                :name, 
                :slug, 
                :content, 
                :excerpt,
                image_attributes: [
                  :id,
                  :file,
                  :name,
                  :_destroy
                ], 
                record_ids: []

  filter :name_contains, label: "Name"
  filter :state, as: :select

  controller do
    def new
      @collection = Humap::Collections::Collection.new(image: Attachments::Image.new)
    end

    def edit
      super do
        @collection.image = Attachments::Image.new unless @collection.image.present? 
      end
    end

    def show
      redirect_to edit_admin_collection_path(resource)
    end
  end

  index do
    selectable_column
    column :name
    column :state
    column :created_at
    
    actions defaults: false do |collection|
      item 'Edit', edit_admin_collection_path(collection), class: 'member_link'
      item 'Delete', admin_collection_path(collection), class: 'member_link', method: :delete
    end
  end

end
