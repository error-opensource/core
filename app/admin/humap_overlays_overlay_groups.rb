ActiveAdmin.register Humap::Overlays::OverlayGroup, as: "Overlay Group" do
  extend Publishable
  is_publishable

  form partial: 'form'

  menu priority: 50

  actions :all, except: :show

  permit_params :name, 
                :content,
                :excerpt, 
                image_attributes: [
                  :id,
                  :file,
                  :name,
                  :_destroy
                ],
                overlay_ids: []

  filter :name_contains, label: "Name"
  filter :state, as: :select

  controller do
    def new 
      @overlay_group = Humap::Overlays::OverlayGroup.new(image: Attachments::Image.new)
    end

    def edit
      super do
        @overlay_group.image = Attachments::Image.new unless @overlay_group.image.present? 
      end
    end

    def show
      redirect_to edit_admin_overlay_group_path(resource)
    end
  end

  index do
    selectable_column
    column :name
    column :state
    column :overlays
    column :created_at
    
    actions defaults: false do |overlay_group|
      item 'Edit', edit_admin_overlay_group_path(overlay_group), class: 'member_link'
      item 'Delete', admin_overlay_group_path(overlay_group), class: 'member_link', method: :delete
    end
  end

end
