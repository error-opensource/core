ActiveAdmin.register TaxonomyTerm do

  filter :taxonomy

  config.batch_actions = false

  sortable tree: true,
           max_levels: 2,
           sorting_attribute: :position,
           parent_method: :parent,
           children_method: :children,
           roots_collection: proc { TaxonomyTerm.filter_by_taxonomy(params.dig("q", "taxonomy_id_eq")) },
           collapsible: false,
           start_collapsed: false

  permit_params :name, :taxonomy_id, :order, q: [:taxonomy_id_eq], taxonomy_terms: []

  form partial: 'form'

  menu priority: 65

  index as: :sortable do
    label :name
    # the view button is hidden with css, because this sortable-tree gem doesn't let you filter the actions properly
    actions defaults: true do |term|
      link_to "Taxonomy: #{term.taxonomy.name}", admin_taxonomy_terms_path(q: {taxonomy_id_eq: term.taxonomy.id}, commit: "Filter"), class: "member_link taxonomy_link"
    end
  end

  controller do
    skip_before_action :verify_authenticity_token, only: [:sort]
  end
end
 