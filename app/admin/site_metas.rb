ActiveAdmin.register SiteMeta do
  permit_params :tenant_id,   
                :website, 
                :contact_email, 
                :site_title, 
                :twitter, 
                :facebook, 
                :instagram,
                :lng,
                :lat,
                logos_attributes: [
                  :id,
                  :link_to,
                  :size,
                  :_destroy,
                  image_attributes: [
                    :id,
                    :file,
                    :name,
                    :_destroy
                  ]
                ]
                
  menu false
  
  controller do
    def index
      redirect_to admin_site_settings_path
    end

    def show
      redirect_to admin_site_settings_path
    end
  end
end
