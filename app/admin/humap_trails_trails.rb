ActiveAdmin.register Humap::Trails::Trail, as: "Trail" do
  extend Publishable
  is_publishable
  extend HasPermalink
  has_permalink

  form partial: 'form'

  menu priority: 30

  permit_params :tenant_id, 
                :name, 
                :slug, 
                :content, 
                :excerpt,
                :active_admin_state_event,
                :duration, 
                :distance,
                image_attributes: [
                  :id,
                  :file,
                  :name,
                  :_destroy
                ], 
                trail_records_attributes: [
                  :id,
                  :record_id,
                  :position,
                  :_destroy
                ]

  config.filters = false

  controller do
    def new
      @trail = Humap::Trails::Trail.new(image: Attachments::Image.new)
    end

    def edit
      super do
        @trail.image = Attachments::Image.new unless @trail.image.present?
      end
    end
    
    def show
      redirect_to edit_admin_trail_path(resource)
    end
  end

  breadcrumb do 
    links = [
      link_to('Admin', admin_root_path),
      link_to('Trails', admin_trails_path)
    ]

    if %(show edit).include?(params[:action])
      links << link_to(resource.name, edit_admin_trail_path)
    end
  end

  index do
    column :name
    column :state
    column :created_at
    
    actions defaults: false do |trail|
      item 'Edit', edit_admin_trail_path(trail), class: 'member_link'
      item 'Delete', admin_trail_path(trail), class: 'member_link', method: :delete
    end
  end
end
