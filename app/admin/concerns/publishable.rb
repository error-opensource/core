module Publishable

  def is_publishable

    singular = config.resource_name.singular
    plural = config.resource_name.plural

    batch_action :publish do |ids|
      batch_action_collection.find(ids).each do |resource|
        resource.publish! if resource.may_publish?
      end
      flash[:notice] = "Your #{plural} have been published"
      redirect_to send(:"admin_#{plural}_path")
    end

    batch_action :unpublish do |ids|
      batch_action_collection.find(ids).each do |resource|
        resource.unpublish! if resource.may_unpublish?
      end
      flash[:notice] = "Your #{plural} have been set to draft"
      redirect_to send(:"admin_#{plural}_path")
    end

    member_action :publish, method: :put do
      resource.publish! if resource.may_publish?
      redirect_to resource_path, notice: "Your #{singular} has been published"
    end

    member_action :unpublish, method: :put do
      resource.unpublish! if resource.may_unpublish?
      redirect_to resource_path, notice: "Your #{singular} has been unpublished"
    end

    action_item :publish,
                only: [:edit],
                if: -> { resource.may_publish?} do
      link_to "Publish", send(:"publish_admin_#{singular}_path", resource), method: :put
    end

    action_item :unpublish,
                only: [:edit],
                if: -> { resource.may_unpublish? } do
      link_to "Unpublish", send(:"unpublish_admin_#{singular}_path", resource), method: :put
    end
  end
end