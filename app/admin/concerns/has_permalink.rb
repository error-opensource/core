module HasPermalink
  def has_permalink
    singular = config.resource_name.singular

    member_action :create_permalink, method: :put do
      resource.create_permalink unless resource.permalink.present?
      redirect_to resource_path(anchor: "sharing"), notice: "A permalink for your #{singular} has been created"
    end

    member_action :recreate_permalink, method: :put do
      resource.recreate_permalink! if resource.permalink.present?
      redirect_to resource_path(anchor: "sharing"), notice: "A permalink for your #{singular} has been re-created"
    end
  end
end