ActiveAdmin.register TrayView do
  form partial: 'form'

  menu priority: 10

  config.filters = false

  permit_params :title, 
                :content, 
                :view_type, 
                :highlighted_records_title,  
                :highlighted_taxonomies_title,
                :highlighted_taxonomies_description,
                :highlighted_quick_starts_title,
                :highlighted_quick_starts_description,
                highlighted_record_ids: [], 
                highlighted_taxonomy_ids: [],
                highlighted_quick_start_ids: []

  controller do
    def show
      redirect_to edit_admin_tray_view_path(resource)
    end
  end

  index do
    column :title
    column "Tray View Type" do |view| 
      view.view_type.to_s.humanize
    end
    column :created_at
    
    actions defaults: false do |view|
      item 'Edit', edit_admin_tray_view_path(view), class: 'member_link'
    end
  end
end
