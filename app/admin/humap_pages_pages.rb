ActiveAdmin.register Humap::Pages::Page, as: "Page" do
  
  form partial: 'form'

  menu priority: 10

  config.filters = false

  config.batch_actions = false

  permit_params :name, 
                :content,
                :active_admin_state_event,
                :page_type,
                :description,
                :highlighted_content_intro,
                :page_location,
                :position,
                image_attributes: [
                  :id,
                  :file,
                  :name
                ],
                images_attributes: [
                  :id,
                  :file,
                  :name,
                  :_destroy
                ],
                search_image_attributes: [
                  :id,
                  :file,
                  :name
                ],
                quick_start_ids: [],
                cta_blocks_attributes: [
                  :title,
                  :url,
                  :content,
                  :button_text,
                  :id,
                  :_destroy
                ]

  sortable

  controller do
    def new
      @page = Humap::Pages::Page.new(image: Attachments::Image.new, search_image: Attachments::Image.new)
    end

    def edit
      super do
        @page.image = Attachments::Image.new unless @page.image.present?
        @page.search_image = Attachments::Image.new unless @page.search_image.present?
      end
    end

    def show
      redirect_to edit_admin_page_path(resource)
    end

    def destroy
      destroy! do |success, failure|
        failure.html do
          flash.now[:alert] = "#{flash.now[:alert]} #{resource.errors.full_messages.to_sentence}"
          render action: :index
        end
      end
    end
  end

  index as: :sortable do
    label :page_label
    actions
  end

  after_save do |page|
    event = params[:humap_pages_page][:active_admin_state_event]
    unless event.blank?
      safe_event = (page.aasm.events.map(&:name) & [event.to_sym]).first
      raise "Forbidden event #{event} requested on instance #{page.id}" unless safe_event
      page.send("#{event}!")
    end
  end

  controller do
    skip_before_action :verify_authenticity_token, only: [:sort]
  end
end
