ActiveAdmin.register Tenant, namespace: :super_admin do

  permit_params :name, :slug, :creation_status, :state, :custom_domain, :stylesheet, site_logo_attributes: [:id, :name, :file], map_logo_attributes: [:id, :name, :file]


  form partial: 'form'

  config.filters = false

  controller do
    def edit
      super do
        @tenant.site_logo = Attachments::Image.new unless @tenant.site_logo.present?
        @tenant.map_logo = Attachments::Image.new unless @tenant.map_logo.present?
      end
    end

    def new
      super do
        @tenant.site_logo = Attachments::Image.new unless @tenant.site_logo.present?
        @tenant.map_logo = Attachments::Image.new unless @tenant.map_logo.present?
      end
    end
  end
  
  member_action :rebuild, method: :post do
    resource.rebuild!
    redirect_to resource_path, notice: "Queued for rebuild!"
  end

  action_item :rebuild, only: :show do
    link_to 'Rebuild', rebuild_super_admin_tenant_path(resource), method: :post
  end

  action_item :pipelines, only: :show do
    link_to 'Import Pipelines', super_admin_tenant_tenant_pipelines_path(resource)
  end

  index do
    column :name
    column :slug
    column "Status", :state
    column :created_at
    column :custom_domain
    
    actions defaults: false do |tenant|
      item 'View', super_admin_tenant_path(tenant), class: 'member_link'
      item 'Edit', edit_super_admin_tenant_path(tenant), class: 'member_link'
      item 'Rebuild', rebuild_super_admin_tenant_path(tenant), class: 'member_link', method: :post
      item 'View pipelines', super_admin_tenant_tenant_pipelines_path(tenant), class: 'member_link'
    end
  end

  show do
    attributes_table do
      row :site_logo do |tenant|
        if tenant.site_logo&.file&.attached?
          ix_image_tag(tenant.site_logo.file.key, url_params: {w: 100, h:100})
        end
      end
      row :map_logo do |tenant|
        if tenant.map_logo&.file&.attached?
          ix_image_tag(tenant.map_logo.file.key, url_params: {w: 100, h:100})
        end
      end
      row :name
      row :slug
      row :custom_domain do |tenant|
        link_to tenant.custom_domain
      end
      tenant.creation_status.collect do |service, status|
        row "#{service}" do
          status_tag(status)
        end
      end
      row :build_status do |tenant|
        status_tag(tenant.build_status)
      end
      row "Netlify build log URL" do |tenant|
        link_to tenant.last_build_url, tenant.last_build_url, target: "_blank"
      end
    end

    panel "Last Build Message" do
      pre class: "build-log" do
        resource.build_log
      end
    end
  end
end
