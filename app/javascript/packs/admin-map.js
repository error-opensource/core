import React from "react";
import ReactDOM from "react-dom";
import MapGL, {Marker, NavigationControl} from "react-map-gl";
import Geocoder from "react-map-gl-geocoder";
import DeckGL, {GeoJsonLayer} from "deck.gl";
import Pin from "./components/pin";

const MAPTILER_KEY = process.env.MAPTILER_KEY
const MAPBOX_TOKEN = process.env.MAPBOX_TOKEN

export default class Map extends React.Component { 
  constructor(props) {
    super(props);
    let startLat = parseFloat(document.getElementById('latitude').value) || 51.5300
    let startLon = parseFloat(document.getElementById('longitude').value) || 0.1277
    this.state = {
      viewport: {
        width: '100%',
        height: 400,
        latitude: startLat,
        longitude: startLon,
        zoom: 3.5,
        bearing: 0,
        pitch: 0,
      },
      marker: {
        latitude: startLat,
        longitude: startLon
      },
      events: {},
    };
  }

  updateViewport = viewport => {
    this.setState({viewport});
  };

  updateFields(lngLat) {
    document.getElementById('longitude').value = lngLat[0].toFixed(4)
    document.getElementById('latitude').value = lngLat[1].toFixed(4)
  }

  onMarkerDrag = event => {
    this.updateFields(event.lngLat)
  };

  onMarkerDragEnd = event => {
    this.setState({
      marker: {
        longitude: event.lngLat[0],
        latitude: event.lngLat[1]
      }
    });
    this.updateFields(event.lngLat)
  };

  onMapClick = event => {
    this.setState({
      marker: {
        longitude: event.lngLat[0],
        latitude: event.lngLat[1]
      }
    });
    this.updateFields(event.lngLat)
  }

  mapRef = React.createRef();

  handleViewportChange = viewport => {
    this.setState({
      viewport: { ...this.state.viewport, ...viewport }
    });
  };

  handleGeocoderViewportChange = viewport => {
    const geocoderDefaultOverrides = { transitionDuration: 1000 };

    return this.handleViewportChange({
      ...viewport,
      ...geocoderDefaultOverrides
    });
  };

  handleOnResult = event => {
    this.setState({
      searchResultLayer: new GeoJsonLayer({
        id: "search-result",
        data: event.result.geometry,
        getFillColor: [255, 0, 0, 128],
        getRadius: 1000,
        pointRadiusMinPixels: 10,
        pointRadiusMaxPixels: 10
      })
    });
  };
  

  render() {
    const {viewport, marker, searchResultLayer} = this.state;
    return (
      <MapGL 
        {...viewport} 
        mapStyle={`https://api.maptiler.com/maps/basic/style.json?key=${MAPTILER_KEY}`}
        onViewportChange={this.updateViewport} 
        onClick={this.onMapClick}
        ref={this.mapRef}
      >
        <Geocoder
          mapRef={this.mapRef}
          onResult={this.handleOnResult}
          onViewportChange={this.handleGeocoderViewportChange}
          mapboxApiAccessToken={MAPBOX_TOKEN}
          position="top-left"
        />
        <DeckGL {...viewport} layers={[searchResultLayer]} />
        <Marker
          latitude={marker.latitude}
          longitude={marker.longitude}
          offsetTop={-20}
          offsetLeft={-10}
          anchor="bottom"
          draggable
          onDragStart={this.onMarkerDragStart}
          onDragEnd={this.onMarkerDragEnd}
          onDrag={this.onMarkerDrag}>
            <Pin size={20}/>
        </Marker>
        <div style={{position: 'absolute', bottom: 20, left: 20}}>
          <NavigationControl showCompass={false}/>
        </div>
      </MapGL>
    );
  }
}

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#record-map') && ReactDOM.render(<Map/>, document.getElementById('record-map'));
})
