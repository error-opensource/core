module ApplicationHelper

  def with_stacktrace(&block)
    begin
      yield block if block_given?
    rescue => e
      puts "--------------- STACKTRACE => -------------------"
      e.backtrace.each(&method(:puts))
      puts "--------------- => STACKTRACE -------------------"
    end
  end
end
