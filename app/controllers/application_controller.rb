class ApplicationController < ActionController::Base
  protect_from_forgery prepend: true, with: :exception

  def after_sign_in_path_for(resource)
    if resource.class == TenantUser
      admin_pages_path
    elsif resource.class == SuperAdminUser
      super_admin_root_path
    end
  end
end
