class TenantsController < ApplicationController
  # Respond with a SCSS stylesheet for a tenant. Might be empty
  def stylesheet
    respond_to do |format|
      format.scss {
        @tenant = Tenant.find(params[:id])
        unless @tenant.present?
          raise ActionController::RoutingError, "Not found"
        end
      }

      format.all {
        raise ActionController::RoutingError, "Not found"
      }
    end

  end
end
