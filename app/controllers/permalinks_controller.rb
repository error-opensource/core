class PermalinksController < ApplicationController
  def redirect
    permalink = Permalink.find_by(short_url: params[:id].downcase)
    raise ActionController::RoutingError, "No permalink" unless permalink.present?
    url = permalink.frontend_url
    if url
      permalink.increment!(:resolution_count)
      redirect_to permalink.frontend_url, status: :permanent_redirect
    else
      raise ActionController::RoutingError, "No frontend url for permalink with ID #{permalink.id}"
    end
  end

  def qr_code
    permalink = Permalink.find_by(short_url: params[:id].downcase)
    raise ActionController::RoutingError, "No permalink" unless permalink.present?
    send_data permalink.qr_code, type: "image/svg+xml", disposition: "attachment", filename: "#{permalink.url.parameterize}.svg"
  end
end
