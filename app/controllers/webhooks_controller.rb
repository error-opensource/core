class WebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token

  def netlify_build_status
    raise ActionController::RoutingError, "incorrect signature" unless Netlify::WebhookSignatureConstraint.new.matches?(request)
  #  Receive a build status hook from netlify.
    tenant = Tenant.find_by_netlify_id(params[:site_id])
    if tenant.present?
      status, log = case request.headers['X-Netlify-Event']
               when 'deploy_building'
                 [:building]
               when 'deploy_failed'
                 [:fail, params[:error_message]]
               when 'deploy_created'
                 [:idle]
               end

      build_url = [params[:admin_url],"builds",params[:id]].join('/')
      BuildStatusJob.perform_later(tenant, build_url, status, log)

      head :ok

    else
      raise ActionController::RoutingError, "Netlify Site ID required"
    end


  end
end
