require 'rails_helper'

RSpec.describe Record, type: :model do
  describe "record_term" do 
    subject(:record_term) {build(:record_term)}

    it "is associated with a record" do 
      expect(record_term.record).not_to be_nil
    end

    it "is associated with a taxonomy term" do 
      expect(record_term.taxonomy_term).not_to be_nil
    end
  end
end
