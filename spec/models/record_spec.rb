require 'rails_helper'

RSpec.describe Record, type: :model do
  describe "record" do 
    subject(:record) {build(:record, lonlat: "POINT(53.44 2.99)")}

    it "has valid longitudinal POINT data" do 
      expect(record.lonlat.lon).to eq(53.44)
    end

    it "has valid latitudinal POINT data" do 
      expect(record.lonlat.lat).to eq(2.99)
    end
  end
end
