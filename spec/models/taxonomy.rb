require 'rails_helper'

RSpec.describe Taxonomy, type: :model do
  describe "taxonomies" do 
    subject(:taxonomy_with_terms) {create(:taxonomy_with_terms, terms_count: 3)}

    it "has associated taxonomy terms" do 
      expect(taxonomy_with_terms.taxonomy_terms.count).to eq(3)
    end
  end
end
