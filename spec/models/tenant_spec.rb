require 'rails_helper'

RSpec.describe Tenant, type: :model do
  let(:settings) {create_list(:setting, 10)}
  let(:tenant) {build(:tenant)}
  let(:tenant_with_modules) {build(:tenant_with_modules, module_count: 3)}

  describe "tenant modules" do 
    it ".hasModule?(Humap::EnabledSubModuleName)" do 
      expect(tenant_with_modules.hasModule?(module_name: tenant_with_modules.enabled_modules.sample.name)).to be true
    end

    it ".hasModule?(Humap::NotEnabledSubModuleName)" do 
      expect(tenant_with_modules.hasModule?(module_name: "Humap::ModuleNameNotEnabled")).to be false
    end
  end

  describe "Tenant switching" do
    it "has no default Tenant" do
      expect(Tenant.current).to eq nil
    end

    it "switches to the correct Tenant" do
      Tenant.switch!(tenant)
      expect(Tenant.current).to eq tenant
    end

    it "can be switched via a Tenant instance" do
      tenant_with_modules.switch!
      expect(Tenant.current).to eq tenant_with_modules
    end
  end
end
