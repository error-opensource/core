require 'rails_helper'

RSpec.describe Humap::Collections::CollectionRecord, type: :model do
  subject {
    build(:record_collection)
  }

  describe "record_collection" do 
    it ".record returns a record" do
      expect(subject.record).not_to be_nil
    end
    
    it ".collection returns a collection" do
      expect(subject.collection).not_to be_nil
    end

    it "adds a .collections association to records" do
      expect(subject.record.respond_to?(:collections)).to be true
    end
  end
end
