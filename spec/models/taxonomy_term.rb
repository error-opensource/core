require 'rails_helper'

RSpec.describe TaxonomyTerm, type: :model do
  describe "taxonomy_terms" do 
    subject(:taxonomy_term) {taxonomy_term = create(:taxonomy_term)}

    it "belongs to a taxonomy" do 
      expect(taxonomy_term.taxonomy).not_to be_nil
    end
  end
end
