FactoryBot.define do
  factory :record do

    before(:build) do
      Tenant.switch!(Tenant.first) unless Tenant.current
    end
    
    name { Faker::Lorem.words(number: rand(2..5)).map(&:capitalize).join(" ") }

    association :image, factory: :attachments_image

    include_image_in_gallery { rand(1..3) != 3 }

    content {
      File.open(File.join(Rails.root,"test","fixtures","record_description.html")).read
    }

    date_from { Faker::Date.between(from: 100.years.ago, to: 1.year.ago) }
    date_to { Faker::Date.between(from: date_from, to: Date.today) }

    # rough bounding box of Coventry - TODO: make a separate task for this
    # -1.605946,52.470196,-1.321544,52.324029
    lonlat { "POINT(#{rand(-1.605946..-1.321544)} #{rand(52.324029..52.470196)})" }
    published_at { published? ? Faker::Date.backward(days: rand(1..30)) : nil}

    trait :with_terms do
      taxonomy_terms {
        terms = TaxonomyTerm.all.sample(5)
        terms.empty? ? create(:taxonomy).taxonomy_terms.sample(5) : terms
      }
    end

    trait :with_attached_images do
      attached_images { create_list :attachments_image, 3 }
    end

    trait :with_attached_files do
      attached_files { create_list :attachments_file, 3 }
    end

    trait :with_attached_video_embeds do
      attached_video_embeds { create_list :attachments_video_embed, 3 }
    end

    trait :with_randomised_attachments do
      attached_images { create_list :attachments_image, rand(1..5) }
      attached_files { create_list :attachments_file, rand(0..3) }
      attached_video_embeds { create_list :attachments_video_embed, rand(0..3) }
    end

    after(:create) do |record|
      record.publish! if rand(1..5) != 5
    end
  end
end
