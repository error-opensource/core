FactoryBot.define do
  factory :taxonomy_term do
    name { Faker::Lorem.unique.word.capitalize }
  end
end
