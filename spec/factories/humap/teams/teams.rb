FactoryBot.define do
  factory :humap_team, class: 'Humap::Teams::Team' do
    name { "MyString" }
    description { "MyText" }
  end
end
