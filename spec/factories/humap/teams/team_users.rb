FactoryBot.define do
  factory :humap_team_user, class: 'Humap::Teams::TeamUser' do
    team { nil }
    user { nil }
  end
end
