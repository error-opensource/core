FactoryBot.define do
  factory :humap_team_record, class: 'Humap::Teams::TeamRecord' do
    team { nil }
    record { nil }
  end
end
