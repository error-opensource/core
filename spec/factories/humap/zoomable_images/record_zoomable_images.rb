FactoryBot.define do
  factory :humap_record_zoomable_image, class: 'Humap::ZoomableImages::RecordZoomableImage' do
    record { nil }
    zoomable_image { nil }
  end
end
