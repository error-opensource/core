FactoryBot.define do
  factory :humap_zoomable_image, class: 'Humap::ZoomableImages::ZoomableImage' do
    name { "MyString" }
    description { "MyText" }
  end
end
