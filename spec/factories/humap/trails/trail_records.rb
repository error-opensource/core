FactoryBot.define do
  factory :trail_record, class: 'Humap::Trails::TrailRecord' do
    trail { nil }
    record { nil }
    sequence(:position, (1..200).cycle) {|pos| pos}
  end
end
