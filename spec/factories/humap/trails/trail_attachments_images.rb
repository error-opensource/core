FactoryBot.define do
  factory :trail_attachments_image, class: 'Humap::Trails::TrailAttachmentsImage' do
    trail { nil }
    attachments_image { nil }
  end
end
