FactoryBot.define do
  factory :trail_taxonomy_term, class: 'Humap::Trails::TrailTaxonomyTerm' do
    trail { nil }
    taxonomy_term { nil }
  end
end
