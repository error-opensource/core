FactoryBot.define do
  factory :trail, class: 'Humap::Trails::Trail' do
    before(:build) do
      Tenant.switch!(Tenant.first) unless Tenant.current
    end

    name { Faker::Lorem.words(number: rand(2..5)).map(&:capitalize).join(" ") }
    content { Faker::Lorem.words(number: rand(2..5)).map(&:capitalize).join(" ") }
    image { nil }

    trait :with_records do
      after(:create) do |trail|
        create_list(:record, rand(3..12), trails: [trail])
      end
    end
  end
end
