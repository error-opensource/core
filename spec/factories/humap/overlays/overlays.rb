FactoryBot.define do
  factory :overlay, class: 'Humap::Overlays::Overlay' do
    name { Faker::Lorem.words(number: rand(2..5)).map(&:capitalize).join(" ") }
    sequence(:url, (1..20).cycle) do |n|
      tileset = "0001_Maps_00#{([n, 20].min).to_s.rjust(2, "0")}" # => 0001_Maps_0001, 0001_Maps_0002 etc
      "https://maptiles.humap.dev/#{tileset}/{z}/{x}/{y}.png"
    end
    
    sequence(:metadata, (1..20).cycle) do |n|
      tileset = "0001_Maps_00#{([n, 20].min).to_s.rjust(2, "0")}" # => 0001_Maps_0001, 0001_Maps_0002 etc
      JSON.parse(open("https://maptiles.humap.dev/#{tileset}/metadata.json").read)
    end
    
    overlay_groups { create_list(:overlay_group, 1) }
    overlay_type { Humap::Overlays::OverlayType.all.sample(1).first || create(:overlay_type) }

    after(:create) do |overlay|
      overlay.publish! if rand(1..5) != 5
    end
  end
end
