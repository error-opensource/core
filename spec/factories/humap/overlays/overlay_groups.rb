FactoryBot.define do
  factory :overlay_group, class: 'Humap::Overlays::OverlayGroup' do
    name { Faker::Lorem.words(number: rand(2..5)).map(&:capitalize).join(" ") }
    
    content {
      File.open(File.join(Rails.root,"test","fixtures","record_description.html")).read
    }

    association :image, factory: :attachments_image
    
    trait :with_overlays do
      after(:create) do |overlay_group|
        create_list :overlay, rand(1..10), overlay_group_ids: [overlay_group.id]
      end
    end

    after(:create) do |overlay_group|
      overlay_group.publish! if rand(1..5) != 5
    end
  end
end
