FactoryBot.define do
  factory :overlay_type, class: 'Humap::Overlays::OverlayType' do
    name { Faker::Lorem.words(number: rand(1..3)).map(&:capitalize).join(" ") }
    description { Faker::Lorem.paragraph }
    overlay_type { [:raster, :vector].sample }
  end
end
