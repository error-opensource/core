FactoryBot.define do
  factory :humap_streaming_media_video_attachment, class: 'Humap::StreamingMedia::VideoAttachment' do
    name { "MyString" }
    description { "MyText" }
  end
end
