FactoryBot.define do
  factory :humap_streaming_media_record_video, class: 'Humap::StreamingMedia::RecordVideo' do
    record { nil }
    video_attachment { nil }
  end
end
