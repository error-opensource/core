FactoryBot.define do
  factory :collection_taxonomy_term, class: 'Humap::Collections::CollectionTaxonomyTerm' do
    collection { nil }
    taxonomy_term { nil }
  end
end
