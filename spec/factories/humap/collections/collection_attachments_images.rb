FactoryBot.define do
  factory :collection_attachments_image, class: 'Humap::Collections::CollectionAttachmentsImage' do
    collection { nil }
    attachments_image { nil }
  end
end
