FactoryBot.define do
  factory :collection, class: 'Humap::Collections::Collection' do

    before(:build) do
      Tenant.switch!(Tenant.first) unless Tenant.current
    end

    name { Faker::Lorem.words(number: rand(2..5)).map(&:capitalize).join(" ") }
    content { "MyText" }

    trait :with_records do
      after(:create) do |collection|
        create_list(:record, rand(5..20), :with_randomised_attachments, collections: [collection])
      end
    end

    trait :with_terms do
      after(:create) do |collection|
        collection.taxonomy_terms {
          terms = TaxonomyTerm.all.sample(5)
          terms.empty? ? create(:taxonomy).taxonomy_terms.sample(5) : terms
        }
      end
    end

    after(:create) do |overlay|
      overlay.publish! if rand(1..5) != 5
    end
  end
end
