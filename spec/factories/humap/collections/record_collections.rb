FactoryBot.define do
  factory :record_collection, class: 'Humap::Collections::CollectionRecord' do
    record 
    collection
  end
end
