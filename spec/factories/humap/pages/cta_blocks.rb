FactoryBot.define do
  factory :cta_block, class: 'Humap::Pages::CtaBlock' do
    title { Faker::Lorem.word }
    content { Faker::Lorem.sentences(number: 2).join("\n") }
    url { Faker::Internet.url(host: 'example.com') }
    button_text { Faker::Lorem.word }
  end
end
