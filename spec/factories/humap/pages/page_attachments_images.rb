FactoryBot.define do
  factory :humap_pages_page_attachments_image, class: 'Humap::Pages::PageAttachmentsImage' do
    page { nil }
    attachments_image { nil }
  end
end
