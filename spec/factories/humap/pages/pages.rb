FactoryBot.define do
  factory :page, class: 'Humap::Pages::Page' do
    name { Faker::Lorem.unique.word.capitalize }
    content {
      File.open(File.join(Rails.root,"test","fixtures","record_description.html")).read
    }
    association :image, factory: :attachments_image
    page_type { "content_page" }
    description { Faker::Lorem.paragraph }

    trait :with_quick_starts do
      quick_starts {
        create_list(:quick_start, rand(1..6))
      }
    end

    trait :with_cta_blocks do
      cta_blocks {
        create_list(:cta_block, rand(1..2))
      }
    end

    after(:create) do |record|
      record.publish! if rand(1..5) != 5
    end
  end
end
