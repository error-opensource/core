FactoryBot.define do
  factory :taxonomy do
    
    before(:build) do
      Tenant.switch!(Tenant.first) unless Tenant.current
    end

    name { Faker::Lorem.unique.word.capitalize }

    after(:create) do |taxonomy|
      create_list(:taxonomy_term, rand(5..20), taxonomy: taxonomy)
    end
  end
end
