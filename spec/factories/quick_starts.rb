FactoryBot.define do
  factory :quick_start do
    title { Faker::Lorem.unique.word.capitalize }
    content { Faker::Lorem.paragraph }
    association :image, factory: :attachments_image
    url { Faker::Internet.url }
  end
end
