FactoryBot.define do
  factory :setting do
    sequence(:name) {|s| "Setting #{s}"}
    sequence(:default_value) {|s| "setting-value-#{s}" }
  end
end
