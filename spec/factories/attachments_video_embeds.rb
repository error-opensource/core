FactoryBot.define do
  factory :attachments_video_embed, class: 'Attachments::VideoEmbed' do
    name { Faker::Lorem.words(number: rand(2..5)).map(&:capitalize).join(" ") }
    url { "https://www.youtube.com/watch?v=dQw4w9WgXcQ" }
  end
end
