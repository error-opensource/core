FactoryBot.define do
  factory :humap_module do
    name { "Humap::#{Faker::Lorem.unique.word.parameterize.underscore.classify}"}
  end
end
