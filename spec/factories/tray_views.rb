FactoryBot.define do
  factory :tray_view do
    title { Faker::Lorem.unique.word.capitalize }
    content {
      File.open(File.join(Rails.root,"test","fixtures","record_description.html")).read
    }
    view_type { "welcome_view "}

    trait :with_highlighted_records do
        highlighted_records {
          records = Record.all.sample(8)
          records.empty? ? create_list(:record, 8) : records
        }
    end
  end
end
