FactoryBot.define do
  factory :record_term do
    record
    taxonomy_term
  end
end
