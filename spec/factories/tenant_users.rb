FactoryBot.define do
  factory :tenant_user do
    name { Faker::Name.name}
    email {Faker::Internet.unique.safe_email}
    password { "testing123"}
    tenant
  end
end
