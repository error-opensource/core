FactoryBot.define do
  factory :tenant do
    name { rand(1..3).even? ? Faker::Address.city : Faker::Educator.university }
    creation_status {
      {}
    }

    trait :with_modules do
      transient do 
        module_count {rand(0..6)}
      end

      after(:build) do |tenant, evaluator|
        tenant.enabled_modules = build_list(:humap_module, evaluator.module_count)
      end
    end

    factory :tenant_with_modules, traits: [:with_modules]

    after(:create) do |tenant|
      tenant.create! if rand(1..10) % 10 > 0
      tenant.archive! if rand(1..10) == 10 && tenant.created?
    end
  end
end
