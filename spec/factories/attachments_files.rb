FactoryBot.define do
  factory :attachments_file, class: 'Attachments::File' do
    name { Faker::Lorem.words(number: rand(2..5)).map(&:capitalize).join(" ") }
    credit {Faker::Lorem.sentence(word_count: rand(5..10))}
    created_at { Faker::Time.backward(days: 365) }
    updated_at { Faker::Time.backward(days: 220) }

    file do
      filename = Dir.glob(Rails.root.join('test', 'fixtures', 'files','images','*.{docx,pdf,txt,xlsx}')).sample
      extname = File.extname(filename)[1..-1]
      mime_type = Mime::Type.lookup_by_extension(extname)
      content_type = mime_type.to_s
      Rack::Test::UploadedFile.new(filename, content_type)
    end

  end
end