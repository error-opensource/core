require_relative 'lib/netlify/version'

Gem::Specification.new do |spec|
  spec.name          = "netlify"
  spec.version       = Netlify::VERSION
  spec.authors       = ["Ed Jones"]
  spec.email         = ["ed@error.agency"]

  spec.summary       = %q{Implements a small subset of calls to the Netlify API}
  spec.homepage      = "https://humap.me"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "faraday"
  spec.add_dependency "faraday_middleware"
  spec.add_dependency "require_all"
  spec.add_dependency "jwt"

end
