module Netlify
  class Hook < Base
    self.endpoint = "hooks"

    def self.all(site)
      get(self.endpoint, {site_id: site})
    end

    def self.types
      get("#{self.endpoint}/types").body
    end

    def self.create_for_site!(site, config={})
      raise ArgumentError, "you need to include at least a type" unless config[:type].present?
      self.new(post(self.endpoint, config.merge(site_id: site.id)).body)
    end
  end
end