module Netlify
  class Base < ::OpenStruct
    
    class << self
      attr_accessor :endpoint
    end

    def self.all
      get(endpoint).body.collect do |entity|
        self.new(entity)
      end
    end

    def self.find(id)
      response = get([endpoint, id].join("/"))
      self.new(response.body)
    end

    def self.create!(args)
      entity = self.new(args)
      entity.save
    end
    
    def save
      if persisted?
        # functions can't be amended in a put, so we remove them from the data we're sending
        self.class.put([self.class.endpoint,id].join('/'), self.to_h.except(:functions_config).to_json)
      else
        return self.class.new(self.class.post(self.class.endpoint, self.to_h.to_json).body)
      end
    end

    def persisted?
      self.id.present?
    end

    private

    def self.connection
      Netlify.configuration.connection
    end

    def self.get(endpoint, body=nil, headers=nil, &block)
      connection.get(endpoint, body, headers, &block)
    end

    def self.post(endpoint, body=nil, headers=nil, &block)
      connection.post(endpoint, body, headers, &block)
    end

    def self.put(endpoint, body=nil, headers=nil, &block)
      connection.put(endpoint, body, headers, &block)
    end





  end
end