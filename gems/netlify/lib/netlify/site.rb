
module Netlify
  class Site < Base
    self.endpoint = 'sites'

    def initialize(args={})
      super({
              repo: {
                provider: 'gitlab',
                repo: 'error-agency/humap/frontend',
                repo_url: 'https://gitlab.com/error-agency/humap/frontend',
                private: true,
                branch: ENV['HOSTING_ENVIRONMENT'] == 'staging' ? 'staging' : 'master',
                cmd: 'gatsby build',
                dir: 'public',
                deploy_key_id: ENV['NETLIFY_DEPLOY_KEY_ID']
              },
              build_settings: {
                env: self.class.environment_vars.merge({
                                                         "GATSBY_TENANT_ID" => args[:tenant].try(:id),
                                                         "GATSBY_TENANT_SLUG" => args[:tenant].try(:slug),
                                                         "GATSBY_MAPBOX_TOKEN" => ENV['MAPBOX_TOKEN'],
                                                         "GATSBY_CORE_URL" => ENV['CORE_URL']
                                                       })
              }

            }.merge(args))
    end


    def builds
      self.class.get("#{self.class.endpoint}/#{id}/builds").body.collect do |build|
        Build.new(build.merge(site: self))
      end
    end

    def build_hooks
      self.class.get("#{self.class.endpoint}/#{id}/build_hooks").body.collect do |build|
        BuildHook.new(build.merge(site: self))
      end
    end

    def deploys
      self.class.get("#{self.class.endpoint}/#{id}/deploys").body.collect do |build|
        Deploy.new(build.merge(site: self))
      end
    end

    def self.environment_vars
      ENV.select {|k,v| k.match(/GATSBY/)}
    end

    def create_build_hook!(title: nil)
      res = self.class.post("#{self.class.endpoint}/#{id}/build_hooks", {site_id: self.id, title: title})
      BuildHook.new(res.body)

    end

    def create_hook!(config)
      Hook.create_for_site!(self, config)
    end

    def hooks
      Hook.all(self.id).body.collect do |hook|
        Hook.new(hook)
      end
    end

  end
end