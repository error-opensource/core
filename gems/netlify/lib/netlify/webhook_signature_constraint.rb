require 'jwt'
module Netlify
  class WebhookSignatureConstraint

    def matches?(request)
      request_is_signed?(request)
    end

    private

    def request_is_signed?(request)
      signature = request.headers["X-Webhook-Signature"]
      return false unless signature

      options = {iss: "netlify", verify_iss: true, algorithm: "HS256"}
      decoded = JWT.decode(signature, Netlify.configuration.webhook_shared_secret, true, options)

      decoded.first['sha256'] == Digest::SHA256.hexdigest(request.body.read)
    rescue JWT::DecodeError
      false
    end
  end
end