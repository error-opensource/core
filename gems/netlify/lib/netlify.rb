require 'faraday'

require 'netlify/base'
require 'netlify/site'
require 'netlify/build'
require 'netlify/deploy'
require 'netlify/build_hook'
require 'netlify/hook'
require 'netlify/webhook_signature_constraint'
module Netlify


  class << self
    attr_accessor :configuration, :connection

    def configure
      self.configuration ||= Configuration.new
      yield(self.configuration)
      self.configuration.configure_connection
      @connection = @configuration.connection
    end
  end

  class Configuration

    API_URL = "https://api.netlify.com/api/v1/"
    attr_accessor :personal_access_token, :webhook_shared_secret

    attr_reader :connection

    def configure_connection
      @connection ||= Faraday.new({
                                    url: API_URL,
                                    headers: {
                                      "Authorization" => "Bearer #{@personal_access_token}"
                                    }
                                  }) do |conn|
        conn.request :json
        conn.response :json
      end
    end
  end

  class Error < StandardError; end

end
