require 'kiba'
require 'active_support/all'
require 'require_all'
require 'geocoder'

require_rel 'data_pipelines'

module Humap
  module DataPipelines
    def instrument!(event, data={})
      puts "instrument #{event}"
      ActiveSupport::Notifications.instrument(event, data.merge(tenant_id: Tenant.current.try(&:id)))
    end
    module_function :instrument!

    def all
      pipelines = Tenants.constants.collect do |tenant|
        Tenants.const_get(tenant).constants.collect {|c| Tenants.const_get(tenant).const_get(c)}
      end
      pipelines.flatten
    end
    module_function :all

    def templates_path(tenant:)
      File.expand_path(File.join(File.dirname(__FILE__),"..","..","templates","tenants",tenant))
    end
    module_function :templates_path

    class Error < StandardError; end

  end
end
