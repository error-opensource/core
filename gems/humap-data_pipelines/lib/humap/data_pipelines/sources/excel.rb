module Humap
  module DataPipelines
    module Sources
      class Excel
        def initialize(file:, mapping:)
          file = Humap::DataSources::Excel::File.new(file: file, mapping: mapping)
          @worksheets = file.worksheets
        end

        def each
          begin
            if @worksheets.count == 1
              @worksheets.first.rows.each_with_index do |row, i|
                DataPipelines.instrument!('each.excel.sources.data_pipelines', {count: i})
                yield(row)
              end
            elsif @worksheets.count > 1
              @worksheets.each_with_index do |worksheet, i|
                DataPipelines.instrument!('each.excel.sources.data_pipelines', {count: i})
                yield(worksheet)
              end
            end
          rescue => e
            DataPipelines.instrument!('error.excel.sources.data_pipelines', {error: e})
          end
        end


      end
    end
  end
end