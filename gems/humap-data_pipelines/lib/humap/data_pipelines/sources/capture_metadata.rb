module Humap
  module DataPipelines
    module Sources
      class CaptureMetadata

        # Get the list of available assets, which are then used to instantiate the pipeline to import them.

        def initialize(config:,params:{})
          # capture_config is expected to be a hash
          Humap::DataSources::Capture.configure do |conf|
            config.each do |setting,val|
              conf.send(:"#{setting}=",val)
            end
          end

          @params = params
        end

        def each
          begin
            metadata = Humap::DataSources::Capture::Asset.metadata(limit: @params[:limit], from: @params[:from], to: @params[:to])
            yield(metadata)
          rescue => e
            DataPipelines.instrument!('error.capture_metadata.sources.data_pipelines', {error: e})
            Rollbar.log('error.capture_metadata.sources.data_pipelines',e)
          end

        end
      end
    end
  end
end
