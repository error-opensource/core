module Humap
  module DataPipelines
    module Sources
      class Capture

        def initialize(config:,params:{})
          # capture_config is expected to be a hash
          Humap::DataSources::Capture.configure do |conf|
            config.each do |setting,val|
              conf.send(:"#{setting}=",val)
            end
          end

          @params = params
        end

        def each
          begin
            filter = @params.has_key?(:filter) && @params[:filter].is_a?(Proc)
            assets = Humap::DataSources::Capture::Asset.all(limit: @params[:limit], offset: @params[:offset])
            if filter
              assets = assets.select {|a| @params[:filter].call(a)}
            end
            assets.each_with_index do |asset, i|
              DataPipelines.instrument!('each.capture.sources.data_pipelines', {count: i})
              yield(asset)
            end
          rescue => e
            DataPipelines.instrument!('error.capture.sources.data_pipelines', {error: e})
            Rollbar.log('error.capture.sources.data_pipelines',e)
          end

        end
      end
    end
  end
end
