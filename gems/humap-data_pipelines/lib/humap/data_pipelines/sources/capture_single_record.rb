module Humap
  module DataPipelines
    module Sources
      class CaptureSingleRecord

        def initialize(config:,id:)
          # capture_config is expected to be a hash
          Humap::DataSources::Capture.configure do |conf|
            config.each do |setting,val|
              conf.send(:"#{setting}=",val)
            end
          end

          @id = id
        end

        def each
          begin
            asset = Humap::DataSources::Capture::Asset.find(@id)
            DataPipelines.instrument!('each.capture_singleton.sources.data_pipelines', {id: @id})
            yield(asset) if asset.present?
          rescue => e
            DataPipelines.instrument!('error.capture_singleton.sources.data_pipelines', {error: e})
            Rollbar.log('error.capture_singleton.sources.data_pipelines',e)
          end

        end
      end
    end
  end
end
