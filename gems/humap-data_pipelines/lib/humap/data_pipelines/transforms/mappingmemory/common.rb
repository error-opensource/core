module Humap
  module DataPipelines
    module Transforms
      module Mappingmemory
        module Common
          extend ActiveSupport::Concern

          def initialize(assets_folder: nil)
            raise "you need to pass an assets folder into #{self.class}" if assets_folder.nil?
            @assets_folder = assets_folder
          end

          private

          def get_taxonomy_terms(record)
            record.memorycategories.split(",").collect(&:strip)
          end

          def get_title(record)
            [record.placename, record.memorytitle].join(": ")
          end

          def build_record_data(record, content_type: nil)
            taxonomy_terms = get_taxonomy_terms(record)
            data = {
              record: {
                import_metadata: {
                  id: record.id
                },
                name: get_title(record),
                content: "",
                lonlat: "POINT(#{record.longitude} #{record.latitude})",
                keywords: (taxonomy_terms - ['all']).join(' ')
              },
              taxonomies: {
                "Themes" => taxonomy_terms - ['all']
              }
            }

            if content_type.present?
              data[:taxonomies].merge!({
                                                  "Content Types" => [content_type]
                                                })
            end

            return data
          end

          def is_record_type?(type, record:)
            record.respond_to?(:record_type) && record.record_type == type
          end


        end
      end
    end
  end
end