module Humap
  module DataPipelines
    module Transforms
      module Mappingmemory
        class Text
          include ActionView::Helpers::TextHelper
          include Humap::DataPipelines::Transforms::Mappingmemory::Common

          # Return a hash with the data in the format of attributes for a Record
          # receives a row from the excel source
          # Return a hash with the data in the format of attributes for a Record
          # receives a row from the excel source
          def process(record)
            DataPipelines.instrument!('process.text.mappingmemory.transforms.data_pipelines', {id: record.id})
            # Skip unless this is an image record
            puts "Record type #{record.record_type}"
            return record unless is_record_type?('text', record: record)

            begin

              record_data = build_record_data(record, content_type: "text")
              record_data[:record][:content] = record.description
              return record_data

            rescue => e
              DataPipelines.instrument!('error.text.mappingmemory.transforms.data_pipelines', {error: e})
              Rollbar.log('error.text.mappingmemory.transforms.data_pipelines', e)
              return nil
            end
          end
        end
      end
    end
  end
end
