module Humap
  module DataPipelines
    module Transforms
      module Mappingmemory
        class Audio
          include ActionView::Helpers::TextHelper

          # Return a hash with the data in the format of attributes for a Record
          # receives a row from the excel source
          def process(row)
            DataPipelines.instrument!('process.audio.mappingmemory.transforms.data_pipelines', {id: row.id})
            # we skip over this transform if the content is not audio
            return row unless row.record_type == "audio"


            begin
              credit = ["Copyright: #{asset.copyright_notice}"]
              if asset.creator.present?
                credit << "Creator: #{asset.creator}"
              end

              credit = credit.join(". ")

              data = {}
              taxonomy_terms = [
                asset.subject_terms,
                asset.subject_people,
                asset.content_types
              ].flatten

              # By preference we use a truncated caption for the title.
              if asset.caption.strip.present?
                caption = truncate(asset.caption.strip, length: 60, separator: " ")
                title = [asset.title.strip, caption].join(" - ")
              else
                title = asset.title
              end


                data.merge!({
                              record: {
                                import_metadata: {
                                  id: asset.id.to_i
                                },
                                name: CGI.unescape_html(title),
                                content: simple_format([asset.caption, credit].join("\n\n")),
                                lonlat: "POINT(#{asset.lng} #{asset.lat})",
                                keywords: taxonomy_terms.join(' ')
                              },
                              taxonomies: {
                                "Categories" => taxonomy_terms
                              }
                            })

                data.merge!({
                              image: {
                                import_metadata: {
                                  id: asset.id.to_i
                                },
                                name: asset.title,
                                file: (@image_quality == :high ? asset.highres : asset.lowres),
                                credit: credit,
                                description: asset.caption,
                                filename: asset.summary['org_filename']
                              }
                            })

              return data
            rescue => e
              DataPipelines.instrument!('error.legacy_record.mappingmemory.transforms.data_pipelines', {error: e})
              Rollbar.log('error.legacy_record.mappingmemory.transforms.data_pipelines',e)
              return nil
            end
          end
        end
      end
    end
  end
end
