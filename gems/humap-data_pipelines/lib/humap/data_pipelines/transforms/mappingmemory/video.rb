module Humap
  module DataPipelines
    module Transforms
      module Mappingmemory
        class Video
          include ActionView::Helpers::TextHelper
          include Humap::DataPipelines::Transforms::Mappingmemory::Common

          # Return a hash with the data in the format of attributes for a Record
          # receives a row from the excel source
          def process(record)
            DataPipelines.instrument!('process.video.mappingmemory.transforms.data_pipelines', {id: record.id})
            # Skip unless this is an image record
            puts "Record type #{record.record_type}"
            return record unless is_record_type?('video', record: record)

            begin

              record_data = build_record_data(record, content_type: "video")
              record_data[:record][:content] = generate_transcript(record)
              
              video_data = {
                video_embed: {
                  import_metadata: {
                    # The import metadata associated with an image needs to be unique to the image, but deterministically reference the record.
                    # So we use a hash of the two
                    id: Digest::MD5.hexdigest("#{record.id}/#{record.youtube}")
                  },
                  url: record.youtube,
                  name: "Video: #{record_data[:record][:name]}",
                  credit: record.description
                }
              }

              return video_data.merge(record_data)

            rescue => e
              DataPipelines.instrument!('error.video.mappingmemory.transforms.data_pipelines', {error: e})
              Rollbar.log('error.video.mappingmemory.transforms.data_pipelines', e)
              return nil
            end
          end

          def generate_transcript(record)
            ApplicationController.render(inline: File.open(File.join(Humap::DataPipelines.templates_path(tenant: "mappingmemory"), "video_transcript.html.erb")).read, locals: {record: record})
          end
        end
      end
    end
  end
end
