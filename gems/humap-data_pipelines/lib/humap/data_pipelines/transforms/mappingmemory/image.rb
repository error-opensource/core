module Humap
  module DataPipelines
    module Transforms
      module Mappingmemory
        class Image
          include ActionView::Helpers::TextHelper
          include Humap::DataPipelines::Transforms::Mappingmemory::Common

          # Return a hash with the data in the format of attributes for a Record
          # receives a row from the excel source
          def process(record)
            DataPipelines.instrument!('process.image.mappingmemory.transforms.data_pipelines', {id: record.id})
            # Skip unless this is an image record
            return record unless is_record_type?('image', record: record)

            begin

              # One row in the spreadsheet is one record, but it'll have more than one image associated with it.
              # - create the record and return that as the data from the transform
              # - ALSO yield for each image, with a hash for that image (referencing the record)

              record_data = build_record_data(record, content_type: "image")

              # For the images, we need to iterate over each of the images in the zipfile's appropriate foldername
              # And we assume that the image captions match in lexicographical order
              captions = record.imagecaptions.split(";").collect(&:strip)
              Dir.glob(File.join(@assets_folder, record.foldername, "image*")).each_with_index do |file, i|
                # the caption and credit are in the same field, but consistently the title can be grabbed by splitting at the first full-stop.
                full_caption = captions[i]
                title = full_caption[0..full_caption.index('.')].strip
                credit = full_caption[full_caption.index('.')+1..-1].strip
                filename = File.basename(file)
                data = {
                  image: {
                    import_metadata: {
                      # The import metadata associated with an image needs to be unique to the image, but deterministically reference the record.
                      # So we use a hash of the two
                      id: Digest::MD5.hexdigest("#{record.id}/#{filename}")
                    },
                    name: title,
                    file: file,
                    credit: credit,
                    description: full_caption,
                    filename: filename
                  }
                }.merge(record_data)
                # we yield multiple times, generating a new image for each.
                yield(data)
              end

              # don't return the image directly here
              return nil
            rescue => e
              DataPipelines.instrument!('error.image.mappingmemory.transforms.data_pipelines', {error: e})
              Rollbar.log('error.image.mappingmemory.transforms.data_pipelines', e)
              return nil
            end
          end
        end
      end
    end
  end
end
