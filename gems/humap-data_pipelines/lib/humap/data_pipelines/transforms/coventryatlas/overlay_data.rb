module Humap
  module DataPipelines
    module Transforms
      module Coventryatlas
        class OverlayData
          def initialize(group_on: nil)
            @group_on = group_on
          end

          def process(row)
            begin
              DataPipelines.instrument!('process.overlay_data.coventryatlas.transforms.data_pipelines', {id: row.id})
              folder = row.id.gsub(".tif", "") # => OOO1_Maps_XXXX
              tile_url = "https://overlays.#{ENV['BASE_DOMAIN']}/#{Tenant.current.slug}/#{folder}/{z}/{x}/{y}.png" # => overlays.humap.dev/coventryatlas/0001_Maps_XXXX/{z}/{x}/{y}.png
              metadata_url = "https://overlays.#{ENV['BASE_DOMAIN']}/#{Tenant.current.slug}/#{folder}/metadata.json" # => overlays.humap.dev/coventryatlas/0001_Maps_XXXX/metadata.json

              # if we have a column to group on, return a
              data = {
                name: row.title,
                description: row.caption,
                import_metadata: {
                  id: row.id,
                  metadata_url: metadata_url,
                  tile_size: 1024
                },
                url: tile_url
              }

              if @group_on.present?
                data[:import_metadata].merge!({
                                                overlay_group_id: row.send(@group_on.to_sym)
                                              })
              end

              return data
            rescue => e
              DataPipelines.instrument!('error.overlay_data.coventryatlas.transforms.data_pipelines', {error: e})
              Rails.logger.debug("Error transforming overlay metadata row #{row.id}:")
              Rails.logger.debug(e)
              Rails.logger.debug(e.backtrace.each(&method(:puts)))
              return nil
            end

          end
        end
      end
    end
  end
end