module Humap
  module DataPipelines
    module Transforms
      module Coventryatlas
        class CaptureAsset
          include ActionView::Helpers::TextHelper
          def initialize(skip_ids: [], skip_download_for_ids: [], image_quality: :high)
            @skip_ids = skip_ids.collect(&:to_i)
            @skip_download_for_ids = skip_download_for_ids.collect(&:to_i)
            @image_quality = image_quality
          end

          # Return a hash with the data in the format of attributes for a Record
          def process(asset)
            DataPipelines.instrument!('process.capture_asset.coventryatlas.transforms.data_pipelines', {id: asset.id})
            begin
              credit = ["Copyright: #{asset.copyright_notice}"]
              if asset.creator.present?
                credit << "Creator: #{asset.creator}"
              end

              credit = credit.join(". ")

              data = {}


              # By preference we use a truncated caption for the title.
              if asset.caption.strip.present?
                caption = truncate(asset.caption.strip, length: 60, separator: " ")
                title = [asset.title.strip, caption].join(" - ")
              else
                title = asset.title
              end


              unless @skip_ids.include?(asset.id.to_i)
                data.merge!({
                              record: {
                                import_metadata: {
                                  id: asset.id.to_i
                                },
                                name: CGI.unescape_html(title),
                                content: simple_format([asset.caption, credit].join("\n\n")),
                                lonlat: "POINT(#{asset.lng} #{asset.lat})",
                                keywords: [
                                  asset.subject_terms,
                                  asset.subject_people,
                                  asset.content_types
                                ].flatten.join(' ')
                              },
                              taxonomies: {
                                "Subjects" => asset.subject_terms,
                                "People" => asset.subject_people
                              }
                            })
              end

              unless @skip_download_for_ids.include?(asset.id.to_i)
                data.merge!({
                              image: {
                                import_metadata: {
                                  id: asset.id.to_i
                                },
                                name: asset.title,
                                file: (@image_quality == :high ? asset.highres : asset.lowres),
                                credit: credit,
                                description: asset.caption,
                                filename: asset.summary['org_filename']
                              }
                            })
              end

              return data
            rescue => e
              DataPipelines.instrument!('error.capture_asset.coventryatlas.transforms.data_pipelines', {error: e})
              Rollbar.log('error.capture_asset.coventryatlas.transforms.data_pipelines',e)
              return nil
            end
          end
        end
      end
    end
  end
end
