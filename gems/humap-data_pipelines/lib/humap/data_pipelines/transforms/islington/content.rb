module Humap
  module DataPipelines
    module Transforms
      module Islington
        class Content
          include ActionView::Helpers::TextHelper

          def initialize(text_folder:)
            @text_folder = text_folder
          end

          # receive a record from an excel row, and get the associated text from a folder of text files
          def process(record)
            DataPipelines.instrument!('process.content.islington.transforms.data_pipelines', {id: record.id})
            begin

              # get the likely unique word from the name - i.e. remove things like 'the' and non-letters
              likely_unique = record.name.downcase.gsub(/[^a-zA-Z ]/,"").gsub(/the/i,"").split(" ").first

              # Get the appropriate text file, based on the row in the spreadsheet we're at
              filenames = Dir.entries(@text_folder).inject({}) do |hash, filename|
                hash[filename.downcase] = filename
                hash
              end
              #
              file = filenames[filenames.keys.grep(%r{#{likely_unique}}).first]

              taxonomy_terms = record.category.split(" ").collect(&:downcase)

              return {
                record: {
                  import_metadata: {
                    id: Digest::MD5.hexdigest(record.name),
                    address: record.address
                  },
                  name: record.name,
                  content: file.present? ? get_content(file) : "",
                  # location: "POINT(#{record.longitude} #{record.latitude})",
                  keywords: taxonomy_terms.join(' ')
                },
                taxonomies: {
                  "Category" => taxonomy_terms
                }
              }

            rescue => e
              DataPipelines.instrument!('error.content.islington.transforms.data_pipelines', {error: e})
              Rollbar.log('error.content.islington.transforms.data_pipelines', e)
              return nil
            end
          end

          def get_content(file)
            filepath = File.join(@text_folder, file)
            # Take a stab at finding the mimetype of the file.
            mime_encoding = begin
                              `file --mime-encoding "#{filepath}" | awk '{print $(NF)}'`.strip
                        rescue
                          'UTF-8'
                            end
            raw = File.open(File.join(@text_folder, file)).read&.encode!('UTF-8', mime_encoding, :invalid => :replace)
            # if the incoming string matches 'text:', split at that point and only do the bit afterwards
            if raw.match(/Text:/)
              simple_format(raw.split("Text:").last.strip)
            else
              simple_format(raw)
            end
          end
        end
      end
    end
  end
end
