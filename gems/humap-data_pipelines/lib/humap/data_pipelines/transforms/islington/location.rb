module Humap
  module DataPipelines
    module Transforms
      module Islington
        class Location

          # receives a hash from the previous transform, which includes 'address' as a key in the import metadata.
          # We need to get the location from a geocoder call before it hits the Record destination
          def process(record_data)
            DataPipelines.instrument!('process.location.islington.transforms.data_pipelines', {id: record_data[:record][:import_metadata][:id]})
            # begin
              address = record_data[:record][:import_metadata][:address]
              if address.present?
                location = Geocoder.search(address).first
                if location.present?
                  lng = location.data["geometry"]["location"]["lng"]
                  lat = location.data["geometry"]["location"]["lat"]
                  record_data[:record][:lonlat] = "POINT(#{lng} #{lat})"
                  return record_data
                else
                  raise Humap::DataPipelines::Error, "Couldn't find location for #{address}"
                end
              end
            # rescue => e
            #   DataPipelines.instrument!('error.location.islington.transforms.data_pipelines', {error: e})
            #   Rollbar.log('error.location.islington.transforms.data_pipelines', e)
            #   return nil
            # end

          end

        end
      end
    end
  end
end