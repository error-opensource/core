module Humap
  module DataPipelines
    def observe(event)
      ActiveSupport::Notifications.subscribe(event) do |name, started, finished, unique_id, data|
        yield(name, started, finished, unique_id, data) if block_given?
      end
    end
    module_function :observe
  end
end