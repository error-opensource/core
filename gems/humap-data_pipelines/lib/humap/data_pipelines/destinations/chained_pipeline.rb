module Humap
  module DataPipelines
    module Destinations
      class ChainedPipeline
        # Takes a pipeline to invoke, with batch size
        def initialize(pipeline:, batch_size: 1, calling_pipeline: nil)
          @pipeline = pipeline
          @batch_size = batch_size
          @calling_pipeline = calling_pipeline
        end

        def write(incoming_data)
          begin
            #   split the incoming data into batches of @batch_size, if there is one, and the pipeline being called has parameters for limit and offset
            parameters = @pipeline.method(:setup).parameters.collect(&:last)
            if @batch_size && parameters.include?(:limit) && parameters.include?(:offset) && parameters.include?(:calling_pipeline)
              (incoming_data.count / @batch_size.to_f).ceil.times do |i|
                DataPipelines.instrument!('write.chained_pipeline.destinations.data_pipelines', {pipeline: @pipeline, batch_size: @batch_size, offset: i * @batch_size})
                Kiba.run(@pipeline.send(:setup, limit: @batch_size, offset: i * @batch_size, calling_pipeline: @calling_pipeline))
              end
            else
              DataPipelines.instrument!('skip.chained_pipeline.destinations.data_pipelines', {reason: "#{@pipeline}#setup does not respond to :limit and :offset"})
            end
          rescue => e
            DataPipelines.instrument!('error.chained_pipeline.destinations.data_pipelines', {pipeline: @pipeline, error: e})
            Rollbar.log('error.chained_pipeline.destinations.data_pipelines', e)
          end
        end
      end
    end
  end
end