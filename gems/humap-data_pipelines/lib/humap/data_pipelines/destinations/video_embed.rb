# This destination expects incoming data to be a hash with at least a video key, as follows:
#
# video_embed: {
#                                 import_metadata: {
#                                   id: "id, can be a string or a numeric"
#                                 },
#                                 name: "title of video",
#                                 url: filepath to image ON DISK. Download in a transform if necessary.
#                                 credit: "credit for image",
#                                 description: "description of image",
#                                 filename: "filename for the file"
#                               }

# Additionally, you can specify a key/value pair to associate to
module Humap
  module DataPipelines
    module Destinations
      class VideoEmbed

        # @param associate_to_class [#find] The entity to associate this image to. Probably Record.
        # @param associate_by [Array] The path in the incoming data hash to find the key to the entity (passed to Hash#dig). e.g. `[:record, :id]`
        # @param association_names [Array] The association names on the target entity to which you want to add this image. Associated in priority order, where the first without an image will be assigned.
        def initialize(associate_to_class: nil, associate_by: [:record, :id], association_names: [:attached_video_embeds])
          @klass = associate_to_class
          @associate_by = associate_by
          @association_names = association_names
        end

        def write(incoming_data)
          begin
            DataPipelines.instrument!('write.video_embed.destinations.data_pipelines', {id: (incoming_data.dig(*@associate_by) rescue nil)})
            #  expects a hash with an :image key
            #  and optionally a :record key with an id to associate the image to
            #
            data = incoming_data[:video_embed]
            return unless data.present? && data[:url].present?

            video_embed = Attachments::VideoEmbed.where("import_metadata @> ?", {id: data[:import_metadata][:id]}.to_json).first || Attachments::VideoEmbed.new
            video_embed.update(data)
            video_embed.save

            # The video embed is saved but it's not associated to anything. We need to do that, if @klass is present and there's an association name(s)
            if @klass.present? && @associate_by.present?
              if incoming_data.dig(*@associate_by).present?
                entity = @klass.find(incoming_data.dig(*@associate_by))
                return unless entity.present?

                #iterate over @association_names, assigning to the first which doesn't return a video embed.
                #
                # If the association responds to << we assume it's a has_many, otherwise we assume it's a 1:1 relationship

                @association_names.each do |association|
                  association_data = entity.send(association.to_sym)
                  if association_data.respond_to?(:<<)
                    # 1:many relationship
                    unless association_data.include?(video_embed)
                      association_data.send(:<<, video_embed)
                      entity.save
                      break
                    end
                  else
                    # 1:1 relationship
                    unless association_data.present?
                      entity.send(:"#{association}=",video_embed)
                      entity.save
                      break
                    end
                  end
                end
              end
            end
          rescue => e
            DataPipelines.instrument!('error.image.destinations.data_pipelines', {data: incoming_data, error: e})
            Rollbar.log('error.video_embed.destinations.data_pipelines',e)
          end
        end
      end
    end
  end
end
