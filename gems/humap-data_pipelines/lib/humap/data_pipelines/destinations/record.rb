# This destination expects incoming data as a hash, with at least a record key as follows
# record: {
#     import_metadata: {
#       id: "some id" # can we a string or a numeric
#     },
#     name: "Title",
#     content: "HTML for the content",
#     lonlat: "POINT(lng lat)",
#     keywords: "space separated keywords"
#   },
#   taxonomies: {
#     "Taxonomy Name" => taxonomy_terms
#   }
# }

module Humap
  module DataPipelines
    module Destinations
      class Record

        def initialize(publish_immediately: false)
          @publish_immediately = publish_immediately
        end

        def write(incoming_data)
          begin
            data = incoming_data[:record]
            return unless data.present?
            DataPipelines.instrument!('write.record.destinations.data_pipelines', {id: incoming_data[:record][:id], publish_immediately: @publish_immediately})
            #  Expects a hash with a key of :record. All other data is ignored
            record = ::Record.where("import_metadata @> ?", {id: data[:import_metadata][:id]}.to_json).first || ::Record.new
            record.update(data)
            record.save
            record.publish! if @publish_immediately && record.may_publish?
            
            # mutate the data which will be passed on to any other destinations, so we can associate with this record
            incoming_data[:record][:id] = record.id
          rescue => e
            DataPipelines.instrument!('error.record.destinations.data_pipelines', {data: incoming_data, error: e})
            Rollbar.log('error.record.destinations.data_pipelines',e)
          end

        end
      end
    end
  end
end
