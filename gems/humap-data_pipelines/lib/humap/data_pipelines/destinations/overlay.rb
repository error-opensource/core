module Humap
  module DataPipelines
    module Destinations
      class Overlay
        def initialize(publish_immediately: false)
          @publish_immediately = publish_immediately
        end

        def write(incoming_data)
          begin
            DataPipelines.instrument!('write.overlay.destinations.data_pipelines', {id: incoming_data[:id], publish_immediately: @publish_immediately})
            puts "Loading overlay data for #{incoming_data}"
            #  Expects a hash with a key of :overlay. All other data is ignored
            data = incoming_data
            return unless data.present?
            overlay = Humap::Overlays::Overlay.where("import_metadata @> ?", {id: data[:import_metadata][:id]}.to_json).first || Humap::Overlays::Overlay.new
            if data[:import_metadata][:metadata_url].present?
              data[:metadata] = JSON.parse(open(data[:import_metadata][:metadata_url]).read)
            end
            overlay.overlay_type = Humap::Overlays::OverlayType.where(name: "Raster Overlay").first
            overlay.update(data)
            overlay.save!
            overlay.publish! if @publish_immediately && overlay.may_publish?

            # if the import_metadata has an overlay_group_id, we need to append this overlay to that.
            #
            if data[:import_metadata].has_key?(:overlay_group_id)
              overlay_group = Humap::Overlays::OverlayGroup.where("import_metadata @> ?", {id: data[:import_metadata][:overlay_group_id]}.to_json).first
              overlay_group.overlays << overlay
            end

          rescue => e
            DataPipelines.instrument!('error.overlay.destinations.data_pipelines', {data: incoming_data, error: e})
            Rails.logger.debug("Error importing #{incoming_data}")
            Rails.logger.debug(e)
            Rails.logger.debug(e.backtrace.each(&method(:puts)))
          end
        end
      end
    end
  end
end