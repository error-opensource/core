module Humap
  module DataPipelines
    module Destinations
      class Taxonomy

        # @param associate_to_entity [#find] The entity to associate this image to. Probably Record.
        # @param associate_by [Array] The path in the incoming data hash to find the key to the entity (passed to Hash#dig). e.g. `[:record, :id]`
        # @param association_names [Array] The association names on the target entity to which you want to add this image. Associated in priority order, where the first without an image will be assigned.
        def initialize(associate_to_class: nil, associate_by: nil, term_association_name: :taxonomy_terms, subterm_mapping: {})
          @klass = associate_to_class
          @associate_by = associate_by
          @term_association_name = term_association_name
          @subterm_mapping = subterm_mapping
        end

        def write(incoming_data)
          @incoming_data = incoming_data
          begin
            DataPipelines.instrument!('write.taxonomy.destinations.data_pipelines', {terms: incoming_data[:taxonomies]})
            entity = get_associated_entity

            data = @incoming_data[:taxonomies]
            return unless data.present?

            # clear any existing terms
            entity.send(:"#{@term_association_name}=",[])

            # For each key in taxonomies, find or create a taxonomy
            #
            data.each do |name, terms|
              taxonomy = ::Taxonomy.find_or_create_by(name: name)

              # if we have as subterm mapping, we use that exclusively to map the terms and subterms. We then need to inspect the data which has been passed in, and associate that with the record, if any
              # otherwise if we have a hash, we assume a nested taxonomy
              # otherwise if we have an array, we assume a normal taxonomy

              if @subterm_mapping.any?
                taxonomy.nested!
                @subterm_mapping.each do |term, subterms|
                  t = taxonomy.taxonomy_terms.find_or_create_by(name: term)
                  subterms.each do |subterm|
                    st = t.children.find_or_create_by(name: subterm, taxonomy: taxonomy)
                    #  We need to assume that the incoming data is an array, and the subterms aren't in it. So we check to see if the subterm we just added is associated to this incoming data, and if it is, we find the record and associate it as a subterm.
                    if data[name].include?(st.name) && entity.present?
                      entity.send(@term_association_name) << t unless entity.send(@term_association_name).include?(t)
                      entity.send(@term_association_name) << st unless entity.send(@term_association_name).include?(st)
                    end
                  end
                end
              elsif terms.is_a?(Hash)
                taxonomy.nested!
                terms.each do |term, subterms|
                  t = taxonomy.taxonomy_terms.find_or_create_by(name: term)
                  subterms.each do |subterm|
                    st = t.children.find_or_create_by(name: subterm, taxonomy: taxonomy)
                    if data[name].include?(st.name) && entity.present?
                      entity.send(@term_association_name) << t
                      entity.send(@term_association_name) << st
                    end
                  end


                end
              elsif terms.is_a?(Array)
                taxonomy.dropdown!

                terms.each do |term|
                  t = taxonomy.taxonomy_terms.find_or_create_by(name: term)

                  # Associate the taxonomy term with the entity
                  if data[name].include?(t.name) && entity.present? && !entity.send(@term_association_name).include?(t)
                    entity.send(@term_association_name) << t
                  end


                end
              end
            end
          rescue => e
            DataPipelines.instrument!('error.taxonomy.destinations.data_pipelines', {data: incoming_data, error: e})
            Rollbar.log('error.taxonomy.destinations.data_pipelines', e)
          end
        end

        private

        def get_associated_entity
          if @klass.present? && @associate_by.present? && @incoming_data.dig(*@associate_by).present?
            @klass.find(@incoming_data.dig(*@associate_by))
          end
        end
      end
    end
  end
end
