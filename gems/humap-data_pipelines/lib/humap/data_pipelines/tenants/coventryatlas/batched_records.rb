module Humap
  module DataPipelines
    module Tenants
      module Coventryatlas
        module BatchedRecords
          module_function

          def setup(batch_size: 100, limit: nil, from: nil, to: nil)
            Kiba.parse do
              pre_process do
                Tenant.switch!(Tenant.find('coventryatlas'))
                DataPipelines.instrument!('pre_process.batched_records.coventryatlas.tenants.data_pipelines', {pipeline: 'Humap::DataPipelines::Tenants::Coventryatlas::BatchedRecords'})
                Tenant.disable_site_builds!
              end

              source Sources::CaptureMetadata, config: {
                api_token: ENV['CAPTURE_API_TOKEN'],
                base_url: ENV['CAPTURE_BASE_URL'],
                # we don't perform caching on the images because we selectively exclude them if they exist, later in the pipeline
                perform_caching: Rails.env.development?,
                cache_path: Rails.root.join("tmp/pipelines/coventryatlas/cache"),
                metadata_field_mappings: {
                  subject_terms: :md47,
                  subject_people: :md50,
                  content_types: :md45,
                  for_import: :md52
                }
              }, params: {
                limit: limit,
                from: from,
                to: to
              }

              destination Destinations::ChainedPipeline, pipeline: Humap::DataPipelines::Tenants::Coventryatlas::Records, batch_size: batch_size, calling_pipeline: Humap::DataPipelines::Tenants::Coventryatlas::BatchedRecords

              post_process do
                Tenant.enable_site_builds!
                DataPipelines.instrument!('post_process.batched_records.coventryatlas.tenants.data_pipelines')
              end
            end


          end
        end
      end
    end

  end
end