module Humap
  module DataPipelines
    module Tenants
      module Coventryatlas
        module Overlays
          module_function

          def setup(file:)
            Kiba.parse do

              pre_process do
                Tenant.switch!(Tenant.find('coventryatlas'))
                Tenant.disable_site_builds!
                DataPipelines.instrument!('pre_process.overlays.coventryatlas.tenants.data_pipelines', pipeline: 'Humap::DataPipelines::Tenants::Coventryatlas::Overlays')
              end

              source Sources::Excel, file: file, mapping: {
                columns: {
                    a: nil,
                    b: nil,
                    c: "filename",
                    d: "title",
                    e: "caption",
                    f: "keyword",
                    g: "country",
                    h: "state",
                    i: "city",
                    j: "location",
                    k: "datetime",
                    l: "rating"
                  },
                primary_key: "filename",
                headers: true
              }

              transform Transforms::Coventryatlas::OverlayData, group_on: nil

              destination Destinations::Overlay, publish_immediately: true

              post_process do
                Tenant.enable_site_builds!
                DataPipelines.instrument!('post_process.records.coventryatlas.tenants.data_pipelines', tenant_id: Tenant.current.id)
              end
            end
          end
        end
      end
    end
  end
end