module Humap
  module DataPipelines
    module Tenants
      module Coventryatlas
        module SingleRecord
          module_function

          def setup(capture_id:)
            Kiba.parse do

              pre_process do
                Tenant.switch!(Tenant.find('coventryatlas'))
                DataPipelines.instrument!('pre_process.records.coventryatlas.tenants.data_pipelines', {pipeline: 'Humap::DataPipelines::Tenants::Coventryatlas::SingleRecord'})
                Tenant.disable_site_builds!
              end

              source Sources::CaptureSingleRecord, config: {
                api_token: ENV['CAPTURE_API_TOKEN'],
                base_url: ENV['CAPTURE_BASE_URL'],
                # we don't perform caching on the images because we selectively exclude them if they exist, later in the pipeline
                perform_caching: Rails.env.development?,
                cache_path: Rails.root.join("tmp/pipelines/coventryatlas/cache"),
                metadata_field_mappings: {
                  subject_terms: :md47,
                  subject_people: :md50,
                  content_types: :md45,
                  for_import: :md52
                }
              }, id: capture_id

              import_ids_attached_to_images = Attachments::Image.select("import_metadata ->> 'id' as import_id, id").where(id: RecordAttachmentsImage.where(record_id: Record.all.select(:image_id))).collect(&:import_id).compact
              transform Transforms::Coventryatlas::CaptureAsset, skip_download_for_ids: import_ids_attached_to_images, image_quality: :low

              # nb order is important here - we have to create the record before the image
              destination Destinations::Record, publish_immediately: true
              destination Destinations::Image,
                          associate_to_class: ::Record,
                          associate_by: [:record, :id],
                          association_names: [:image, :attached_images]
              destination Destinations::Taxonomy,
                          associate_to_class: ::Record,
                          associate_by: [:record, :id],
                          term_association_name: :taxonomy_terms,
                          subterm_mapping: {
                            "Special Places" => ["Coventry Cathedral", "Highfield Road", "Upper Precinct"],
                            "People of Coventry" => ["Lady Godiva", "Masterji", "George Eliot"],
                            "20th Century" => ["20th Century", "Post-War Urban Design", "WWII Bombing"],
                            "Roads etc" => ["Ring Road", "Hospitals", "Schools"],
                            "Trade & Industry" => ["Cars", "Urban Development", "Deindustrialisation"],
                            "Periods of history" => ["Early Medieval", "19th Century", "Late Medieval"],
                            "Activism & Politics" => ["Activist People", "Colonialism", "Politics"],
                            "Sport" => ["Coventry City FC", "Coventry Central Baths", "Ice Hockey"],
                            "Faith & Migration" => ["History of Migration", "Coventry Cathedral", "Twin Cities"],
                            "Government" => ["Building plans", "Administration", "Second World War (1939-1945)"],
                            "Culture" => ["Belgrade Theatre", "Herbert Art Gallery", "Coventry College of Art"]
                          }

              post_process do
                Tenant.enable_site_builds!
                DataPipelines.instrument!('post_process.records.coventryatlas.tenants.data_pipelines')
              end
            end
          end
        end
      end
    end
  end
end
