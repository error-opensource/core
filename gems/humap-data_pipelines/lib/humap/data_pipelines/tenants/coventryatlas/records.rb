module Humap
  module DataPipelines
    module Tenants
      module Coventryatlas
        module Records
          module_function

          def setup(limit: 1, offset: 0, calling_pipeline: nil)
            # we use safe defaults for limit and offset, so if this data pipeline is run without the batched one, it only gets one record.
            Kiba.parse do

              pre_process do
                Tenant.switch!(Tenant.find('coventryatlas'))
                # We don't pass the pipeline into the instrumentation if there's a calling pipeline. That's because we want the log to end up in the caller, not start a new log for this pipeline.
                DataPipelines.instrument!('pre_process.records.coventryatlas.tenants.data_pipelines', {pipeline: calling_pipeline.present? ? nil : 'Humap::DataPipelines::Tenants::Coventryatlas::Records'})
                Tenant.disable_site_builds!
              end

              source Sources::Capture, config: {
                api_token: ENV['CAPTURE_API_TOKEN'],
                base_url: ENV['CAPTURE_BASE_URL'],
                # we don't perform caching on the images because we selectively exclude them if they exist, later in the pipeline
                perform_caching: Rails.env.development?,
                cache_path: Rails.root.join("tmp/pipelines/coventryatlas/cache"),
                metadata_field_mappings: {
                  subject_terms: :md47,
                  subject_people: :md50,
                  content_types: :md45,
                  for_import: :md52
                }
              }, params: {
                # limit the query to a certain number
                limit: limit,
                offset: offset,
                filter: ->(asset) {
                  # asset.for_import == '1' && asset.has_location?
                  has_location = asset.has_location?
                  unless has_location
                    DataPipelines.instrument!('skip.each.capture.sources.data_pipelines', {reason: "no location", id: asset.id})
                  end

                  for_import = (asset.for_import == '1')
                  unless for_import
                    DataPipelines.instrument!('skip.each.capture.sources.data_pipelines', {reason: "not for import", id: asset.id})
                  end

                  return has_location && for_import
                }
              }

              import_ids_attached_to_images = Attachments::Image.select("import_metadata ->> 'id' as import_id, id").where(id: RecordAttachmentsImage.where(record_id: Record.all.select(:image_id))).collect(&:import_id).compact

              transform Transforms::Coventryatlas::CaptureAsset, skip_download_for_ids: import_ids_attached_to_images, image_quality: :low

              # nb order is important here - we have to create the record before the image
              destination Destinations::Record, publish_immediately: true
              destination Destinations::Image,
                          associate_to_class: ::Record,
                          associate_by: [:record, :id],
                          association_names: [:image, :attached_images]
              destination Destinations::Taxonomy,
                          associate_to_class: ::Record,
                          associate_by: [:record, :id]

              post_process do
                Tenant.enable_site_builds!
                DataPipelines.instrument!('post_process.records.coventryatlas.tenants.data_pipelines')
              end
            end
          end
        end
      end
    end
  end
end
