module Humap
  module DataPipelines
    module Tenants
      module Mappingmemory
        module Records
          module_function

          def setup(limit: 1, offset: 0, calling_pipeline: nil, assets_folder: nil, data_file: nil)
            # we use safe defaults for limit and offset, so if this data pipeline is run without the batched one, it only gets one record.
            Kiba.parse do

              pre_process do
                Tenant.switch!(Tenant.find('mappingmemory'))
                # We don't pass the pipeline into the instrumentation if there's a calling pipeline. That's because we want the log to end up in the caller, not start a new log for this pipeline.
                DataPipelines.instrument!('pre_process.records.mappingmemory.tenants.data_pipelines', {pipeline: calling_pipeline.present? ? nil : 'Humap::DataPipelines::Tenants::Mappingmemory::Records'})
                Tenant.disable_site_builds!
              end

              source Sources::Excel, file: data_file, mapping: {
                columns: {
                  a: "id",
                  b: "placename",
                  c: "latitude",
                  d: "longitude",
                  e: "memorytitle",
                  f: "transcript",
                  g: "foldername",
                  h: "record_type",
                  i: "youtube",
                  j: "links",
                  k: "imagecount",
                  l: "imagecaptions",
                  m: "description",
                  n: "memorycategories"
                },


                primary_key: "id",
                headers: true
              }

              transform Transforms::Mappingmemory::Image, assets_folder: File.expand_path(assets_folder)
              # transform Transforms::Mappingmemory::Audio
              transform Transforms::Mappingmemory::Text, assets_folder: File.expand_path(assets_folder)
              transform Transforms::Mappingmemory::Video, assets_folder: File.expand_path(assets_folder)

              # nb order is important here - we have to create the record before the image
              destination Destinations::Record, publish_immediately: true
              destination Destinations::Image,
                          associate_to_class: ::Record,
                          associate_by: [:record, :id],
                          association_names: [:image, :attached_images]
              destination Destinations::Taxonomy,
                          associate_to_class: ::Record,
                          associate_by: [:record, :id]
              destination Destinations::VideoEmbed,
                          associate_to_class: ::Record,
                          associate_by: [:record, :id],
                          association_names: [:attached_video_embeds]

              post_process do
                Tenant.enable_site_builds!
                DataPipelines.instrument!('post_process.records.mappingmemory.tenants.data_pipelines')
              end
            end
          end
        end
      end
    end
  end
end
