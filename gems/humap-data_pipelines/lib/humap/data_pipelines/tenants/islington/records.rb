module Humap
  module DataPipelines
    module Tenants
      module Islington
        module Records
          module_function

          def setup(limit: 1, offset: 0, calling_pipeline: nil, text_folder: nil, data_file: nil, google_api_key: nil)
            # we use safe defaults for limit and offset, so if this data pipeline is run without the batched one, it only gets one record.
            Kiba.parse do

              pre_process do
                Tenant.switch!(Tenant.find('islington'))
                # We don't pass the pipeline into the instrumentation if there's a calling pipeline. That's because we want the log to end up in the caller, not start a new log for this pipeline.
                DataPipelines.instrument!('pre_process.records.islington.tenants.data_pipelines', {pipeline: calling_pipeline.present? ? nil : 'Humap::DataPipelines::Tenants::islington::Records'})
                Tenant.disable_site_builds!

                Geocoder.configure(
                  lookup: :google,
                  api_key: google_api_key
                )
              end

              source Sources::Excel, file: data_file, mapping: {
                columns: {
                  a: "name",
                  b: "category",
                  c: "dates",
                  d: "address"
                },
                primary_key: "name",
                headers: true
              }

              transform Transforms::Islington::Content, text_folder: File.expand_path(text_folder)
              transform Transforms::Islington::Location


              # nb order is important here - we have to create the record before the image
              destination Destinations::Record, publish_immediately: true
              destination Destinations::Taxonomy,
                          associate_to_class: ::Record,
                          associate_by: [:record, :id]
            
              post_process do
                Tenant.enable_site_builds!
                DataPipelines.instrument!('post_process.records.islington.tenants.data_pipelines')
              end
            end
          end
        end
      end
    end
  end
end
