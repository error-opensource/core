# Humap::DataPipelines

This gem contains the [Kiba](https://github.com/thbar/kiba) pipelines for Humap.

# Structure

Sources, transforms and destinations are in their own namespaces. Pipelines per project are in the `Tenants` folder.

# Running a pipeline
Set up the tenant pipeline with the right parameters, then tell Kiba to run it.

For example:

```
pipeline = Humap::DataPipelines::Tenants::Coventryatlas.setup
Kiba.run(pipeline)
```