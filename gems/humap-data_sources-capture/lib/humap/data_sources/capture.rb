require 'fileutils'

require "humap/data_sources/capture/version"
require "humap/data_sources/capture/asset"

module Humap
  module DataSources
    module Capture
      class << self
        attr_accessor :configuration, :connection

        def configure(config=nil)
          self.configuration ||= Configuration.new(config)
          yield(self.configuration) if block_given?
          self.configuration.configure_connection
          self.configuration.configure_cache! if self.configuration.perform_caching?
          @connection = @configuration.connection

        end
      end

      class Configuration

        attr_accessor :base_url, :api_token, :cache_path, :perform_caching, :metadata_field_mappings

        attr_reader :connection

        def initialize(config)
          if config.is_a?(Hash)
            config.each_pair do |setting, val|
              send(:"#{setting}=",val)
            end
          end
          @metadata_field_mappings ||= {}
        end

        def configure_connection
          @connection ||= Faraday.new({
                                        url: @base_url,
                                        headers: {
                                          "Authorization" => "Bearer #{@api_token}"
                                        }
                                      }) do |conn|
            conn.request :json
            conn.response :json
          end
        end

        def configure_cache!
          if @perform_caching && @cache_path.present?
            FileUtils.mkdir_p(@cache_path)
          end
        end

        def perform_caching?
          @perform_caching
        end
      end

      class Error < StandardError; end
    end
  end
end
