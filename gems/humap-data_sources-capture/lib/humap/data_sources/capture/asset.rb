module Humap
  module DataSources
    module Capture
      class Asset < ::OpenStruct

        # Dynamically create two instance methods called lowres and hires (aliased to highres)
        # This will either return a closed file handle, or take a block with an open file IO
        # for reading / streaming elsewhere.
        [:lowres, :highres].each do |meth|
          define_method(meth) do |&block|
            begin
              f = Capture.configuration.perform_caching? ? File.open(File.join(Capture.configuration.cache_path, "#{id}-#{meth}"), "ab+") : Tempfile.new.binmode

              # return the cached file if it already exists
              if f.size > 0
                block.call(f) if block
                return f unless block
              else
                self.class.get("asset/#{id}/#{meth}") do |req|
                  req.options.on_data = Proc.new do |chunk, total|
                    f.write(chunk)
                  end
                end

                block.call(f) if block
                f.close

                return f unless block
              end
            ensure
              f.unlink if f.is_a?(Tempfile)
            end
          end
        end

        # A paginated request to get the metadata for assets.
        def self.metadata(per_page: 100, page: 1, limit: nil, from: nil, to: nil)
          puts "Fetching metadata: limit: #{limit || "none"}, per_page: #{per_page}, page: #{page}"
          per_page = limit if (limit && limit < per_page)
          dates = []
          dates << from.strftime('%Y-%m-%d') if from.present?
          dates << to.strftime('%Y-%m-%d') if to.present?
          options = {"itemsPerPage" => per_page, "page" => page, "published" => "yes"}
          if dates.compact.any?
            options.merge!({"uploadDate" => dates.compact.join(":")})
          end
          results = []
          loop do
            break if limit.present? && results.flatten.count >= limit
            response = get('admin/assets', options).body['data']
            print "\r\tPage #{page} of #{response['pages']} total"
            # stop if we're on the last page
            results << response['assets']
            break if response['pages'] == page
            page += 1
            options["page"] = page
          end
          results.flatten.collect {|r| r["summary"]}.collect do |asset_data|
            OpenStruct.new(asset_data.merge({id: asset_data["idasset"].try(&:to_i)}))
          end
        end

        # Actually get the data for each asset. We get the metadata first, then map over it to get the real stuff.
        def self.all(limit: nil, from: nil, to: nil, offset: nil)
          page = 1
          if limit.present? && offset.present?
            page = ((offset / limit.to_f).ceil + 1)
          end
          metadata = metadata(limit: limit, from: from, to: to, per_page: limit, page: page)
          count = metadata.count
          puts "\nFetching data: #{count} total items to fetch, fetching from item #{offset || "0"}"
          results = metadata.each_with_index.collect do |meta, i|
            print "\r\t#{i} / #{count}"
            find(meta.id)
          end

          results.compact
        end

        def self.find(id)
          cache =  File.join(Capture.configuration.cache_path, "#{id}.json")
          # if caching is enabled, check to see if the file exists and has content
          # if it does, read that.
          # if it doesn't, fetch the content
          # if caching isn't enabled, just fetch the content
          #
          #

          if Capture.configuration.perform_caching?
            if File.exist?(cache) && File.size(cache) > 0
              print " Using cache for id #{id}"
              self.new(JSON.parse(File.open(cache, "r").read))
            else
              response = get_asset_response(id)
              if response.has_key?(:success)
                File.open(cache, "w+") {|f| f.write(response[:success].to_json)}
                self.new(response[:success])
              else
                Error.new(response[:failure])
              end
            end
          else
            response = get_asset_response(id)
            if response.has_key?(:success)
              self.new(response[:success])
            else
              Error.new(response[:failure])
            end
          end
        end

        def id
          summary["idasset"].try(:to_i)
        end

        def title
          metadata['md30']['value']
        end

        def caption
          metadata['md2']['value']
        end

        def location
          metadata['md42']['value']
        end

        def copyright_notice
          metadata['md25']['value']
        end

        def credit
          metadata['md9']['value']
        end

        def lat
          metadata['md49']['value']
        end

        def lng
          metadata['md51']['value']
        end

        def has_location?
          matcher = /[^\d.-]/
          # location needs to be present, but also needs to be only numbers and a dot. if there's anything else, it won't work.
          lat.present? && lng.present? && !lat.match(matcher) && !lng.match(matcher)
        end

        def method_missing(meth, *args)
          if meth.in?(Capture.configuration.metadata_field_mappings.keys)
            val = metadata[Capture.configuration.metadata_field_mappings[meth].to_s]['value']
            val.index(',') ? val.split(',') : val
          else
            super
          end
        end

        private

        def self.connection
          Capture.configuration.connection
        end

        def self.get(endpoint, body = nil, headers = nil, &block)
          connection.get(endpoint, body, headers, &block)
        end

        def self.get_asset_response(id)
          response = get("asset/#{id}/preview")
          if response.success?
            {success: response.body['data']['asset']}
          else
            {failure: response.body['error']['summary']}
          end
        end

      end
    end
  end
end
