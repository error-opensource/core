# Humap::DataSources::Capture

A client for the [Capture](http://capture.co.uk/) API.

# Use

Configure in an initializer: 

```
Humap::DataSources::Capture.configure do |config|
  config.api_token = ENV['CAPTURE_API_TOKEN']
  config.base_url = ENV['CAPTURE_BASE_URL']
  config.perform_caching = true
  config.cache_path = Rails.root.join("tmp/data_sources/capture/cache")
end
```

Access assets like this:

```
Humap::DataSources::Capture::Asset.all(limit: 10) # returns 10 assets
Humap::DataSources::Capture::Asset.all() # returns all the assets - very slow

Humap::DataSources::Capture::Asset.find(1234) # returns 1 asset
```                                                                          

## Files

Assets have a high and low resolution file associated with them. We download them to a cache or tempfile. Cache is preferred.

You can either call the method and use the file handle, or pass a block and use the IO object:

```
asset = Humap::DataSources::Capture::Asset.find(1234)

closed_file = asset.high_res #returns a closed file handle. You can open it again with File.open(closed_file)

asset.high_res do |io|
    io.read #do something with the IO object here
end
```         



