require_relative 'lib/humap/data_sources/capture/version'

Gem::Specification.new do |spec|
  spec.name          = "humap-data_sources-capture"
  spec.version       = Humap::DataSources::Capture::VERSION
  spec.authors       = ["Ed Jones", "Error Team"]
  spec.email         = ["ed@error.agency", "team@error.agency"]

  spec.summary       = "Data sources for the Capture digital asset management platform"
  spec.homepage      = "https://humap.me"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")


  spec.metadata["homepage_uri"] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "faraday"
end
