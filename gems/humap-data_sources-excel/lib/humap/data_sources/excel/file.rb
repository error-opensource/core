module Humap
  module DataSources
    module Excel
      class File
        attr_reader :mapping, :worksheets

        def initialize(file:, mapping:)
          if mapping.is_a?(String) && (File.exists?(File.expand_path(mapping)) rescue false)
            #  we have a file which needs to be parsed
            @mapping = YAML.load(File.open(File.expand_path(mapping)).read)
          elsif mapping.is_a?(String)
            @mapping = YAML.load(mapping)
            # we have a YAML string to parse
          elsif mapping.is_a?(Hash)
            # we have a mapping already as a hash
            @mapping = mapping.with_indifferent_access
          elsif mapping.is_a?(Array)
            @mapping = mapping.collect {|h| h.with_indifferent_access}
          else
            raise MappingError, "Mapping needs to be a file path, a YAML string, Hash or Array"
          end

          unless (@mapping.is_a?(Array) && @mapping.first.is_a?(Hash) && @mapping.first.has_key?(:columns)) || (@mapping.is_a?(Hash) &&@mapping.has_key?(:columns))
            raise MappingError, "Mapping must have at least a 'columns' key containing a hash of column identifiers and field names"
          end

          @file = file

        end

        def parse!
          workbook = RubyXL::Parser.parse(@file)

          return @worksheets if @worksheets.present?

          if @mapping.is_a?(Array)
            #  we have more than one worksheet we need to work through; return rows for each in an array
            @worksheets = @mapping.each_with_index.collect do |mapping,i|
              rows = rows_from_worksheet(workbook.worksheets[i], mapping)
              Worksheet.new(rows)
            end
          elsif @mapping.is_a?(Hash)
            #  we have one worksheet, return an array of one.
            @worksheets = [
              Worksheet.new(
                rows_from_worksheet(workbook.worksheets[0], @mapping)
              )
            ]
          end

        end

        def worksheets
          parse!
        end

        private

        def rows_from_worksheet(worksheet, mapping)
          header_row_count = mapping[:header_row_count]
          data_row_count = header_row_count.is_a?(Integer) ? worksheet.count - header_row_count : worksheet.count
          rows = (0..worksheet.count-1).collect do |i|
            if mapping[:headers].is_a?(TrueClass)
              if mapping[:header_row_count].is_a?(Integer)
                # Skip header row count
                next if i < header_row_count
              else
                # skip just one row
                next if i === 0
              end
            end
            print "\r#{i} / #{data_row_count}"
            rows = worksheet[i]
            next if rows.nil?
            data = mapping[:columns].inject({}) do |hash, (identifier, field_name)|
              next hash if field_name.nil?
              column = column_name_to_number(identifier)
              next hash if column.nil?
              hash[field_name] = worksheet[i][column].try(&:value)
              hash
            end

            Row.new(data, pk: mapping[:primary_key])
          end
          
          rows.compact
        end

        def column_name_to_number(name)
          ('a'..'zzz').to_a.index(name.to_s)
        end

      end
    end
  end
end