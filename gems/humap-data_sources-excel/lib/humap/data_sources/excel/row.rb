module Humap
  module DataSources
    module Excel
      class Row < OpenStruct

        def initialize(hash = nil, pk: nil)
          @pk = pk
          super(hash)
        end

        def id
          if @pk && !respond_to?(@pk)
            self.send(@pk.to_sym)
          else
            super
          end
          
        end

      end
    end
  end
end