require 'rubyXL'
require 'rubyXL/convenience_methods'
require 'fileutils'
require 'yaml'

require "humap/data_sources/excel/version"
require "humap/data_sources/excel/file"
require "humap/data_sources/excel/worksheet"
require "humap/data_sources/excel/row"

module Humap
  module DataSources
    module Excel
      class MappingError < StandardError; end
    end
  end
end
